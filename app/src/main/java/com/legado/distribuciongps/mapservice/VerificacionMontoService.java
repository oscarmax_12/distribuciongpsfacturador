package com.legado.distribuciongps.mapservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.mapgeofence;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.SessionUsuario;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificacionMontoService extends Service {

        private final int TIEMPO = (60*1000)*30;
//    private final int TIEMPO = 25000;
    Handler handler;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private SessionUsuario sessionUsuario;

    public VerificacionMontoService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        sessionUsuario = new SessionUsuario(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("")
                    .setAutoCancel(true)
                    .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                    .build();

            startForeground(1, notification);
        }
        int NOTIFICATION_ID = (int) (System.currentTimeMillis() % 10000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, new Notification.Builder(this).build());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        } else {
            stopSelf();
        }
        ejecutarTarea();
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        //handler = new Handler();
        super.onStart(intent, startId);
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("")
                    .setAutoCancel(true)
                    .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                    .build();

            startForeground(1, notification);
        }

            if (intent.getAction().equalsIgnoreCase("stop")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    stopForeground(true);
                } else {
                    stopSelf();
                }
            }else{
                if (intent.getAction().equalsIgnoreCase("stop2")){
                    stopSelf();
                }else{
                    if (intent.getAction().equalsIgnoreCase("startFore")){
                        Log.e("startFore","entro foreground");
                    }else{
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            stopForeground(true);
                        } else {
                            stopSelf();
                        }
                    }

                }

            }



//        String input = intent.getStringExtra("inputExtra");
//        Log.e("SERIVIO DENTRO", input);
        ejecutarTarea();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("destroy","asdasd");
    }

    public void ejecutarTarea() {
        handler.postDelayed(new Runnable() {
            public void run() {

                // función a ejecutar
                cargarEntregasByPlaca(sessionUsuario.getCodigoAplicacion()); // función para refrescar la ubicación del conductor, creada en otra línea de código

                handler.postDelayed(this, TIEMPO);
            }

        }, TIEMPO);

    }

    public void cargarEntregasByPlaca(String placa) {

        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("placa", placa);
        dataConsulta.put("codSede", sessionUsuario.getCodSede());
        Call<JsonRespuesta<Rebote>> call = ApiRetrofit.getInstance().getMetricasService().getEntregasByPlaca(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta<Rebote>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Rebote>> call, Response<JsonRespuesta<Rebote>> response) {
                if (response.code() == 401) {
                    Toast.makeText(getApplicationContext(), "SE EXPIRÓ EL TIEMPO DE LA TOMA DE PEDIDOS PARA SU USUARIO ,POR FAVOR COMUNIQUESE CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //  progressDialog.dismiss();
                } else if (response.code() == 403) {
                    Toast.makeText(getApplicationContext(), "USUARIO INACTIVO, COMUNIQUESE CON CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //   progressDialog.dismiss();

                } else {

                    JsonRespuesta<Rebote> rpta = response.body();

                    if (rpta.getData().isEmpty()) {
                        Log.e("ServicioVerificacion", "NO PRESENTA PAGOS AL CONTADO");
                        // MensajesDialogAlert.mensajeInformacion(getApplicationContext(),"info", "NO PRESENTA PAGOS A CONTADO");

                    } else {
                        Log.e("AUN NO ENTRA", "EL MONTO ES MAYOR AL ESPECIFICADO");
                        BigDecimal montoTotal = montoTotalLiquidacion(rpta.getData());
                        BigDecimal b2 = new BigDecimal("3000.00");
                        if (1 == (montoTotal.compareTo(b2))) {
                            Log.e("MAYOR", "EL MONTO ES MAYOR AL ESPECIFICADO");
//                            sendNotification("MONTO AL CONTADO EXCEDIDO","Se recomienda hacer deposito por precaución");
                            createNotificationChannel();
                            crearNotificacion("Se recomienda hacer deposito, hacer clic para ver los bancos cercanos ", "El monto al contado excede a 3000 soles");
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonRespuesta<Rebote>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "HUBO UN PROBLEMA CON LA VERIFICACION DE MONTO", Toast.LENGTH_LONG).show();
                //
            }
        });
    }

    public BigDecimal montoTotalLiquidacion(List<Rebote> documentoDeudas) {
        BigDecimal montoTotal = new BigDecimal(0);
        for (int i = 0; i < documentoDeudas.size(); i++) {
            montoTotal = montoTotal.add(documentoDeudas.get(i).getMonto());
        }
        return montoTotal;
    }

    private void crearNotificacion(String mensaje, String titulo) {
        Intent notificationIntent = new Intent(this, mapgeofence.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(titulo)
                .setColor(Color.RED)
                .setContentText(mensaje)
                .setSmallIcon(R.drawable.ic_attach_money_red_600_48dp)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
        //PendingIntent.getService(this, GEOFENCE_REQ_CODE, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        startForeground(1, notification);

    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
