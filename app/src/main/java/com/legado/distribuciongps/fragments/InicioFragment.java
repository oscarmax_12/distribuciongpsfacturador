package com.legado.distribuciongps.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.dialogs.DialogProgress;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.UtilAndroid;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InicioFragment extends Fragment {

    //    @BindView(R.id.reciclador)
//    RecyclerView reciclador;
    @BindView(R.id.txtPlaca)
    TextView txtPlaca;

    @BindView(R.id.txtMonto)
    TextView txtMonto;

    @BindView(R.id.txtMontoRebote)
    TextView txtMontoRebote;
    @BindView(R.id.txtMontoEntregas)
    TextView txtMontoEntregas;
    @BindView(R.id.txtCantidad)
    TextView txtCantidad;

    @BindView(R.id.txtMontoContados)
    TextView txtMontoContados;
    @BindView(R.id.txtCantCont)
    TextView txtCantCont;

    @BindView(R.id.txtCantCred)
    TextView txtCantCred;
    @BindView(R.id.txtMontoCreditos)
    TextView txtMontoCreditos;
    @BindView(R.id.txtMontoAdel)
    TextView txtMontoAdel;
    @BindView(R.id.txtCantAdel)
    TextView txtCantAdel;

    @BindView(R.id.txtLabelPorcent)
    TextView txtLabelPorcent;
    @BindView(R.id.btnMenu)
    Button btnMenu;




    DialogProgress dialogLoadingMetricas;
    private SessionUsuario sessionUsuario;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private Double progressStatus = 0.0;
    private Handler handler = new Handler();
    private InicioFragmentListeners listeners;
    public static InicioFragment newInstance(InicioFragmentListeners listeners) {
        InicioFragment fragment = new InicioFragment(listeners);
        return fragment;
    }

    public InicioFragment(InicioFragmentListeners listeners) {
        this.listeners=listeners;
    }

    public String getFormatoFecha(int yyyy, int mm, int dd) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = dateFormat.parse(dd + "-" + (mm + 1) + "-" + yyyy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outDate = dateFormat.format(date);

        return outDate;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inicio_info, container, false);
        ButterKnife.bind(this, rootView);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialogLoadingMetricas = new DialogProgress();
        dialogLoadingMetricas.show(ft, "dialog");
        sessionUsuario = new SessionUsuario(getContext());
        loadMetricas();
        btnMenu.setOnClickListener(view -> {
            listeners.showdrawer();
        });



        return rootView;
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_inicio, menu);
    }


    private void showProgress(final String s) {
        progressStatus = 0.0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < new Double(s)) {
                    progressStatus += 0.1;
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressStatus.intValue());
                            if(progressStatus>100){
                                txtLabelPorcent.setText(100 + "%");
                            }else {
                                txtLabelPorcent.setText(UtilAndroid.round(progressStatus, 2) + "%");
                            }
                        }
                    });
                }
            }
        }).start();
    }


    private void loadMetricas() {
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("placa", sessionUsuario.getCodigoAplicacion());
        dataConsulta.put("codSede",sessionUsuario.getCodSede());
        Call<JsonRespuesta> loginCall = ApiRetrofit.getInstance().getMetricasService().getMetricas(dataConsulta);
        loginCall.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                if (response.body().getEstado() == 1) {
                    ArrayList objeto = (ArrayList) response.body().getData();

                            ArrayList cabecera = (ArrayList) objeto.get(0);
                            if(cabecera.size()>0){
                                ArrayList condiciones = (ArrayList) objeto.get(1);

                                BigDecimal porcentEfectividad=null;
                                for (int i = 0; i < cabecera.size(); i++) {
                                    txtPlaca.setText(((ArrayList) cabecera.get(0)).get(0).toString());
                                    txtCantidad.setText(((ArrayList) cabecera.get(0)).get(1).toString());

                                    txtMonto.setText("S/."+ Metodos.formatearDecimales(((ArrayList) cabecera.get(0)).get(2).toString()));
                                    BigDecimal montoTotal=new BigDecimal(((ArrayList) cabecera.get(0)).get(2).toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
                                    BigDecimal montoEfectivo=new BigDecimal(((ArrayList) cabecera.get(0)).get(7).toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
                                    txtMontoRebote.setText("S/."+((ArrayList) cabecera.get(0)).get(5).toString());

                                    txtMontoEntregas.setText("S/."+Metodos.formatearDecimales(((ArrayList) cabecera.get(0)).get(7).toString()));

                                    System.out.println("dididid "+montoTotal);
                                    System.out.println("dididid "+montoEfectivo);
                                    BigDecimal division=montoEfectivo.multiply(new BigDecimal(100)).divide(montoTotal,2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                                    porcentEfectividad=division;//(division.multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));

                                    System.out.println("dididid "+division);
                                }

                                for (int i = 0; i < condiciones.size(); i++) {


                                    txtMontoAdel.setText("S/."+Metodos.formatearDecimales(((ArrayList) condiciones.get(0)).get(5).toString()));
                                    txtMontoContados.setText("S/."+Metodos.formatearDecimales(((ArrayList) condiciones.get(0)).get(3).toString()));
                                    txtMontoCreditos.setText("S/."+Metodos.formatearDecimales(((ArrayList) condiciones.get(0)).get(4).toString()));
                                    txtCantCred.setText(((ArrayList) condiciones.get(0)).get(1).toString());
                                    txtCantCont.setText(((ArrayList) condiciones.get(0)).get(0).toString());
                                    txtCantAdel.setText(((ArrayList) condiciones.get(0)).get(2).toString());//
                                }

                                showProgress(porcentEfectividad.toString());
                            }else{
                                dialogLoadingMetricas.dismiss();
                                MensajesDialogAlert.mensajeInformacion(getContext(),"info","PLACA SIN INFORMACION PARA HOY");
                            }

                    dialogLoadingMetricas.dismiss();
                    }


            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                dialogLoadingMetricas.dismiss();
            }
        });
    }

    public interface InicioFragmentListeners {
        void showdrawer();

    }
}
