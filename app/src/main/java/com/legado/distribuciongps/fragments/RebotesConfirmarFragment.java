package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.TabsReboteActivity;
import com.legado.distribuciongps.adapters.EntregasRecyclerAdapter;
import com.legado.distribuciongps.adapters.ReboteConfirmarRecyclerAdapter;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.SessionUsuario;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RebotesConfirmarFragment extends Fragment implements ReboteConfirmarRecyclerAdapter.InterfaceReboteInventarioFragment {


    @BindView(R.id.reciclador)
    RecyclerView reciclador;

    private RecyclerView.LayoutManager lManager;
    @BindView(R.id.txtTotalRebotes)
    TextView txtTotalRebotes;
    @BindView(R.id.txtTotalRebotesCant)
    TextView txtTotalRebotesCant;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    ProgressDialog progressDialog;
    private SessionUsuario sessionUsuario;
    ReboteConfirmarRecyclerAdapter entregasRecyclerAdapter;
    ReboteConfirmarRecyclerAdapter.InterfaceReboteInventarioFragment interfaceReboteInventarioFragment;
    TabsReboteActivity.SectionsPagerAdapter adapter;

    public static RebotesConfirmarFragment newInstance(TabsReboteActivity.SectionsPagerAdapter adapter) {
        RebotesConfirmarFragment fragment = new RebotesConfirmarFragment(adapter);

        return fragment;
    }

    public RebotesConfirmarFragment(TabsReboteActivity.SectionsPagerAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reboteconfirmar, container, false);
        ButterKnife.bind(this, rootView);
        this.interfaceReboteInventarioFragment = this;
        sessionUsuario = new SessionUsuario(getContext());
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);


        cargarRebotesByConfirmar();
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        cargarRebotesByConfirmar();
                        swiperefresh.setRefreshing(false);
                    }
                });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inicio, menu);

    }


    public void cargarRebotesByConfirmar() {
        progressDialog.show();
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("placa", sessionUsuario.getCodigoAplicacion());
        dataConsulta.put("codSede", sessionUsuario.getCodSede());
        Call<JsonRespuesta<Rebote>> call = ApiRetrofit.getInstance().getMetricasService().getRebotesByPlaca(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta<Rebote>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Rebote>> call, Response<JsonRespuesta<Rebote>> response) {
                if (response.code() == 401) {
                    Toast.makeText(
                            getActivity(), "SE EXPIRÓ EL TIEMPO DE LA TOMA DE PEDIDOS PARA SU USUARIO ,POR FAVOR COMUNIQUESE CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //  progressDialog.dismiss();
                } else if (response.code() == 403) {
                    Toast.makeText(getActivity(), "USUARIO INACTIVO, COMUNIQUESE CON CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //   progressDialog.dismiss();

                } else {
                    reciclador.setVisibility(View.VISIBLE);
                    //txtEmpty.setVisibility(View.GONE);
                    reciclador.setHasFixedSize(true);
                    lManager = new LinearLayoutManager(getActivity());
                    reciclador.setLayoutManager(lManager);
                    ((InventarioReboteFragment) adapter.getItem(1)).cargarInventarioReboteByPlaca();
                    JsonRespuesta<Rebote> rpta = response.body();
                    if (rpta.getData().isEmpty()) {
                        MensajesDialogAlert.mensajeInformacion(getContext(), "info", "NO PRESENTA REBOTES");

                    } else {
                        entregasRecyclerAdapter = new ReboteConfirmarRecyclerAdapter(rpta.getData(), getActivity(), RebotesConfirmarFragment.this, swiperefresh, interfaceReboteInventarioFragment);
                        txtTotalRebotes.setText(entregasRecyclerAdapter.montoTotalLiquidacion().toString());
                        txtTotalRebotesCant.setText(String.valueOf(entregasRecyclerAdapter.cantTotal()));
                        reciclador.setAdapter(entregasRecyclerAdapter);
                    }

                    progressDialog.dismiss();
//
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Rebote>> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(getView(), "Problemas de conexión  ", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Reintentar", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // cargarPlanillaCobranza();

                            }
                        }).show();
                //
            }
        });
    }


    @Override
    public void reloadInventario() {
        MensajesDialogAlert.mensajeConfirmacion(getContext(), "aaa", "oko");
    }


}
