package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.adapters.AdaptadorPedidosXrefacturar;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Pedido;
 import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class pedidoXrefacturar_frag extends Fragment {
    AdaptadorPedidosXrefacturar adapter;
    RecyclerView rvPedidosSinFacturar;
    SessionUsuario sesionUsuario;
    ProgressDialog progressDialog;
    Button btnMenu2;




    public pedidoXrefacturar_frag() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_pedido_xrefacturar_frag, container, false);
        rvPedidosSinFacturar=(RecyclerView)v.findViewById(R.id.recyclerPedidosXRefacturar);
        progressDialog = new ProgressDialog(getContext());
        sesionUsuario=new SessionUsuario(getActivity());
        rvPedidosSinFacturar.setHasFixedSize(true);
        rvPedidosSinFacturar.setLayoutManager(new LinearLayoutManager(this.getContext()));
        Metodos.validarSesion(getActivity());
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(getContext(), "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            getActivity().finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(getContext(), "ERROR!", "Error");
            getActivity().finish();
        }

        Metodos.showProgressDialogWithTitle("Cargando lista de pedidos por refacturar",progressDialog);
        cargarPedidosxRefacturar();

        return v;
    }

    public void cargarPedidosxRefacturar() {
        Map<String, String> data = new HashMap<>();
        data.put("placa", sesionUsuario.getCodigoAplicacion());
        Log.e("placa",sesionUsuario.getCodigoAplicacion());
        Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getMetricasService().getPedidosByRefacturar(data);
        call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Pedido>> call, retrofit2.Response<JsonRespuesta<Pedido>> response) {

                try {

                    int estado = response.body().getEstado();
                    if (estado == 1) {
                        if (!response.body().getData().isEmpty()) {



                            adapter = new AdaptadorPedidosXrefacturar(getContext(),getActivity());
                            adapter.addList(response.body().getData());
                            rvPedidosSinFacturar.setAdapter(adapter);
                            Metodos.hideProgressDialogWithTitle(progressDialog);

                        } else {
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            MensajesDialogAlert.mensajeInformacion(getContext(), "Información", "No se encontraron Pedidos por Refacturar para tu placa 1");
                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog);
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron pedido por refacturar para tu placa 2");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }

public interface pedidoXrefacturar_frag_listeners {
    void showdrawer();

}



}
