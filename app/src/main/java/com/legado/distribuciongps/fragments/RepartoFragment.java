package com.legado.distribuciongps.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.AnyOrientationCaptureActivity;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Asignacion;
import com.legado.distribuciongps.modelo.DocReparto;
import com.legado.distribuciongps.modelo.ListaDocumento;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.AdapterDocReparto;
import com.legado.distribuciongps.util.SessionUsuario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ERICK on 15/08/2019.
 */

public class RepartoFragment extends Fragment {

    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final String TAG = RepartoFragment.class.getSimpleName();
    private ZXingScannerView scannerView;
    //public static List<Articulo> detalles;
    private static AdapterDocReparto customAdapter;
    private SessionUsuario sessionUsuario;
    public ProgressDialog progressDialog;
    @BindView(R.id.btnRegistrarRebote)
    Button btnRegistrarRebote;
    @BindView(R.id.listaDocumentos)
    RecyclerView listaDocumentos;
    @BindView(R.id.cantPedidos)
    TextView cantPedidos;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("REPARTO DE PEDIDOS");
    }

    public void EscanerQR(View view){
        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(RepartoFragment.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
        integrator.initiateScan();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reparto, container, false);
        ButterKnife.bind(this,rootView);

        progressDialog = new ProgressDialog(getActivity(),R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        sessionUsuario = new SessionUsuario(getActivity());
        List<Asignacion> accesos = sessionUsuario.getAsignaciones();

        String codEmp = "";
        String codSede = "";
        if(!accesos.isEmpty()){
            codEmp = accesos.get(0).getEmpresa().getCodEmpresa();
            codSede = accesos.get(0).getSede().getCodSede();
        }
        progressDialog.show();
        Map<String, String> dataClientes = new HashMap<>();
        dataClientes.put("codemp", codEmp);
        dataClientes.put("codsede",codSede);
        dataClientes.put("numplaca",sessionUsuario.getCodigoAplicacion());
        Call<JsonRespuesta<DocReparto>> call = ApiRetrofit.getInstance().getRepartoService().IniciarJornadaReparto(dataClientes);
        call.enqueue(new Callback<JsonRespuesta<DocReparto>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<DocReparto>> call, retrofit2.Response<JsonRespuesta<DocReparto>> response) {
                JsonRespuesta<DocReparto> respuesta = response.body();
                List<DocReparto> documentos = respuesta.getData();
                Integer estado = respuesta.getEstado();

                if (estado == 1) {
                    cantPedidos.setText(String.valueOf(documentos.size()));

                    customAdapter = new AdapterDocReparto(getActivity(),getContext(),documentos);
                    listaDocumentos.setAdapter(customAdapter);
                    listaDocumentos.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

                    ListaDocumento listado = new ListaDocumento();
                    listado.setLista(documentos);
                    sessionUsuario.GuardarListaDocumentos(listado);
                    progressDialog.dismiss();

                } else {
                    Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<DocReparto>> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

        btnRegistrarRebote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()){
                    EscanerQR(v);
                } else {
                    requestPermission();
                }
            }
        });
        return rootView;
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA},PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if(result.getContents() == null){
                Toast.makeText(getContext(),"Cancelaste el escaneo",Toast.LENGTH_SHORT).show();
            } else {
                //01 = FACTURA.
                //03 = BOLETA.
                //20271522950|03|134|0563376|10.96|71.83|02-08-2019|6|43137964
                String cadena = result.getContents().toString();
                String[] arreglos = cadena.split("\\|");

                String tipDoc = arreglos[1];
                String serie = arreglos[2];
                String preimpreso = arreglos[3];

                if(tipDoc != null){
                    tipDoc = tipDoc.equals("03") ? "02" : (tipDoc.equals("01") ? "01" : "");
                }

                List<DocReparto> listado = sessionUsuario.getListaDocumentos().getLista();
                String seencuentra = "0";
                Rebote rebote = new Rebote();
                for(int i=0;i<listado.size();i++){
                    if(tipDoc.equals(listado.get(i).getCodTipDoc()) && serie.equals(listado.get(i).getSerie())
                        && preimpreso.equals(listado.get(i).getImpreso())){
                        rebote.setCodEmp("01");
                        rebote.setTipoDoc(tipDoc);
                        rebote.setSerie(serie);
                        rebote.setPreimpreso(preimpreso);
                        rebote.setTipoRebote("05");
                        rebote.setCodUsuario("DSIS");
                        seencuentra = "1";
                        i=listado.size()+2;
                    }
                }

                if(seencuentra.equals("1")){
                    System.out.println("REBOTE REGISTRADO.");
                    Call<JsonRespuesta> call = ApiRetrofit.getInstance().getRepartoService().RegistrarRebote(rebote);
                    call.enqueue(new Callback<JsonRespuesta>() {
                        @Override
                        public void onResponse(Call<JsonRespuesta> call,retrofit2.Response<JsonRespuesta> response){
                            JsonRespuesta rptaClientes=response.body();
                            Integer estado = rptaClientes.getEstado();
                            if(estado == 1) {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Rebote registrado correctamente." , Toast.LENGTH_LONG).show();
                                //finish();
                            }else if(estado == 2){
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), rptaClientes.getMensaje() , Toast.LENGTH_LONG).show();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getContext(),"Problemas de conexión , por favor intentelo de nuevo",Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                            //progress.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            Toast.makeText(getContext(),"Problemas de conexión , por favor intentelo de nuevo",Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(getContext(), "NO SE ENCUENTRA EL DOCUMENTO EN LA LISTA.", Toast.LENGTH_LONG).show();
                }








            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}