package com.legado.distribuciongps.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.toFrameMapa;
import com.legado.distribuciongps.adapters.AdaptadorClienteBuscar;
import com.legado.distribuciongps.modelo.Cliente;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.SimpleDividerItemDecorator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientesBuscar extends Fragment {
    AdaptadorClienteBuscar adapter;
    RecyclerView rvCliente;
    Cliente objCliente;

    @BindView(R.id.txtSearch)
    TextView txtSearch;
    SessionUsuario sessionUsuario;

    public static ClientesBuscar newInstance() {
        ClientesBuscar fragment = new ClientesBuscar();
        return fragment;
    }

    public ClientesBuscar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_clientes_buscar, container, false);
        sessionUsuario = new SessionUsuario(getActivity());
        ButterKnife.bind(this, v);
        rvCliente = (RecyclerView) v.findViewById(R.id.recyclerListaClientesSinCoordenadas);

        rvCliente.setHasFixedSize(true);
        rvCliente.setLayoutManager(new LinearLayoutManager(this.getContext()));

        ((toFrameMapa) getActivity()).hideFloatingActionButton();
        inicio();
        SharedPreferences.Editor editor = RutasFrag.sharedPref.edit();

        editor.putString("recarga","si");
        editor.putString("depaso","si");
        editor.commit();

        txtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    txtSearch.setHint("");
                else
                    txtSearch.setHint("BUSCAR CLIENTE");
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            String ba = "";

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //mostrarIconoClear();
                // if (txtSearch.getText().length() >= ba.length()) {
                //if (cs.toString().length() >= 4) {
                String clienteText = txtSearch.getText().toString().toLowerCase(); //newText.toLowerCase();
                ArrayList<Cliente> newClientList = new ArrayList<Cliente>();
                for (Cliente c : sessionUsuario.getPaqueteCliente().getListaClientes()) {
                    if (c.getNomCliente().toLowerCase().contains(clienteText) || c.getDirDespacho().toLowerCase().contains(clienteText)) {
                        newClientList.add(c);
                    }
                }
                adapter.updateList(newClientList);
                adapter = new AdaptadorClienteBuscar(newClientList, getContext());
                rvCliente.setAdapter(adapter);
                rvCliente.addItemDecoration(new SimpleDividerItemDecorator(getActivity()));

                // }

                //}
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

                ba = arg0.toString();
                //  recicladorArticulo.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
//                if (descArticulo.getText().toString().length() <= 0) {
//                  recicladorArticulo.setVisibility(View.GONE);
//
//                }
            }
        });

        return v;
    }


    public void inicio() {
//        ArrayList<Cliente> listaFiltrada=new ArrayList<Cliente>();
//        for (int i = 0; i < objCliente.listaCliente.size(); i++) {
//            Cliente item=objCliente.listaCliente.get(i);
//
//        }
        adapter = new AdaptadorClienteBuscar(getContext());
        adapter.addList(RutasFrag.listaClienteSinCoordenadas);
        rvCliente.setAdapter(adapter);
    }

}
