package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Packing;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PorLiquidarFragment extends Fragment implements View.OnClickListener {


    TextView txtPackingsLiquidar, txtDocsAsignadosLiquidar, txtRBTLiquidar, txtRPLiquidar, txtDocEntregadosLiquidar, txtTotalSuma, txtFalta, txtMontoTotalLiquidar;
    Button btnProductosRebotesLiquidar, btnTerminarLiquidar;
    int cantDocs = 0;
    SwipeRefreshLayout swiperefresh;
    String monto, packings,codEmpresa="",codAlmacen="",fecha="";

    //    @BindView(R.id.swiperefresh)
//    SwipeRefreshLayout swiperefresh;
    ProgressDialog progressDialog;
    private SessionUsuario sessionUsuario;
//    EntregasRecyclerAdapter entregasRecyclerAdapter;

    public static PorLiquidarFragment newInstance() {
        PorLiquidarFragment fragment = new PorLiquidarFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_por_liquidar, container, false);
        ButterKnife.bind(this, rootView);
        sessionUsuario = new SessionUsuario(getContext());
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        txtPackingsLiquidar = (TextView) rootView.findViewById(R.id.txtPackingsLiquidar);
        txtDocsAsignadosLiquidar = (TextView) rootView.findViewById(R.id.txtDocsAsignadosLiquidar);
        txtRBTLiquidar = (TextView) rootView.findViewById(R.id.txtCantRBTLiquidar);
        txtRPLiquidar = (TextView) rootView.findViewById(R.id.txtCantRPTLiquidar);
        txtDocEntregadosLiquidar = (TextView) rootView.findViewById(R.id.txtDocEntregadosLiquidar);
        txtTotalSuma = (TextView) rootView.findViewById(R.id.txtCantTotalLiquidar);
        txtFalta = (TextView) rootView.findViewById(R.id.txtCantFaltaLiquidar);
        txtMontoTotalLiquidar = (TextView) rootView.findViewById(R.id.txtMontoSolesLiquidar);
        swiperefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefreshLiquidar);

        btnProductosRebotesLiquidar = (Button) rootView.findViewById(R.id.btnProductosLiquidar);
        btnTerminarLiquidar = (Button) rootView.findViewById(R.id.btnTerminarLiquidar);
        Metodos.validarSesion(getActivity());
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(getContext(), "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            getActivity().finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(getContext(), "ERROR!", "Error");
            getActivity().finish();
        }

        if (getArguments() != null) {
            packings = getArguments().getString("packings");
            fecha = getArguments().getString("fecha");
//            editor.putString("recarga", "si");
//            editor.commit();
        }
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        cargarPackings(sessionUsuario.getCodigoAplicacion(), packings);
                        swiperefresh.setRefreshing(false);
                    }
                });

        cargarPackings(sessionUsuario.getCodigoAplicacion(), packings);
        btnTerminarLiquidar.setOnClickListener(this);
        btnProductosRebotesLiquidar.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inicio, menu);

    }

    public void cargarPackings(String placa, String packings) {
        Map<String, String> data = new HashMap<>();
        data.put("placa", placa);
        data.put("codsede", sessionUsuario.getCodSede());
        data.put("codemp", codEmpresa);
        data.put("nroPacking", packings);
        data.put("fecha", fecha);
        Gson gson = new Gson();
        String pedidoJson = gson.toJson(data);
        Log.e("EnvoiCargarPacking",pedidoJson);
        Metodos.showProgressDialogWithTitle("Cargando Informacion", progressDialog);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getLiquidarService().infoLiquidarXPlaca(data);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    Log.e("res", response.body().toString());
                    ArrayList array = (ArrayList) response.body().getData();
                    ArrayList cantidadesRS = (ArrayList) array.get(0);//DETALLE DE CANTIDADES POR PLACA
                    ArrayList packingDetalles = (ArrayList) array.get(1);//LA INFO DE PACKINGS
                    Log.e("hay", cantidadesRS.toString());


                    int estado = response.body().getEstado();
                    if (estado == 200) {
                        JSONArray jsonArrayCantidades = new JSONArray(cantidadesRS);
                        JSONArray jsonArray = new JSONArray(packingDetalles);
                        Packing.listaPackings.clear();
                        String packings = "";
                        codEmpresa="";
                        codAlmacen="";
                        int sum = 0;

                        if (jsonArrayCantidades.length() > 0) {
                            for (int i = 0; i < jsonArrayCantidades.length(); i++) {
                                JSONObject objItem = jsonArrayCantidades.getJSONObject(i);
                                Log.e("itemUnico", objItem.toString());
                                txtRPLiquidar.setText("");
                                if (!(objItem.getString("montoSolesLiquidar").isEmpty() || objItem.getString("montoSolesLiquidar") == null || objItem.getString("montoSolesLiquidar").equalsIgnoreCase("null"))) {
                                    txtMontoTotalLiquidar.setText("S/. " + (objItem.getString("montoSolesLiquidar")));
                                    monto = objItem.getString("montoSolesLiquidar");
                                } else {
                                    txtMontoTotalLiquidar.setText("--------");
                                    monto = "--------";
                                }


                                txtDocEntregadosLiquidar.setText(objItem.getString("docsentregadosCantLiquidar"));
                                txtRBTLiquidar.setText(objItem.getString("rbtcantLiquidar"));
                                txtRPLiquidar.setText(objItem.getString("rpcantLiquidar"));
                                int rebTotales = Integer.parseInt(objItem.getString("rbtcantLiquidar"));
                                int rebParciales = Integer.parseInt(objItem.getString("rpcantLiquidar"));
                                int docsEntraga = Integer.parseInt(objItem.getString("docsentregadosCantLiquidar"));
                                sum = rebTotales + docsEntraga;

                                txtTotalSuma.setText(String.valueOf(sum));

                            }
                        }

                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Packing objPacking = new Packing();
                                objPacking.setNro_packing(objItem.getString("nro_Packings"));
                                objPacking.setFecha(objItem.getString("fecha"));
                                objPacking.setMonto_total(objItem.getDouble("montoTotal"));
                                objPacking.setCantDocsxPacking(objItem.getString("cantDocsxPacking"));
                                objPacking.setCodAlmacen(objItem.getString("codAlmacenes"));//PARA SACAR LOS ALMACENES DE AQUI Y ENVIAR PARA LISTAR LOS PEDIDOS
                                objPacking.setCodEmpresa(objItem.getString("codEmpresa"));
                                objPacking.setRutas(objItem.getString("rutas"));
                                objPacking.setIsHoy(objItem.getString("ishoy"));
                                int op = Integer.parseInt(objItem.getString("ishoy"));

                                cantDocs = cantDocs + Integer.valueOf(objItem.getString("cantDocsxPacking"));
                                if (packings.equalsIgnoreCase("")) {
                                    packings = objItem.getString("nro_Packings");
                                } else {
                                    packings = packings + "," + objItem.getString("nro_Packings");
                                }

                                    codEmpresa = objItem.getString("codEmpresa");

                                if (codAlmacen.equalsIgnoreCase("")) {
                                    codAlmacen = objItem.getString("codAlmacenes");
                                } else {
                                    codAlmacen = codAlmacen + "," + objItem.getString("codAlmacenes");
                                }

                                if(txtMontoTotalLiquidar.getText().toString().equalsIgnoreCase("--------")){
                                    monto="0";
                                    txtMontoTotalLiquidar.setText("0000");
                                }


//                    Log.e("BIEN", jsonDatosRutina.getString("identificador_sin"));
                                Log.e("PACKINGS", objItem.getString("nro_Packings"));
                                Packing.listaPackings.add(objPacking);

                            }
                            txtPackingsLiquidar.setText(packings);
                            txtDocsAsignadosLiquidar.setText(String.valueOf(cantDocs));
                            txtFalta.setText(String.valueOf(cantDocs - sum));
                            cantDocs = 0;

                            Metodos.hideProgressDialogWithTitle(progressDialog);
                        } else {
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            MensajesDialogAlert.mensajeInformacion(getContext(), "Información", "No se encontraron datos de cantidades 1");
                            btnProductosRebotesLiquidar.setEnabled(false);
                            btnTerminarLiquidar.setEnabled(false);
                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog);
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontro datos de cantidades");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }

                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(PorLiquidarFragment.this.getActivity(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();

//            showProgress(false);
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnTerminarLiquidar:
                String packing = txtPackingsLiquidar.getText().toString();
                String monto2 = txtMontoTotalLiquidar.getText().toString();
                if (packing.equalsIgnoreCase("-------") || monto2.equalsIgnoreCase("--------") || packing.equalsIgnoreCase("")) {
                    MensajesDialogAlert.mensajeError(getContext(), "ERROR", "El packing(s) o monto en soles cargado no es valido");
                } else {
                    boolean op = MensajesDialogAlert.mensajeConfirmacion(getContext(), "CONFIRMAR", "Seguro que quiere terminar el packing " + packing);
                    if (op) {
                        terminarLiquidar();
                    }
                }

                break;
            case R.id.btnProductosLiquidar:
                InventarioReboteFragment fr = new InventarioReboteFragment();
//                fr.setArguments(bn);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_por_liquidar, fr)
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }


    public void terminarLiquidar() {
        String packing = txtPackingsLiquidar.getText().toString();
        Map<String, String> data = new HashMap<>();
        data.put("placa", sessionUsuario.getCodigoAplicacion());
        data.put("codsede", sessionUsuario.getCodSede());
        data.put("codempresa", codEmpresa);
        data.put("codalmacen", codAlmacen);
        data.put("usuario", sessionUsuario.getNickUsuario());
        data.put("packings", packing);
        data.put("monto", monto);
        data.put("coorX", "");
        data.put("coorY", "");
        Gson gson = new Gson();
        String pedidoJson = gson.toJson(data);
        Log.e("LiquidarData",pedidoJson);
        Metodos.showProgressDialogWithTitle("Terminando para imprimir", progressDialog);

        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getLiquidarService().liquidarCierre(data);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                try {
                    Log.e("res", response.body().toString());


                    int estado = response.body().getEstado();

                    switch (estado) {
                        case 200:
                            //FUNCIONO EL INSERTAR EL TERMINAR PARA LIQUIDAR
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            MensajesDialogAlert.mensajeExito(PorLiquidarFragment.this.getActivity(), "PACKINGS(S) TERMINADOS", "Proceder a imprimir su documento");
                            break;
                        case 405:
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            MensajesDialogAlert.mensajeError(PorLiquidarFragment.this.getActivity(), "ERROR", response.body().getMensaje());
                            break;
                        default:
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            MensajesDialogAlert.mensajeError(PorLiquidarFragment.this.getActivity(), "ERROR 2", response.body().getMensaje());
                            break;
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Metodos.hideProgressDialogWithTitle(progressDialog);
                Toast.makeText(PorLiquidarFragment.this.getActivity(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
            }
        });
    }
}
