package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.toFrameMapa;
import com.legado.distribuciongps.adapters.AdaptadorPedidosXcliente;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Almacen;
import com.legado.distribuciongps.modelo.Cliente;
import com.legado.distribuciongps.modelo.Empresa;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Producto;
import com.legado.distribuciongps.modelo.Sede;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.intListadoPedido;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class pedidosXcliente extends Fragment implements listado_productos.pedidoxclienteListener, intListadoPedido {

    AdaptadorPedidosXcliente adapaterPedidosxClient;
    RecyclerView recyclerPackings;
    TextView txtCliente;
    public static String cliente = "", codcliente = "", empresa = "", almacenes = "", asignaciones = "";
    String direc = "", packings = "";
    public static String placa = "";
    SessionUsuario sesionUsuario;


    SwipeRefreshLayout swiperefresh;
    public static ProgressDialog progressDialog;

    public pedidosXcliente() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pedidos_xcliente, container, false);
        txtCliente = (TextView) v.findViewById(R.id.lblCliente);
        sesionUsuario = new SessionUsuario(getContext());
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefreshPedidos);
        progressDialog = new ProgressDialog(getContext());
//        Metodos.showProgressDialogWithTitle("Cargando Pedidos de Cliente",progressDialog);
        Metodos.validarSesion(getActivity());
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(getContext(), "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            getActivity().finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(getContext(), "ERROR!", "Error");
            getActivity().finish();
        }
        SharedPreferences.Editor editor = RutasFrag.sharedPref.edit();

        editor.putString("recarga","si");
        editor.putString("depaso","si");
        editor.commit();

        recyclerPackings = (RecyclerView) v.findViewById(R.id.recyclerListaPedidos);
        recyclerPackings.setHasFixedSize(true);
        recyclerPackings.setLayoutManager(new LinearLayoutManager(this.getContext()));

        cliente = getArguments().getString("cliente");
        direc = getArguments().getString("direc");
        packings = getArguments().getString("packings");
        codcliente = getArguments().getString("codcliente");
        empresa = getArguments().getString("empresa");
        almacenes = getArguments().getString("almacenes");
        asignaciones = getArguments().getString("asignaciones");
        placa = sesionUsuario.getCodigoAplicacion();
        ((toFrameMapa) getActivity()).hideFloatingActionButton();
        if (cliente.equals("")) {
            MensajesDialogAlert.mensajeError(getContext(), "Parametro error", cliente);
            System.exit(1);
        }
        Metodos.showProgressDialogWithTitle("Cargando Pedidos de Cliente", pedidosXcliente.progressDialog);
        swiperefresh.setOnRefreshListener(
                () -> {
                    cargarPedidos(empresa, almacenes, asignaciones, codcliente, placa);

                    swiperefresh.setRefreshing(false);
                });
//        Metodos.hideProgressDialogWithTitle(progressDialog);
        String titulo = "Cli.: " + cliente + ", " + codcliente + System.getProperty("line.separator") + " Direc.: " + direc + System.getProperty("line.separator") + " Pack.: " + packings;
        txtCliente.setText(titulo);
//        cargarPedidos(empresa, almacenes, asignaciones, codcliente);
        Toast.makeText(getContext(), "codigo: " + codcliente, Toast.LENGTH_LONG);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarPedidos(empresa, almacenes, asignaciones, codcliente, placa);

        Log.e("Frontales", "resume");
    }


    public void cargarPedidos(String empresa, String almacenes, String asignaciones, String codCliente, String placa) {

        Map<String, String> data = new HashMap<>();
        data.put("empresa", empresa);
        data.put("almacenes", almacenes);
        data.put("asignaciones", asignaciones);
        data.put("codCliente", codCliente);
        data.put("placa", placa);
        Gson gson = new Gson();
        String pedidoJson = gson.toJson(data);
        Log.d("PostCliente", pedidoJson);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPedidoService().listarPedidosyProductosXCliente(data);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {
                try {
                    ArrayList array = (ArrayList) response.body().getData();

                    ArrayList nroPedidosUnicos = (ArrayList) array.get(0);//PEDIDOS UNICOS SIN REPETIR
                    ArrayList detalle_pedido = (ArrayList) array.get(1);//EL DETALLE DEL PEDIDO CON PRODUCTOS


                    int estado = response.body().getEstado();
                    if (estado == 200) {
                        ArrayList jsonArrayUnicos = new ArrayList(nroPedidosUnicos);
                        JSONArray jsonArrayDetalle = new JSONArray(detalle_pedido);
                        Pedido.listaPedido.clear();
                        Producto.listaProductos.clear();


                        if (jsonArrayUnicos.size() > 0) {
                            for (int i = 0; i < jsonArrayUnicos.size(); i++) {
                                String objItem = (String) jsonArrayUnicos.get(i);
                                Log.e("itemUnico", objItem);
                                Pedido objPedido = new Pedido();
                                objPedido.setNumPedido(objItem);
                                String montototal = "", seriePre = "", tipoPago = "", flag10 = "", codEmpresa = "", tipoDoc = "", codLoc = "",ruta="",vendNomb="",dirDespacho="";
                                String contaCre="",placa="",codalmacen="",packi="",nroInternoGuia="",fechaEmision="",fechaVencimien="",montoIGVTotal="";
                                String codClienters="",direccEmpresa="",rucEmpresa="";
                                Sede objSede=new Sede();
                                Empresa objEmpresa=new Empresa();
                                Gson gson2 = new Gson();

                                for (int j = 0; j < jsonArrayDetalle.length(); j++) {
                                    JSONObject objJson = jsonArrayDetalle.getJSONObject(j);
                                    Producto objProducto = new Producto();
                                    objProducto.setNombreProducto(objJson.getString("desc_articulo"));
                                    objProducto.setCodProd(objJson.getString("cod_articulo"));
                                    objProducto.setCantProd(objJson.getString("cant_articulo"));
                                    objProducto.setUniMedida(objJson.getString("uniMedida"));
                                    objProducto.setMontoProdUnidad(objJson.getString("montoPrecioXItem"));
                                    objProducto.setMontoDescuentoProd(objJson.getString("montoDescuentoxArticulo"));
                                    objProducto.setMontoIGVxArticulo(objJson.getString("montoIGVxArticulo"));
                                    objProducto.setMontoTotalxProduc(objJson.getString("monto_articulo"));
                                    objProducto.setNroPedido(objJson.getString("numPedido"));
                                    objProducto.setLlevar(true);

                                    if (objJson.getString("numPedido").equals(objItem)) {
                                        String json = objJson.getString("sede");
                                        objSede = gson2.fromJson(json, Sede.class);
                                        String jsonEmpresa = objJson.getString("empresa");
                                        objEmpresa = gson2.fromJson(jsonEmpresa, Empresa.class);
                                        montototal = objJson.getString("montoTotal_guia");
                                        seriePre = objJson.getString("seriePre");
                                        tipoPago = objJson.getString("contaCre");
                                        flag10 = objJson.getString("flag10");
                                        codEmpresa = objJson.getString("codEmpresaTemp");
                                        tipoDoc = objJson.getString("tipDoc");
                                        codLoc = objJson.getString("codLocalidad");
                                        codClienters = objJson.getString("codCliente");
                                        ruta = objJson.getString("ruta");
                                        vendNomb = objJson.getString("vendedorNomb");
                                        dirDespacho = objJson.getString("direccDespacho");
                                        contaCre = objJson.getString("contaCre");
                                        placa = objJson.getString("placa");
                                        codalmacen = objJson.getString("codAlmacen");
                                        packi = objJson.getString("nroPacking");
                                        nroInternoGuia = objJson.getString("nroInternoGuia");
                                        fechaEmision = objJson.getString("fechaEmision");
                                        fechaVencimien = objJson.getString("fechaVencimien");
                                        montoIGVTotal = objJson.getString("montoIGVTotal");
                                        Producto.listaProductos.add(objProducto);
                                    }
                                }
                                Cliente objcliente = new Cliente();
                                objcliente.setCodCliente(codClienters);
                                objcliente.setNomCliente(cliente);
                                objcliente.setDirDespacho(dirDespacho);

                                Almacen objAlmacen =new Almacen();
                                objAlmacen.setCodAlmacen(codalmacen);
                                objPedido.setSede(objSede);
                                objPedido.setEmpresa(objEmpresa);
                                objPedido.setAlmacen(objAlmacen);
                                objPedido.setNroPacking(packi);
                                objPedido.setCliente(objcliente);
                                objPedido.setSeriePre(seriePre);
                                objPedido.setTipoPago(tipoPago);
                                objPedido.setFlag10(flag10);
                                objPedido.setCodEmpresaTemp(codEmpresa);
                                objPedido.setTipDoc(tipoDoc);
                                objPedido.setCodLocalidad(codLoc);
                                objPedido.setRuta(ruta);
                                objPedido.setVendedorNomb(vendNomb);
                                objPedido.setDireccDespacho(dirDespacho);
                                objPedido.setCodCondicion(contaCre);
                                objPedido.setPlaca(placa);
                                objPedido.setNroInternoGuia(nroInternoGuia);
                                objPedido.setFechaEmision(fechaEmision);
                                objPedido.setFechaVencimien(fechaVencimien);
                                objPedido.setMontoIGVTotal(montoIGVTotal);
                                objPedido.setMontoTotal(BigDecimal.valueOf(Double.valueOf(montototal)));

                                Pedido.listaPedido.add(objPedido);
                            }

                            adapaterPedidosxClient = new AdaptadorPedidosXcliente(getActivity(), getContext());
                            adapaterPedidosxClient.addList(Pedido.listaPedido);
                            recyclerPackings.setAdapter(adapaterPedidosxClient);
                            Metodos.hideProgressDialogWithTitle(pedidosXcliente.progressDialog);

//                            recyclerPackings.setVisibility(View.VISIBLE);
//                            txt_lista_vacia.setVisibility(View.GONE);
                        } else {
//                            recyclerPackings.setVisibility(View.GONE);
//                            txt_lista_vacia.setVisibility(View.VISIBLE);
                            Metodos.hideProgressDialogWithTitle(pedidosXcliente.progressDialog);

//                            MensajesDialogAlert.mensajeInformacion(getContext(), "Información", "No se encontraron PEDIDOS 1");
//                            Toast.makeText(pedidosXcliente.this.getContext(), "No se encontraron PEDIDOS 1", Toast.LENGTH_LONG).show();


                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(pedidosXcliente.progressDialog);
//                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron PEDIDOS 2");
//                        Toast.makeText(pedidosXcliente.this.getContext(), "No se encontraron PEDIDOS 2", Toast.LENGTH_LONG).show();
                        Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(pedidosXcliente.progressDialog);
//                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
//                    Toast.makeText(pedidosXcliente.this.getContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS" + e.getMessage());
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Metodos.hideProgressDialogWithTitle(progressDialog);
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }

    @Override
    public void refresh() {
        swiperefresh.setRefreshing(true);
    }


    @Override
    public void REFRESH(String var) {
        Metodos.showProgressDialogWithTitle("RECARGANDO LISTA DE PEDIDOS", pedidosXcliente.progressDialog);
        cargarPedidos(pedidosXcliente.empresa, pedidosXcliente.almacenes, pedidosXcliente.asignaciones, pedidosXcliente.codcliente, pedidosXcliente.placa);


    }
}
