package com.legado.distribuciongps.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.PedidoActivity;
import com.legado.distribuciongps.activities.act_productos;
import com.legado.distribuciongps.adapters.AdaptadorMotivo;
import com.legado.distribuciongps.adapters.AdaptadorProductosXPedido;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Coordenadas;
import com.legado.distribuciongps.modelo.LogReparto;
import com.legado.distribuciongps.modelo.Motivo;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Producto;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.PermissionUtils;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.intListadoPedido;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class listado_productos extends Fragment implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMyLocationButtonClickListener {


    private LocationRequest mLocationRequest;


    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final String TAG = act_productos.class.getSimpleName();
    int contador = 0;
    boolean FLAG_COBRADO;
    ///PARA MENSAJE CON INPUT PARA PEDIR LA CANTIDAD DE COBRO QUE LO INGRESE EL USUARIO
//    private String cantSolesDialog = "";
    //////////////////////////////////////
    private boolean isNetworkLocation, isGPSLocation;
    private String provider;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private LatLng latLng;
    private Location location;
    public boolean flagmant;

    AdaptadorProductosXPedido adapaterPedidosxClient;
    RecyclerView recyclerProductos;
    Producto objProducto;
    String codPedido, montoTotal, flag10, codEmpresaTemp, tipoDoc, seriePre, codLocallidad, codCliente;
    TextView txtNroPedido, txtMontoTotal;
    AdaptadorMotivo adapterMotivo;
    Button btnRebote, btnEntrega, btnCobrar;
    SessionUsuario sessionUsuario;
    String latitudActual, longitudActual;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog2;
    pedidoxclienteListener mListener;

    intListadoPedido mListen2;


    public listado_productos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_listado_productos, container, false);
        sessionUsuario = new SessionUsuario(getActivity());
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog2 = new ProgressDialog(getContext());
        recyclerProductos = (RecyclerView) v.findViewById(R.id.recyclerListaProduc);
        recyclerProductos.setHasFixedSize(true);
        recyclerProductos.setLayoutManager(new LinearLayoutManager(getContext()));
        txtNroPedido = (TextView) v.findViewById(R.id.txtNroPedidoTitulo);
        txtMontoTotal = (TextView) v.findViewById(R.id.txtMontoTitulo);
        btnRebote = (Button) v.findViewById(R.id.btnReboteProduct);
        btnCobrar = (Button) v.findViewById(R.id.btnCobrarProduct);
        btnEntrega = (Button) v.findViewById(R.id.btnEntregaProduc);
        btnRebote.setOnClickListener(this);
        btnEntrega.setOnClickListener(this);
        btnCobrar.setOnClickListener(this);
        mListen2 = new pedidosXcliente();
        initGoogleAPIClient();
        //METODOS DE ADRIAN PARA TOMAR LA UBICACION ACTUAL
        getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Registrar el receptor de difusión para comprobar el estado del GPS.
        LocationManager mListener = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (mListener != null) {
            isGPSLocation = mListener.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkLocation = mListener.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.e("gps, network", String.valueOf(isGPSLocation + "," + isNetworkLocation));
        }
        if (isGPSLocation) {
            provider = LocationManager.GPS_PROVIDER;
        } else if (isNetworkLocation) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else {
            showSettingDialog();
        }
        //Init Google API Client
        checkPermissions();     //Check Permission
        buildGoogleApiClient();

        //METODOS DE ADRIAN PARA TOMAR LA UBICACION ACTUAL

        Gson gson = new Gson();
        String pedidoJson = gson.toJson(objProducto.listaProductos);
        Log.e("PedidoJson", pedidoJson);
        Log.e("LISTAPRODUCTOS", pedidoJson);


        codPedido = getArguments().getString("nroPedido");
        montoTotal = getArguments().getString("montoTotal");
        flag10 = getArguments().getString("flag10");
        codEmpresaTemp = getArguments().getString("codEmpresaTemp");
        tipoDoc = getArguments().getString("tipoDoc");
        seriePre = getArguments().getString("seriePre");
        codLocallidad = getArguments().getString("codLoc");
        codCliente = getArguments().getString("codCliente");
        Log.e("flag10", "listado" + flag10);
        if (flag10.equals("2")) {
            btnEntrega.setEnabled(false);
//            btnEntrega.setBackgroundColor(Color.BLACK);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentregax));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentregax));
            }
//            btnCobrar.setBackgroundColor(getResources().getColor(R.color.amber900));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobro));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobro));
            }
            btnRebote.setEnabled(true);
            btnCobrar.setEnabled(true);
        } else {
//            btnCobrar.setBackgroundColor(Color.BLACK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobrox));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobrox));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentrega));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentrega));
            }
//            btnEntrega.setBackgroundColor(getResources().getColor(R.color.amber900));
            btnCobrar.setEnabled(false);
            btnEntrega.setEnabled(true);
            btnRebote.setEnabled(true);
        }
        txtNroPedido.setText("Pedido: " + codPedido);
        txtMontoTotal.setText("Monto: " + montoTotal);
        Metodos.showProgressDialogWithTitle("CARGANDO PRODUCTOS", progressDialog2);
        inicio();
//        init();


        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected synchronized void buildGoogleApiClient() {
        Log.e("TAG", "buildGoogleApiClient");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.reconnect();
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }


    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("TAG", "onRequestPermissionsResult");
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // Si se cancela la solicitud, las matrices de resultados están vacías.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Si el permiso otorgado muestra el cuadro de diálogo de ubicación si APIClient no es nulo
                    if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();


                } else {
                    Toast.makeText(getActivity(), "Location Permission denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void initGoogleAPIClient() {
        System.out.println(getActivity());
        if (getActivity() != null) {
//            System.out.println(LocationServices.API);
//            System.out.println(mGoogleApiClient);

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e("TAG", "SUCCESS");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("TAG", "RESOLUTION_REQUIRED");
                        try {
                            if (getActivity() != null) {
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            }

                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("TAG", "GPS NO DISPONIBLE");
                        break;
                }
            }
        });
    }


    public static boolean mensajeReboteTotalParcial(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };
        final boolean[] mResult = new boolean[1];
        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        alert.setTitle(titleM);
        alert.setIcon(R.drawable.ic_help_orange_a700_48dp);
        alert.setPositiveButton("TOTAL", (dialog, whichButton) -> {
            mResult[0] = true;
            handler.sendMessage(handler.obtainMessage());
        });
        alert.setNegativeButton("PARCIAL", (dialog, whichButton) -> {
            mResult[0] = false;
            handler.sendMessage(handler.obtainMessage());
        });
        alert.show();


        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult[0];
    }

    public void inicio() {

        ArrayList<Producto> listaFiltrada = new ArrayList<>();
        listaFiltrada.clear();
        for (int i = 0; i < objProducto.listaProductos.size(); i++) {
            Producto item = objProducto.listaProductos.get(i);
            Log.e("Item: ", item.getNombreProducto() + "//" + item.isLlevar());
            Log.e("codPedido: ", codPedido);
            Log.e("codPedidoANTES: ", item.getNroPedido());
            if (codPedido.equals(item.getNroPedido())) {
                Log.e("codPedidoENTRO: ", item.getNroPedido());
                listaFiltrada.add(item);
            }
        }
        adapaterPedidosxClient = new AdaptadorProductosXPedido(getContext());
        adapaterPedidosxClient.addList(listaFiltrada);
        recyclerProductos.setAdapter(adapaterPedidosxClient);
        Metodos.hideProgressDialogWithTitle(progressDialog2);
    }

    public void mantLogReparto(LogReparto logReparto, String mensaje, boolean finishActivity, boolean isCobro) {
        Gson gson = new Gson();

        String logRepartoJson = gson.toJson(logReparto);
        Log.d("assas", logRepartoJson);
        Log.wtf("DATOS LOGREPARTO", logRepartoJson);

        Call<JsonRespuesta<Integer>> call = ApiRetrofit.getInstance().getMetricasService().mantLogReparto(logReparto);
        call.enqueue(new Callback<JsonRespuesta<Integer>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Integer>> call, Response<JsonRespuesta<Integer>> response) {
                if (response.body().getEstado() == -1) {
                    Log.e("errorCobro", response.body().getMensaje());
                    Snackbar.make(getView(), response.body().getMensaje(), Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    }).show();
                    flagmant = false;
                } else {

//                    MensajesDialogAlert.mensajeExito(getContext(), "EXITO", mensaje);

                    if (isCobro) {
                        enableDisablebtn(2);
                        if (btnEntrega.isEnabled()) {
                            enableDisablebtn(2);
                            Timer timer2 = new Timer();
                            TimerTask t2 = new TimerTask() {
                                @Override
                                public void run() {


                                }
                            };
                            timer2.scheduleAtFixedRate(t2, 1000, 8000);
                            String[] serie2 = seriePre.split("-");
                            GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie2[0], serie2[1], getActivity());
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                        } else {
                            Toast.makeText(getContext(), "ocurrio un error", Toast.LENGTH_LONG).show();
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                        }
                    }
                    Toast.makeText(getContext(), "Exito " + mensaje, Toast.LENGTH_LONG).show();
                    flagmant = true;
                    if (finishActivity == true) {
                        flagmant = true;
                        mListen2.REFRESH("EXITO");
                        getActivity().finish();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Integer>> call, Throwable t) {
                Metodos.hideProgressDialogWithTitle(progressDialog2);
                flagmant = false;

                Snackbar.make(getView(), "Problemas de conexion  ", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Cerrar", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        }).show();
            }
        });
        Log.e("call", call.toString());
    }

    @Override
    public void onClick(View v) {
        int op = v.getId();
        getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));
        switch (op) {
            case R.id.btnReboteProduct:
                boolean flag = mensajeReboteTotalParcial(getContext(), "REBOTE", "Presione el tipo de rebote");
                if (flag) {
                    cargarMotivosTotales();//DENTRO DEL METODO DE CARGAR LOS MOTIVOS SE ENCUENTRA EL GUARDAR REBOTE TOTAL
//                    tomarUbicacion();
                } else {
                    paraPedidoParcial();
                }
                break;
            case R.id.btnEntregaProduc:
//                tomarUbicacion();
                String[] serie = seriePre.split("-");
                GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie[0], serie[1], getActivity());

                break;
            case R.id.btnCobrarProduct:
//                tomarUbicacion();

                FLAG_COBRADO = MensajesDialogAlert.mensajeConfirmacion(getContext(), "Confirme el monto", "Confirmar el cobro de " + montoTotal);
                Metodos.showProgressDialogWithTitle("Verificando Ubicación", progressDialog2);
                Coordenadas LL;
                LL = checkMyPermissionLocation();
                if (LL == null) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", "Debe tener prendido el GPS");

                } else {
                    if (FLAG_COBRADO) {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        Metodos.showProgressDialogWithTitle("Procesando cobro", progressDialog2);

                        LogReparto logReparto = new LogReparto();
                        logReparto.setCodEmpresa(codEmpresaTemp);
                        logReparto.setSerie(seriePre.split("-")[0]);
                        logReparto.setPreimpreso(seriePre.split("-")[1]);
                        logReparto.setTipoDoc(tipoDoc);
                        logReparto.setNroPedido(codPedido);
                        String tipoCondicion = "";
                        if (flag10.equals("2")) {
                            tipoCondicion = "CONTADO";
                        } else if (flag10.equals("1")) {
                            tipoCondicion = "CRÉDITO";
                        } else {
                            tipoCondicion = "ADELANTADO";
                        }
                        logReparto.setTipoCondicion(tipoCondicion);
                        logReparto.setUsuario(sessionUsuario.getNickUsuario());
                        logReparto.setMontoDoc(new BigDecimal(montoTotal));
                        logReparto.setMontoCobrado(new BigDecimal(montoTotal));
                        logReparto.setOperacion(1);// registrar cobro
                        logReparto.setPlaca(sessionUsuario.getCodigoAplicacion());
                        logReparto.setCodCliente(codCliente);
                        logReparto.setX(LL.getLatitud());
                        logReparto.setY(LL.getLongitud());
                        mantLogReparto(logReparto, "SE REGISTRÓ EL COBRO CORRECTAMENTE", false, true);

//                        if (btnEntrega.isEnabled()) {
//                            enableDisablebtn(2);
//                            Timer timer2 = new Timer();
//                            TimerTask t2 = new TimerTask() {
//                                @Override
//                                public void run() {
//
//
//                                }
//                            };
//                            timer2.scheduleAtFixedRate(t2, 1000, 6000);
//                            String[] serie2 = seriePre.split("-");
//                            GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie2[0], serie2[1], getActivity());
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        } else {
//                            Toast.makeText(getContext(), "ocurrio un error", Toast.LENGTH_LONG).show();
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        }

//                        btnEntrega.callOnClick();

                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        enableDisablebtn(1);
                    }
                }


                break;

        }
    }
    ////POR CONSIDERAR///
//    public void showDialog(final Coordenadas preVenta) {
//        new AlertDialog.Builder(getContext())
//                .setTitle("CONFIRMACION")
//                .setMessage("Desea realizar la operación?")
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        registrarPreventaUbic(preVenta);
//                        //  System.out.println(preVenta.getCodVendedor()+correlativo);
//
//                    }
//                }).setNegativeButton(android.R.string.no, null).show();
//    }
//
//    public void registrarPreventaUbic(Coordenadas coord) {
//
//        checkMyPermissionLocation();
//    }
    ////POR CONSIDERAR///


    public void enableDisablebtn(int op) {
        //1 CONTADO
        //2 CREDITO
        //HABILITAR ENTREGA Y DESHABILITAR COBRO
        btnCobrar.setEnabled(op == 1 ? true : false);
        btnEntrega.setEnabled(op == 1 ? false : true);
        if (op == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentregax));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentregax));
            }
//            btnEntrega.setBackgroundColor(Color.BLACK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobro));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobro));
            }
//            btnCobrar.setBackgroundColor(getResources().getColor(R.color.amber900));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobrox));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobrox));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentrega));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentrega));
            }
//            btnCobrar.setBackgroundColor(Color.BLACK);
//            btnEntrega.setBackgroundColor(getResources().getColor(R.color.amber900));
        }


    }

    public void paraPedidoParcial() {

        progressDialog.show();
        Map<String, String> dataClientes = new HashMap<>();
        dataClientes.put("codemp", codEmpresaTemp);
        dataClientes.put("codlocal", codLocallidad);
        dataClientes.put("codtipdoc", tipoDoc);
        dataClientes.put("numpedido", codPedido);
        dataClientes.put("numplaca", sessionUsuario.getCodigoAplicacion());
        Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getPedidoService().ObtenerPedido(dataClientes);
        call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Pedido>> call, retrofit2.Response<JsonRespuesta<Pedido>> response) {
                JsonRespuesta<Pedido> respuesta = response.body();
                Pedido pedido = respuesta.getItem();
                Integer estado = respuesta.getEstado();
                if (estado == 1) {
                    Intent intent = new Intent(getActivity(), PedidoActivity.class);
                    PedidoActivity.actPedido(getActivity());
                    Gson gson = new Gson();
                    pedido.getVendedor().setUsuario(sessionUsuario.getNickUsuario());
                    pedido.setNumPedidoRef(codPedido);
                    String pedidoJson = gson.toJson(pedido);
                    Log.e("PedidoJson", pedidoJson);
                    intent.putExtra("pedido", pedidoJson);
                    intent.putExtra("seriePre", seriePre);
                    intent.putExtra("tipoDoc", tipoDoc);
                    startActivity(intent);
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });


    }


    private Coordenadas checkMyPermissionLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Permission Check
            PermissionUtils.requestPermission2(getActivity());
            return null;
        } else {
            //initGoogleLocation(preVenta);
            getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));
            Coordenadas coord = new Coordenadas();
            if (latLng == null) {
//                Snackbar snackbar;
//
//                Snackbar.make(getView(), "ERROR AL TOMAR LA UBICACION ", Snackbar.LENGTH_INDEFINITE)
//                        .setAction("REINTENTAR", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        showSettingDialog();
//                                    }
//                                }
//                        ).setActionTextColor(Color.WHITE).show();
                boolean asd = MensajesDialogAlert.mensajeError(listado_productos.super.getContext(), "REINTENTAR", "Error al tomar las coordenadas");
                if (asd) {
                    showSettingDialog();
                } else {
                    showSettingDialog();
                }
                coord = null;
            } else {
                Log.e("Latitud", String.valueOf(latLng.latitude));
                Log.e("Longitud", String.valueOf(latLng.longitude));
                coord.setLongitud(String.valueOf(latLng.longitude));
                coord.setLatitud(String.valueOf(latLng.latitude));

                Gson gson = new Gson();

                String preventaJson = gson.toJson(coord);
                Log.d("assas", preventaJson);
//            registrarPreventa(coord);
                Log.wtf("COORDENADA REGISTRO", coord.getLatitud());
                //dialogRegistrarVenta.dismiss();
                if (coord.getLatitud().isEmpty() || coord.getLongitud().isEmpty()) {
                    boolean asd = MensajesDialogAlert.mensajeError(listado_productos.super.getContext(), "REINTENTAR", "Error al tomar las coordenadas");
                    if (asd) {
                        checkMyPermissionLocation();
                    } else {
                        checkMyPermissionLocation();
                    }
//                    Snackbar.make(getView(), "ERROR AL TOMAAR LA UBICACION ", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("REINTENTAR", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    checkMyPermissionLocation();
//                                }
//                            }).show();
                }


            }
            return coord;
        }
    }


    public void cargarMotivosTotales() {
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("op", "1");
        Metodos.showProgressDialogWithTitle("Cargando Motivos", progressDialog2);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getMotivoService().listarMotivos(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();


                    int estado = response.body().getEstado();
                    ArrayList<Motivo> objListaMotivo = new ArrayList<>();
                    ArrayList<Object> obj = new ArrayList<>();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Motivo objMotivo = new Motivo();
                                objMotivo.setDescripcion(objItem.getString("descripcion"));
                                objMotivo.setLlave(objItem.getString("llave"));

                                objListaMotivo.add(objMotivo);
                                obj.add(objItem.getString("llave") + ", " + objItem.getString("descripcion"));
                            }

//                            adapterMotivo = new AdaptadorMotivo(getContext());
//                            adapterMotivo.addList(objListaMotivo);
//                            recyclerPackings.setAdapter(adapterPackings);
                            Object[] mStringArray = obj.toArray();
                            String[] objStrings = new String[obj.size()];
                            for (int i = 0; i < obj.size(); i++) {
                                objStrings[i] = String.valueOf(obj.get(i));
                            }
//                            String[] str = mStringArray.keySet().toArray(new String[mStringArray.length]);
                            for (int i = 0; i < mStringArray.length; i++) {
                                Log.e("string is", (String) mStringArray[i]);
//                                Log.e("string is",str[i]);
                            }
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            withItems(objStrings);

                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }


    public void withItems(String[] motivos) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title = "ELEGIR MOTIVO:";
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(titleM)

                .setItems(motivos, (dialog, which) -> {
                    Toast.makeText(getContext(), motivos[which] + " is clicked", Toast.LENGTH_SHORT).show();
                    String[] parts = motivos[which].split(",");
                    Log.e("cliente", parts[0]);
                    Log.e("codcliente", parts[1]);
                    boolean op = MensajesDialogAlert.mensajeConfirmacion(getContext(), "Confirmar", "Rebote total con motivo " + parts[1] + " ?");
                    if (op) {
                        Coordenadas LL;
                        Metodos.showProgressDialogWithTitle("Procesando Rebote Total", progressDialog2);
                        LL = checkMyPermissionLocation();
                        if (LL == null) {
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "ERROR", "Debe tener el GPS prendido");
                        } else {
                            String user = sessionUsuario.getNickUsuario();
                            String[] serie = seriePre.split("-");

                            GuardarReboteTotal(user, codEmpresaTemp, tipoDoc, serie[0], serie[1], parts[0], getActivity(), LL);
                        }


                    }
                });

//        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", null);
//        builder.setNeutralButton("NEUTRAL", null);
//        builder.setPositiveButtonIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
        builder.setIcon(getResources().getDrawable(R.drawable.ic_24px));

        AlertDialog alertDialog = builder.create();

        alertDialog.show();

//        Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//        button.setBackgroundColor(Color.BLACK);
//        button.setPadding(0, 0, 20, 0);
//        button.setTextColor(Color.WHITE);
    }


    public void GuardarEntrega(String user, String codEmpresa, String tipoDoc, String serie, String preImpreso, Activity act) {
        //REGISTRA EL BROADCAST PARA COMPROBAR EL ESTADO DEL GPS

        Coordenadas LL = null;
        LL = checkMyPermissionLocation();
        if (LL == null) {
            MensajesDialogAlert.mensajeError(getContext(), "ERROR", "Debe tener el GPS prendido");
        } else {
            LogReparto logReparto = new LogReparto();
            logReparto.setCodEmpresa(codEmpresa);
            logReparto.setSerie(serie);
            logReparto.setPreimpreso(preImpreso);
            logReparto.setTipoDoc(tipoDoc);
            logReparto.setNroPedido(codPedido);
            String tipoCondicion = "";
            if (flag10.equals("2")) {
                tipoCondicion = "CONTADO";
            } else if (flag10.equals("1")) {
                tipoCondicion = "CRÉDITO";
            } else {
                tipoCondicion = "ADELANTADO";
            }
            logReparto.setPlaca(sessionUsuario.getCodigoAplicacion());
            logReparto.setCodCliente(codCliente);
            logReparto.setTipoCondicion(tipoCondicion);
            logReparto.setUsuario(sessionUsuario.getNickUsuario());
            logReparto.setMontoDoc(new BigDecimal(montoTotal));
            logReparto.setMontoCobrado((flag10.equals("2") ? new BigDecimal(montoTotal) : new BigDecimal(0)));
            logReparto.setX(LL.getLatitud());
            logReparto.setY(LL.getLongitud());
            logReparto.setOperacion(flag10.equals("2") ? 2 : 1);// si es contado solo update sino es insert

            mantLogReparto(logReparto, "SE REGISTRÓ LA ENTREGA CORRECTAMENTE", true, false);

        }


    }


    public void GuardarReboteTotal(String user, String codEmpresa, String tipoDoc, String serie, String preImpreso, String codtipoRebote, Activity act, Coordenadas coord) {
        Rebote objRe = new Rebote();
        objRe.setCodUsuario(user);
        objRe.setCodEmp(codEmpresa);
        objRe.setTipoDoc(tipoDoc);
        objRe.setSerie(serie);
        objRe.setPreimpreso(preImpreso);
        objRe.setTipoRebote(codtipoRebote);
        objRe.setCoorX(coord.getLatitud());
        objRe.setCoorY(coord.getLongitud());
        Gson gson = new Gson();
        String lcJson = gson.toJson(objRe);
        Log.e("GSON PASS REBOTE", lcJson);

        try {
            Call<JsonRespuesta> call = ApiRetrofit.getInstance().getRepartoService().RegistrarRebote(objRe);
            call.enqueue(new Callback<JsonRespuesta>() {
                @Override
                public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                    switch (response.body().getEstado()) {
                        case 1:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeExito(getContext(), "EXITO", response.body().getMensaje());
                            mListen2.REFRESH("EXITO");

                            getActivity().finish();
                            break;
                        case 2:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "ERROR", response.body().getMensaje());
                            break;
                        case 3:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Problemas al registrar el rebote.", response.body().getMensaje());
                            break;
                        case 4:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Ingreso no valido", response.body().getMensaje());
                            break;
                        default:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Comunicarse con Soporte", response.body().getMensaje());
                            break;

                    }
//                    if (response.body().getEstado() == -1) {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeError(getContext(), "ERROR", "NO SE REALIZO EL REBOTE");
////                        getFragmentManager().beginTransaction().remove(listado_productos.this).commit();
//
//                        getActivity().finish();
//                    } else {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeExito(getContext(), "EXITO", "Se realizo el rebote Total");
//                        mListen2.REFRESH("EXITO");
//
//                        getActivity().finish();
//
//
//                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
//                    Snackbar.make(getView(), "Problemas de conexion  ", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("Cerrar", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//
//                                }
//                            }).show();
//                    dialogRegistrarLocal.dismiss();
                }
            });


        } catch (Exception e) {
            Log.e("error registro", e.getMessage());
        }


    }

    public interface pedidoxclienteListener {
        void refresh();

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("TAG", "onLocationChanged");
        this.location = location;

        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Log.e("TAG", latLng.latitude + "-" + latLng.longitude);
    }


    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("TAG", "onConnected");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(9000);
        mLocationRequest.setFastestInterval(9000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        try {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        getActivity().unregisterReceiver(gpsLocationReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(gpsLocationReceiver);
    }


    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect(i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    //Ejecutar en la interfaz de usuario
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };
    public BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Si la acción es la ubicación
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Compruebe si el GPS está encendido o apagado
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //Si el GPS está apagado, muestre el diálogo de ubicación
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    Log.e("About GPS", "GPS is Disabled in your device");
                    latLng = null;
                    showSettingDialog();
//                    getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Registrar el receptor de difusión para comprobar el estado del GPS.

                    // dismiss();
                }

            }
        }
    };
    //////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


}


