package com.legado.distribuciongps.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.DiscoveryActivity;
import com.legado.distribuciongps.activities.PedidoActivity;
import com.legado.distribuciongps.activities.act_productos;
import com.legado.distribuciongps.adapters.AdaptadorMotivo;
import com.legado.distribuciongps.adapters.AdaptadorProductosXPedido;
import com.legado.distribuciongps.adapters.AdaptadorXFacturar;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Coordenadas;
import com.legado.distribuciongps.modelo.LogReparto;
import com.legado.distribuciongps.modelo.Motivo;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Producto;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.PermissionUtils;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.intListadoPedido;
import com.legado.distribuciongps.util.ShowMsg;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.epson.epos2.Epos2Exception;
//import com.epson.epos2.Log;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class pedidoFacturarFragment extends Fragment implements View.OnClickListener, ReceiveListener, ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMyLocationButtonClickListener, AdaptadorXFacturar.cambioBotonGuardar {


    private LocationRequest mLocationRequest;


    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final String TAG = act_productos.class.getSimpleName();
    int contador = 0;
    boolean FLAG_COBRADO;
    ///PARA MENSAJE CON INPUT PARA PEDIR LA CANTIDAD DE COBRO QUE LO INGRESE EL USUARIO
//    private String cantSolesDialog = "";
    //////////////////////////////////////
    private boolean isNetworkLocation, isGPSLocation;
    private String provider;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private LatLng latLng;
    private Location location;
    public boolean flagmant;

    AdaptadorProductosXPedido adapaterPedidosxClient;
    RecyclerView recyclerProductos;
    Producto objProducto;
    String codPedido, montoTotal, flag10, codEmpresaTemp, tipoDoc, seriePre, codLocallidad, codCliente, direccDespacho, nombCliente, vendedorNomb;
    TextView txtNroPedido, txtMontoTotal;
    AdaptadorMotivo adapterMotivo;
    Button btnRebote, btnEntrega, btnCobrar, btnFacturarGuardar, btnInfoImpresora;
    SessionUsuario sessionUsuario;
    String latitudActual, longitudActual;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog2;
    pedidoxclienteListener mListener;
    Pedido objPedido;

    intListadoPedido mListen2;

    View v;
    public static EditText mEditTarget = null;
    public static Printer mPrinter = null;

    ArrayList<Producto> listaFiltrada = new ArrayList<>();


    public pedidoFacturarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_pedido_facturar, container, false);
        sessionUsuario = new SessionUsuario(getActivity());
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog2 = new ProgressDialog(getContext());
        recyclerProductos = (RecyclerView) v.findViewById(R.id.recyclerListaProduc);
        recyclerProductos.setHasFixedSize(true);
        recyclerProductos.setLayoutManager(new LinearLayoutManager(getContext()));
        txtNroPedido = (TextView) v.findViewById(R.id.txtNroPedidoTitulo);
        txtMontoTotal = (TextView) v.findViewById(R.id.txtMontoTitulo);
        btnRebote = (Button) v.findViewById(R.id.btnReboteTotalFacturar);
//        btnCobrar = (Button) v.findViewById(R.id.btnCobrarProduct);
//        btnEntrega = (Button) v.findViewById(R.id.btnEntregaProduc);
        btnFacturarGuardar = (Button) v.findViewById(R.id.btnFacturarPedido);
        btnInfoImpresora = (Button) v.findViewById(R.id.btnInfoPedidoFacturar);
        mEditTarget = (EditText) v.findViewById(R.id.edtTarget);
        btnRebote.setOnClickListener(this);
        //btnEntrega.setOnClickListener(this);
        //btnCobrar.setOnClickListener(this);
        btnFacturarGuardar.setOnClickListener(this);
        btnInfoImpresora.setOnClickListener(this);
        mListen2 = new pedidosXcliente();
        initGoogleAPIClient();
        //METODOS DE ADRIAN PARA TOMAR LA UBICACION ACTUAL
        getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Registrar el receptor de difusión para comprobar el estado del GPS.
        LocationManager mListener = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (mListener != null) {
            isGPSLocation = mListener.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkLocation = mListener.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.e("gps, network", String.valueOf(isGPSLocation + "," + isNetworkLocation));
        }
        if (isGPSLocation) {
            provider = LocationManager.GPS_PROVIDER;
        } else if (isNetworkLocation) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else {
            showSettingDialog();
        }

        //Init Google API Client
        checkPermissions();     //Check Permission
        buildGoogleApiClient();

        //METODOS DE ADRIAN PARA TOMAR LA UBICACION ACTUAL

        Gson gson = new Gson();
        String pedidoJson = gson.toJson(objProducto.listaProductos);
        Log.e("PedidoJson", pedidoJson);
        Log.e("LISTAPRODUCTOS", pedidoJson);

        Gson gson2 = new Gson();
        String json = getArguments().getString("objPedido");
        objPedido = gson2.fromJson(json, Pedido.class);
        Log.e("flag10", "listado" + flag10);
        codPedido = getArguments().getString("nroPedido");
        montoTotal = getArguments().getString("montoTotal");
        flag10 = getArguments().getString("flag10");
        codEmpresaTemp = getArguments().getString("codEmpresaTemp");
        tipoDoc = getArguments().getString("tipoDoc");
        seriePre = getArguments().getString("seriePre");
        codLocallidad = getArguments().getString("codLoc");
        codCliente = getArguments().getString("codCliente");
        direccDespacho = getArguments().getString("direcc_despacho");
        vendedorNomb = getArguments().getString("vendedorNomb");
        nombCliente = getArguments().getString("nombCliente");
        if (objPedido.getSeriePre().contains("NO FACTURADO")) {
            btnFacturarGuardar.setText("FACTURAR");
        } else {
            btnFacturarGuardar.setText("IMPRIMIR");
        }

        txtNroPedido.setText("Pedido: " + codPedido);
        txtMontoTotal.setText("Monto: " + montoTotal);
        Metodos.showProgressDialogWithTitle("CARGANDO PRODUCTOS", progressDialog2);
        inicio();
//        init();


        return v;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected synchronized void buildGoogleApiClient() {
        Log.e("TAG", "buildGoogleApiClient");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.reconnect();
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }


    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("TAG", "onRequestPermissionsResult");
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // Si se cancela la solicitud, las matrices de resultados están vacías.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Si el permiso otorgado muestra el cuadro de diálogo de ubicación si APIClient no es nulo
                    if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();


                } else {
                    Toast.makeText(getActivity(), "Location Permission denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void initGoogleAPIClient() {
        System.out.println(getActivity());
        if (getActivity() != null) {
//            System.out.println(LocationServices.API);
//            System.out.println(mGoogleApiClient);

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e("TAG", "SUCCESS");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("TAG", "RESOLUTION_REQUIRED");
                        try {
                            if (getActivity() != null) {
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            }

                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("TAG", "GPS NO DISPONIBLE");
                        break;
                }
            }
        });
    }


    public static boolean mensajeReboteTotalParcial(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };
        final boolean[] mResult = new boolean[1];
        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        alert.setTitle(titleM);
        alert.setIcon(R.drawable.ic_help_orange_a700_48dp);
        alert.setPositiveButton("TOTAL", (dialog, whichButton) -> {
            mResult[0] = true;
            handler.sendMessage(handler.obtainMessage());
        });
        alert.setNegativeButton("PARCIAL", (dialog, whichButton) -> {
            mResult[0] = false;
            handler.sendMessage(handler.obtainMessage());
        });
        alert.show();


        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult[0];
    }

    public void inicio() {

        //ArrayList<Producto> listaFiltrada = new ArrayList<>();
        listaFiltrada.clear();
        for (int i = 0; i < objProducto.listaProductos.size(); i++) {
            Producto item = objProducto.listaProductos.get(i);
            Log.e("Item: ", item.getNombreProducto() + "//" + item.isLlevar());
            Log.e("codPedido: ", codPedido);
            Log.e("codPedidoANTES: ", item.getNroPedido());
            if (codPedido.equals(item.getNroPedido())) {
                Log.e("codPedidoENTRO: ", item.getNroPedido());
                listaFiltrada.add(item);
            }
        }
        adapaterPedidosxClient = new AdaptadorProductosXPedido(getContext());
        adapaterPedidosxClient.addList(listaFiltrada);
        recyclerProductos.setAdapter(adapaterPedidosxClient);
        Metodos.hideProgressDialogWithTitle(progressDialog2);
    }

    public void mantLogReparto(LogReparto logReparto, String mensaje, boolean finishActivity, boolean isCobro) {
        Gson gson = new Gson();

        String logRepartoJson = gson.toJson(logReparto);
        Log.d("assas", logRepartoJson);
        Log.wtf("DATOS LOGREPARTO", logRepartoJson);

        Call<JsonRespuesta<Integer>> call = ApiRetrofit.getInstance().getMetricasService().mantLogReparto(logReparto);
        call.enqueue(new Callback<JsonRespuesta<Integer>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Integer>> call, Response<JsonRespuesta<Integer>> response) {
                if (response.body().getEstado() == -1) {
                    Log.e("errorCobro", response.body().getMensaje());
                    Snackbar.make(getView(), response.body().getMensaje(), Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    }).show();
                    flagmant = false;
                } else {

//                    MensajesDialogAlert.mensajeExito(getContext(), "EXITO", mensaje);

                    if (isCobro) {
                        enableDisablebtn(2);
                        if (btnEntrega.isEnabled()) {
                            enableDisablebtn(2);
                            Timer timer2 = new Timer();
                            TimerTask t2 = new TimerTask() {
                                @Override
                                public void run() {


                                }
                            };
                            timer2.scheduleAtFixedRate(t2, 1000, 8000);
                            String[] serie2 = seriePre.split("-");
                            GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie2[0], serie2[1], getActivity());
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                        } else {
                            Toast.makeText(getContext(), "ocurrio un error", Toast.LENGTH_LONG).show();
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                        }
                    }
                    Toast.makeText(getContext(), "Exito " + mensaje, Toast.LENGTH_LONG).show();
                    flagmant = true;
                    if (finishActivity == true) {
                        flagmant = true;
                        mListen2.REFRESH("EXITO");
                        getActivity().finish();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Integer>> call, Throwable t) {
                Metodos.hideProgressDialogWithTitle(progressDialog2);
                flagmant = false;

                Snackbar.make(getView(), "Problemas de conexion  ", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Cerrar", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        }).show();
            }
        });
        Log.e("call", call.toString());
    }

    @Override
    public void onClick(View v) {
        int op = v.getId();
        getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));
        switch (op) {
            case R.id.btnInfoPedidoFacturar:
                Intent intent = new Intent(getContext(), DiscoveryActivity.class);
                getActivity().startActivityForResult(intent, 0);
                break;
            case R.id.btnFacturarPedido:
                if (btnFacturarGuardar.getText().toString().equalsIgnoreCase("IMPRIMIR") &&
                        !(objPedido.getSeriePre().equalsIgnoreCase("NO FACTURADO"))) {
                    updateButtonState(false);
                    if (!runPrintReceiptSequence()) {
                        updateButtonState(true);
                    }
                } else {
                    boolean facturar = MensajesDialogAlert.mensajeConfirmacion(getContext(), "FACTURAR", "Facturar Pedido?");
                    if (facturar) {
                        Metodos.showProgressDialogWithTitle("FACTURANDO", progressDialog2);
                        FacturarPedido(codEmpresaTemp, codEmpresaTemp, objPedido.getTipDoc(), getActivity());
                    }

                }


//                updateButtonState(false);
//                if (!runPrintReceiptSequence()) {
//                    updateButtonState(true);
//                }
                break;
            case R.id.btnReboteTotalFacturar:
                boolean flag = mensajeReboteTotalParcial(getContext(), "REBOTE", "Presione el tipo de rebote");
                if (flag) {
                    cargarMotivosTotales();//DENTRO DEL METODO DE CARGAR LOS MOTIVOS SE ENCUENTRA EL GUARDAR REBOTE TOTAL
//                    tomarUbicacion();
                } else {
                    paraPedidoParcial();
                }
                break;
            case R.id.btnEntregaProduc:
//                tomarUbicacion();
                String[] serie = seriePre.split("-");
                GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie[0], serie[1], getActivity());

                break;
            case R.id.btnCobrarProduct:
//                tomarUbicacion();

                FLAG_COBRADO = MensajesDialogAlert.mensajeConfirmacion(getContext(), "Confirme el monto", "Confirmar el cobro de " + montoTotal);
                Metodos.showProgressDialogWithTitle("Verificando Ubicación", progressDialog2);
                Coordenadas LL;
                LL = checkMyPermissionLocation();
                if (LL == null) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", "Debe tener prendido el GPS");

                } else {
                    if (FLAG_COBRADO) {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        Metodos.showProgressDialogWithTitle("Procesando cobro", progressDialog2);

                        LogReparto logReparto = new LogReparto();
                        logReparto.setCodEmpresa(codEmpresaTemp);
                        logReparto.setSerie(seriePre.split("-")[0]);
                        logReparto.setPreimpreso(seriePre.split("-")[1]);
                        logReparto.setTipoDoc(tipoDoc);
                        logReparto.setNroPedido(codPedido);
                        String tipoCondicion = "";
                        if (flag10.equals("2")) {
                            tipoCondicion = "CONTADO";
                        } else if (flag10.equals("1")) {
                            tipoCondicion = "CRÉDITO";
                        } else {
                            tipoCondicion = "ADELANTADO";
                        }
                        logReparto.setTipoCondicion(tipoCondicion);
                        logReparto.setUsuario(sessionUsuario.getNickUsuario());
                        logReparto.setMontoDoc(new BigDecimal(montoTotal));
                        logReparto.setMontoCobrado(new BigDecimal(montoTotal));
                        logReparto.setOperacion(1);// registrar cobro
                        logReparto.setPlaca(sessionUsuario.getCodigoAplicacion());
                        logReparto.setCodCliente(codCliente);
                        logReparto.setX(LL.getLatitud());
                        logReparto.setY(LL.getLongitud());
                        mantLogReparto(logReparto, "SE REGISTRÓ EL COBRO CORRECTAMENTE", false, true);

//                        if (btnEntrega.isEnabled()) {
//                            enableDisablebtn(2);
//                            Timer timer2 = new Timer();
//                            TimerTask t2 = new TimerTask() {
//                                @Override
//                                public void run() {
//
//
//                                }
//                            };
//                            timer2.scheduleAtFixedRate(t2, 1000, 6000);
//                            String[] serie2 = seriePre.split("-");
//                            GuardarEntrega(sessionUsuario.getNickUsuario(), codEmpresaTemp, tipoDoc, serie2[0], serie2[1], getActivity());
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        } else {
//                            Toast.makeText(getContext(), "ocurrio un error", Toast.LENGTH_LONG).show();
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        }

//                        btnEntrega.callOnClick();

                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        enableDisablebtn(1);
                    }
                }


                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == -1) {
            String target = data.getStringExtra(getString(R.string.title_target));
            EditText mEdtTarget = (EditText) v.findViewById(R.id.edtTarget);
            sessionUsuario.guardaImpresora(target);
            Toast.makeText(getContext(), "IMPRESORA ENCONTRADA " + target, Toast.LENGTH_LONG);
            mEdtTarget.setText(target);
            if (target != null) {

            }
        }
    }

    ////POR CONSIDERAR///
//    public void showDialog(final Coordenadas preVenta) {
//        new AlertDialog.Builder(getContext())
//                .setTitle("CONFIRMACION")
//                .setMessage("Desea realizar la operación?")
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        registrarPreventaUbic(preVenta);
//                        //  System.out.println(preVenta.getCodVendedor()+correlativo);
//
//                    }
//                }).setNegativeButton(android.R.string.no, null).show();
//    }
//
//    public void registrarPreventaUbic(Coordenadas coord) {
//
//        checkMyPermissionLocation();
//    }
    ////POR CONSIDERAR///
    private boolean runPrintReceiptSequence() {
        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }

    private boolean initializeObject() {
        try {
            mPrinter = new Printer(0, 0, getContext());
        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", getContext());
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), getContext());
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", getContext());
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    private boolean connectPrinter() {
        boolean isBeginTransaction = false;

        if (mPrinter == null) {
            return false;
        }

        try {
            String impres = sessionUsuario.getImpresora();
            if (impres.isEmpty()||impres.equals(null)||impres.equalsIgnoreCase("")){
                throw new Exception("IMPRESORA NO INGRESADA, ESCANEAR POR FAVOR");
            }else{
                mPrinter.connect(impres, Printer.PARAM_DEFAULT);
            }
//            mPrinter.connect(mEditTarget.getText().toString(), Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            ShowMsg.showException(e, "connect", getContext());
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", getContext());
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void enableDisablebtn(int op) {
        //1 CONTADO
        //2 CREDITO
        //HABILITAR ENTREGA Y DESHABILITAR COBRO
        btnCobrar.setEnabled(op == 1 ? true : false);
        btnEntrega.setEnabled(op == 1 ? false : true);
        if (op == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentregax));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentregax));
            }
//            btnEntrega.setBackgroundColor(Color.BLACK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobro));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobro));
            }
//            btnCobrar.setBackgroundColor(getResources().getColor(R.color.amber900));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnCobrar.setBackground(getContext().getDrawable(R.drawable.btncobrox));
            } else {
                btnCobrar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btncobrox));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrega.setBackground(getContext().getDrawable(R.drawable.btnentrega));
            } else {
                btnEntrega.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btnentrega));
            }
//            btnCobrar.setBackgroundColor(Color.BLACK);
//            btnEntrega.setBackgroundColor(getResources().getColor(R.color.amber900));
        }


    }

    public void paraPedidoParcial() {

        progressDialog.show();
        Map<String, String> dataClientes = new HashMap<>();
        dataClientes.put("codemp", codEmpresaTemp);
        dataClientes.put("codlocal", codLocallidad);
        dataClientes.put("codtipdoc", tipoDoc);
        dataClientes.put("numpedido", codPedido);
        dataClientes.put("numplaca", sessionUsuario.getCodigoAplicacion());
        Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getPedidoService().ObtenerPedido(dataClientes);
        call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Pedido>> call, Response<JsonRespuesta<Pedido>> response) {
                JsonRespuesta<Pedido> respuesta = response.body();
                Pedido pedido = respuesta.getItem();
                Integer estado = respuesta.getEstado();
                if (estado == 1) {
                    Intent intent = new Intent(getActivity(), PedidoActivity.class);
                    PedidoActivity.actPedido(getActivity());
                    Gson gson = new Gson();
                    pedido.getVendedor().setUsuario(sessionUsuario.getNickUsuario());
                    pedido.setNumPedidoRef(codPedido);
                    String pedidoJson = gson.toJson(pedido);
                    Log.e("PedidoJson", pedidoJson);
                    intent.putExtra("pedido", pedidoJson);
                    intent.putExtra("seriePre", seriePre);
                    intent.putExtra("tipoDoc", tipoDoc);
                    startActivity(intent);
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });


    }


    private Coordenadas checkMyPermissionLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Permission Check
            PermissionUtils.requestPermission2(getActivity());
            return null;
        } else {
            //initGoogleLocation(preVenta);
            getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));
            Coordenadas coord = new Coordenadas();
            if (latLng == null) {
//                Snackbar snackbar;
//
//                Snackbar.make(getView(), "ERROR AL TOMAR LA UBICACION ", Snackbar.LENGTH_INDEFINITE)
//                        .setAction("REINTENTAR", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        showSettingDialog();
//                                    }
//                                }
//                        ).setActionTextColor(Color.WHITE).show();
                boolean asd = MensajesDialogAlert.mensajeError(pedidoFacturarFragment.super.getContext(), "REINTENTAR", "Error al tomar las coordenadas");
                if (asd) {
                    showSettingDialog();
                } else {
                    showSettingDialog();
                }
                coord = null;
            } else {
                Log.e("Latitud", String.valueOf(latLng.latitude));
                Log.e("Longitud", String.valueOf(latLng.longitude));
                coord.setLongitud(String.valueOf(latLng.longitude));
                coord.setLatitud(String.valueOf(latLng.latitude));

                Gson gson = new Gson();

                String preventaJson = gson.toJson(coord);
                Log.d("assas", preventaJson);
//            registrarPreventa(coord);
                Log.wtf("COORDENADA REGISTRO", coord.getLatitud());
                //dialogRegistrarVenta.dismiss();
                if (coord.getLatitud().isEmpty() || coord.getLongitud().isEmpty()) {
                    boolean asd = MensajesDialogAlert.mensajeError(pedidoFacturarFragment.super.getContext(), "REINTENTAR", "Error al tomar las coordenadas");
                    if (asd) {
                        checkMyPermissionLocation();
                    } else {
                        checkMyPermissionLocation();
                    }
//                    Snackbar.make(getView(), "ERROR AL TOMAAR LA UBICACION ", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("REINTENTAR", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    checkMyPermissionLocation();
//                                }
//                            }).show();
                }


            }
            return coord;
        }
    }


    public void cargarMotivosTotales() {
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("op", "1");
        Metodos.showProgressDialogWithTitle("Cargando Motivos", progressDialog2);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getMotivoService().listarMotivos(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();


                    int estado = response.body().getEstado();
                    ArrayList<Motivo> objListaMotivo = new ArrayList<>();
                    ArrayList<Object> obj = new ArrayList<>();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Motivo objMotivo = new Motivo();
                                objMotivo.setDescripcion(objItem.getString("descripcion"));
                                objMotivo.setLlave(objItem.getString("llave"));

                                objListaMotivo.add(objMotivo);
                                obj.add(objItem.getString("llave") + ", " + objItem.getString("descripcion"));
                            }

//                            adapterMotivo = new AdaptadorMotivo(getContext());
//                            adapterMotivo.addList(objListaMotivo);
//                            recyclerPackings.setAdapter(adapterPackings);
                            Object[] mStringArray = obj.toArray();
                            String[] objStrings = new String[obj.size()];
                            for (int i = 0; i < obj.size(); i++) {
                                objStrings[i] = String.valueOf(obj.get(i));
                            }
//                            String[] str = mStringArray.keySet().toArray(new String[mStringArray.length]);
                            for (int i = 0; i < mStringArray.length; i++) {
                                Log.e("string is", (String) mStringArray[i]);
//                                Log.e("string is",str[i]);
                            }
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            withItems(objStrings);

                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }


    public void withItems(String[] motivos) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title = "ELEGIR MOTIVO:";
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(titleM)

                .setItems(motivos, (dialog, which) -> {
                    Toast.makeText(getContext(), motivos[which] + " seleciono", Toast.LENGTH_SHORT).show();
                    String[] parts = motivos[which].split(",");
                    Log.e("cliente", parts[0]);
                    Log.e("codcliente", parts[1]);
                    boolean op = MensajesDialogAlert.mensajeConfirmacion(getContext(), "Confirmar", "Rebote total con motivo " + parts[1] + " ?");
                    if (op) {
                        Coordenadas LL;
                        Metodos.showProgressDialogWithTitle("Procesando Rebote Total", progressDialog2);
                        LL = checkMyPermissionLocation();
                        if (LL == null) {
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "ERROR", "Debe tener el GPS prendido");
                        } else {
                            String user = sessionUsuario.getNickUsuario();
                            String[] serie = seriePre.split("-");

                            GuardarReboteTotal(user, codEmpresaTemp, tipoDoc, serie[0], serie[1], parts[0], getActivity(), LL);
                        }


                    }
                });

//        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", null);
//        builder.setNeutralButton("NEUTRAL", null);
//        builder.setPositiveButtonIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
        builder.setIcon(getResources().getDrawable(R.drawable.ic_24px));

        AlertDialog alertDialog = builder.create();

        alertDialog.show();

//        Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//        button.setBackgroundColor(Color.BLACK);
//        button.setPadding(0, 0, 20, 0);
//        button.setTextColor(Color.WHITE);
    }


    public void GuardarEntrega(String user, String codEmpresa, String tipoDoc, String serie, String preImpreso, Activity act) {
        //REGISTRA EL BROADCAST PARA COMPROBAR EL ESTADO DEL GPS

        Coordenadas LL = null;
        LL = checkMyPermissionLocation();
        if (LL == null) {
            MensajesDialogAlert.mensajeError(getContext(), "ERROR", "Debe tener el GPS prendido");
        } else {
            LogReparto logReparto = new LogReparto();
            logReparto.setCodEmpresa(codEmpresa);
            logReparto.setSerie(serie);
            logReparto.setPreimpreso(preImpreso);
            logReparto.setTipoDoc(tipoDoc);
            logReparto.setNroPedido(codPedido);
            String tipoCondicion = "";
            if (flag10.equals("2")) {
                tipoCondicion = "CONTADO";
            } else if (flag10.equals("1")) {
                tipoCondicion = "CRÉDITO";
            } else {
                tipoCondicion = "ADELANTADO";
            }
            logReparto.setPlaca(sessionUsuario.getCodigoAplicacion());
            logReparto.setCodCliente(codCliente);
            logReparto.setTipoCondicion(tipoCondicion);
            logReparto.setUsuario(sessionUsuario.getNickUsuario());
            logReparto.setMontoDoc(new BigDecimal(montoTotal));
            logReparto.setMontoCobrado((flag10.equals("2") ? new BigDecimal(montoTotal) : new BigDecimal(0)));
            logReparto.setX(LL.getLatitud());
            logReparto.setY(LL.getLongitud());
            logReparto.setOperacion(flag10.equals("2") ? 2 : 1);// si es contado solo update sino es insert

            mantLogReparto(logReparto, "SE REGISTRÓ LA ENTREGA CORRECTAMENTE", true, false);

        }


    }


    public void GuardarReboteTotal(String user, String codEmpresa, String tipoDoc, String serie, String preImpreso, String codtipoRebote, Activity act, Coordenadas coord) {
        Rebote objRe = new Rebote();
        objRe.setCodUsuario(user);
        objRe.setCodEmp(codEmpresa);
        objRe.setTipoDoc(tipoDoc.equalsIgnoreCase("NO DEFINIDO") ? "0" : tipoDoc);
        objRe.setSerie(serie.equalsIgnoreCase("NO FACTURADO") ? null : serie);
        objRe.setPreimpreso(preImpreso.equalsIgnoreCase("NO FACTURADO") ? null : preImpreso);
        objRe.setTipoRebote(codtipoRebote);
        objRe.setCoorX(coord.getLatitud());
        objRe.setCoorY(coord.getLongitud());
        Gson gson = new Gson();
        String lcJson = gson.toJson(objRe);
        Log.e("GSON PASS", lcJson);


        try {

            Call<JsonRespuesta> call = ApiRetrofit.getInstance().getRepartoService().RegistrarRebote(objRe);
            call.enqueue(new Callback<JsonRespuesta>() {
                @Override
                public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                    switch (response.body().getEstado()) {
                        case 1:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeExito(getContext(), "EXITO", response.body().getMensaje());
                            mListen2.REFRESH("EXITO");

                            getActivity().finish();
                            break;
                        case 2:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "ERROR", response.body().getMensaje());
                            break;
                        case 3:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Problemas al registrar el rebote.", response.body().getMensaje());
                            break;
                        case 4:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Ingreso no valido", response.body().getMensaje());
                            break;
                        default:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Comunicarse con Soporte", response.body().getMensaje());
                            break;

                    }
//                    if (response.body().getEstado() == -1) {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeError(getContext(), "ERROR", "NO SE REALIZO EL REBOTE");
////                        getFragmentManager().beginTransaction().remove(listado_productos.this).commit();
//
//                        getActivity().finish();
//                    } else {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeExito(getContext(), "EXITO", "Se realizo el rebote Total");
//                        mListen2.REFRESH("EXITO");
//
//                        getActivity().finish();
//
//
//                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
//                    Snackbar.make(getView(), "Problemas de conexion  ", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("Cerrar", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//
//                                }
//                            }).show();
//                    dialogRegistrarLocal.dismiss();
                }
            });


        } catch (Exception e) {
            Log.e("error registro", e.getMessage());
        }


    }

    //===============================================================
    //=======================PARA FACTURAR=====================
    public void FacturarPedido(String user, String codEmpresa, String tipoDoc, Activity act) {
        Rebote objRe = new Rebote();
        objRe.setCodUsuario(user);
        objRe.setCodEmp(codEmpresa);
        objRe.setTipoDoc(tipoDoc.equalsIgnoreCase("NO DEFINIDO") ? "0" : tipoDoc);
//        objRe.setSerie(serie.equalsIgnoreCase("NO FACTURADO")?null:serie);
//        objRe.setPreimpreso(preImpreso.equalsIgnoreCase("NO FACTURADO")?null:preImpreso);
//        objRe.setTipoRebote(codtipoRebote);
//        objRe.setCoorX(coord.getLatitud());
//        objRe.setCoorY(coord.getLongitud());

        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("codemp", codEmpresa);
        dataConsulta.put("local", objPedido.getCodLocalidad());//FALTA AGREGAR
        dataConsulta.put("codpack", objPedido.getNroPacking());
        dataConsulta.put("almacen", objPedido.getAlmacen().getCodAlmacen());
        dataConsulta.put("codsed", objPedido.getSede().getCodSede());
        dataConsulta.put("nro_pedido", objPedido.getNumPedido());
        dataConsulta.put("user", sessionUsuario.getNickUsuario());
        dataConsulta.put("nrointerguia", objPedido.getNroInternoGuia());

        Gson gson = new Gson();
        String lcJson = gson.toJson(dataConsulta);
        Log.e("GSON PASS", lcJson);

        try {

            Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPedidoService().FacturarPedido(dataConsulta);
            call.enqueue(new Callback<JsonRespuesta>() {
                @Override
                public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {
                    JsonRespuesta rptaClientes = response.body();
                    Integer estado = rptaClientes.getEstado();
                    Log.e("rptaCliente", rptaClientes.toString());
                    switch (response.body().getEstado()) {
                        case 200:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                            MensajesDialogAlert.mensajeExito(getContext(), "EXITO", response.body().getMensaje());
                            mListen2.REFRESH("EXITO");
                            List objList = (List) response.body().getData();
                            objPedido.setSeriePre(objList.get(3).toString() + "-" + objList.get(4).toString());
                            objPedido.setTipDoc(objList.get(0).toString());
                            objPedido.setMontoIGVTotal(objList.get(6).toString());
                            objPedido.setFechaEmision(objList.get(8).toString());
                            objPedido.setFechaVencimien(objList.get(9).toString());
                            objPedido.setPlaca(objList.get(10).toString());
                            MensajesDialogAlert.mensajeExito(getContext(), "EXITO", response.body().getMensaje() + " " + objPedido.getSeriePre());
                            btnFacturarGuardar.setText("IMPRIMIR");
                            //getActivity().finish();
                            break;
                        case 2:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "ERROR", response.body().getMensaje());
                            break;
                        case 3:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Problemas al registrar la factura.", response.body().getMensaje());
                            break;
                        case 4:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Ingreso no valido", response.body().getMensaje());
                            break;
                        default:
                            Metodos.hideProgressDialogWithTitle(progressDialog2);
                            MensajesDialogAlert.mensajeError(getContext(), "Comunicarse con Soporte", response.body().getMensaje());
                            break;

                    }
//                    if (response.body().getEstado() == -1) {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeError(getContext(), "ERROR", "NO SE REALIZO EL REBOTE");
////                        getFragmentManager().beginTransaction().remove(listado_productos.this).commit();
//
//                        getActivity().finish();
//                    } else {
//                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        MensajesDialogAlert.mensajeExito(getContext(), "EXITO", "Se realizo el rebote Total");
//                        mListen2.REFRESH("EXITO");
//
//                        getActivity().finish();
//
//
//                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
//                    Snackbar.make(getView(), "Problemas de conexion  ", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("Cerrar", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//
//                                }
//                            }).show();
//                    dialogRegistrarLocal.dismiss();
                }
            });


        } catch (Exception e) {
            Log.e("error registro", e.getMessage());
        }


    }
    //===============================================================
    //===============================================================


    @Override
    public void cambiobtnFactGuardar() {
        btnFacturarGuardar.setText("SADFSAD");
    }

    private String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }


    private boolean createReceiptData() {
        String method = "";
        Bitmap logoData = BitmapFactory.decodeResource(getResources(), R.drawable.gp3);
        String tipoDocForSunat;
        String tipoDocClienteForSunat;
        String[] seriep = objPedido.getSeriePre().split("-");
        if (objPedido.getTipDoc().equals("02")) {
            //BOLETA
            tipoDocForSunat = "03";
            tipoDocClienteForSunat = "1";
        } else {
            //FACTURA
            tipoDocForSunat = objPedido.getTipDoc();
            tipoDocClienteForSunat = "6";
        }


        Bitmap qrCode = creaQR(objPedido.getEmpresa().getRuc() + "|" + tipoDocForSunat + "|"
                + seriep[0] + "|" + seriep[1] + "|" + objPedido.getMontoIGVTotal() + "|" + objPedido.getMontoTotal()
                + "|" + objPedido.getFechaEmision() + "|" + tipoDocClienteForSunat + "|" + objPedido.getCliente().getCodCliente());
        StringBuilder textData = new StringBuilder();


        if (mPrinter == null) {
            return false;
        }

        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);

            method = "addImage";
            mPrinter.addImage(logoData, 0, 0,
                    logoData.getWidth(),
                    logoData.getHeight(),
                    Printer.COLOR_1,
                    Printer.MODE_MONO,
                    Printer.HALFTONE_DITHER,
                    Printer.PARAM_DEFAULT,
                    Printer.COMPRESS_AUTO);


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);

            if (objPedido.getTipDoc().equalsIgnoreCase("02")) {
                textData.append("BOLETA ELECTRONICA" + "\n");
            } else {
                textData.append("FACTURA ELECTRONICA" + "\n");
            }


            if (objPedido.getTipDoc().equalsIgnoreCase("02")) {
                textData.append("B" + objPedido.getSeriePre() + "\n");
            } else {
                textData.append("F" + objPedido.getSeriePre() + "\n");
            }
            textData.append(objPedido.getEmpresa().getDescEmpresa() + " " + objPedido.getEmpresa().getRuc() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Dir.F:" + objPedido.getEmpresa().getDireccion() + "\n");
            mPrinter.addFeedLine(1);
            textData.append("RUTA:" + objPedido.getRuta());
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("       PLACA:" + objPedido.getPlaca());
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("VENDEDOR:" + objPedido.getVendedorNomb() + "\n");
            textData.append("NOMB/RAZON:" + objPedido.getCliente().getNomCliente() + "\n");
            textData.append("DNI/RUC:" + objPedido.getCliente().getCodCliente() + "\n");
            textData.append("PEDIDO:" + objPedido.getNumPedido() + "          " + "F.VCTO: " + objPedido.getFechaVencimien() + "\n");
            textData.append("T.PAGO: " + objPedido.getTipoPago() + "      " + "\n");
            textData.append("F.EMISI: " + objPedido.getFechaEmision() + "\n");

            method = "addFeedLine";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
//            textData.append("7/01/07 16:58 6153 05 0191 134\n");
            textData.append("-----------------------------------------------\n");
            textData.append("id/Can  Desc./Und.   Dscto.    Valor      Total\n");
            textData.append("-----------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            for (int i = 0; i < listaFiltrada.size(); i++) {


//                method = "addTextStyle";
//                mPrinter.addTextStyle(0,0,1,mPrinter.COLOR_1);
                textData.append(listaFiltrada.get(i).getCodProd());
//                mPrinter.addText(textData.toString());
//                textData.delete(0, textData.length());
//                method = "addTextStyle";
//                mPrinter.addTextStyle(0,0,0,mPrinter.COLOR_1);
                textData.append(" " + listaFiltrada.get(i).getNombreProducto() + "\n");
                switch (listaFiltrada.get(i).getCantProd().length()) {
                    case 1:
                        textData.append("     " + listaFiltrada.get(i).getCantProd() + " " + listaFiltrada.get(i).getUniMedida());
                        break;
                    case 2:
                        textData.append("    " + listaFiltrada.get(i).getCantProd() + " " + listaFiltrada.get(i).getUniMedida());
                        break;
                    case 3:
                        textData.append("   " + listaFiltrada.get(i).getCantProd() + " " + listaFiltrada.get(i).getUniMedida());
                        break;
                    case 4:
                        textData.append("  " + listaFiltrada.get(i).getCantProd() + " " + listaFiltrada.get(i).getUniMedida());
                        break;
                    default:
                        textData.append(listaFiltrada.get(i).getCantProd() + " " + listaFiltrada.get(i).getUniMedida());
                        break;
                }
                String descuentovar = listaFiltrada.get(i).getMontoDescuentoProd();
                String[] descuento = descuentovar.split("\\.");
                String spacios = "";
                Log.e("descuento", descuento.toString());
                Log.e("descuento", descuento[1]);
                for (int j = descuento[0].length(); j < 4; j++) {
                    spacios = spacios + " ";
                }

                if (listaFiltrada.get(i).getUniMedida().length() == 3) {
                    if (descuento[1].length() == 2) {
                        textData.append(addSpacios(11) + spacios + listaFiltrada.get(i).getMontoDescuentoProd());
                    } else {
                        textData.append(addSpacios(11) + spacios + listaFiltrada.get(i).getMontoDescuentoProd() + " ");
                    }


                } else {
                    if (descuento[1].length() == 2) {
                        int resta = listaFiltrada.get(i).getUniMedida().length() - 3;
                        textData.append(addSpacios(11 - resta) + spacios + listaFiltrada.get(i).getMontoDescuentoProd());
                    } else {
                        int resta = listaFiltrada.get(i).getUniMedida().length() - 3;
                        textData.append(addSpacios(11 - resta) + spacios + listaFiltrada.get(i).getMontoDescuentoProd() + " ");
                    }


                }
                String[] valor = listaFiltrada.get(i).getMontoProdUnidad().split("\\.");
                String spacios2 = "";


                if (valor[1].length() == 2) {
                    for (int j = valor[0].length(); j < 4; j++) {
                        spacios2 = spacios2 + " ";
                    }
                    textData.append(addSpacios(3) + spacios2 + listaFiltrada.get(i).getMontoProdUnidad());
                } else {
                    for (int j = valor[0].length(); j < 4; j++) {
                        spacios2 = spacios2 + " ";
                    }
                    textData.append(addSpacios(3) + spacios2 + listaFiltrada.get(i).getMontoProdUnidad() + " ");
                }

                String[] valorTotal = listaFiltrada.get(i).getMontoTotalxProduc().split("\\.");
                String spacios3 = "";

                for (int j = valorTotal[0].length(); j < 4; j++) {
                    spacios3 = spacios3 + " ";
                }
                textData.append(addSpacios(6) + spacios3 + listaFiltrada.get(i).getMontoTotalxProduc());
                //textData.append("            "+listaFiltrada.get(i).getMontoDescuentoProd()+"      "+listaFiltrada.get(i).getMontoProdUnidad());
                textData.append("\n");
            }
//            textData.append("\n");
            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            textData.append("-----------------------------------------------\n");


            method = "addText";

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addImage";
            mPrinter.addImage(qrCode, 0, 0,
                    qrCode.getWidth(),
                    qrCode.getHeight(),
                    Printer.COLOR_1,
                    Printer.MODE_MONO,
                    Printer.HALFTONE_DITHER,
                    Printer.PARAM_DEFAULT,
                    Printer.COMPRESS_AUTO);

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            method = "addText";
            mPrinter.addText("IMP. TOTAL:     " + montoTotal + "\n");
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            method = "addFeedLine";
            mPrinter.addFeedLine(0);
            textData.append("-----------------------------------------------\n");
            mPrinter.addHLine(250, 150, mPrinter.LINE_MEDIUM);
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);

            Metodos.Numero_a_Letra NumLetra = new Metodos.Numero_a_Letra();
            String numero = montoTotal;
            String totalVar = "Son:" + NumLetra.Convertir(numero, false);
            String totalVarSaltoLine = "";
            if (totalVar.length() > 47) {
                while (totalVar.length() >= 47) {

                    totalVarSaltoLine = totalVar.substring(0, 46) + "\n";

                    totalVarSaltoLine = totalVarSaltoLine + totalVar.substring(46, totalVar.length());
                    totalVar = totalVar.substring(0, 46);
                }

            } else {
                totalVarSaltoLine = totalVar;
            }
            textData.append(totalVarSaltoLine);


            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addFeedLine";
            mPrinter.addFeedLine(2);
            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);
        } catch (Exception e) {
            ShowMsg.showException(e, method, getContext());
            return false;
        }

        textData = null;

        return true;
    }

    public static String addSpacios(int tamaño) {
        String addspaceioUM = "";
        for (int j = 1; j < tamaño; j++) {
            addspaceioUM = addspaceioUM + " ";
        }
        return addspaceioUM;
    }

    public Bitmap creaQR(String codigo) {
        MultiFormatWriter formato = new MultiFormatWriter();
        try {
            BitMatrix bit = formato.encode(codigo, BarcodeFormat.QR_CODE, 350, 350);
            BarcodeEncoder barcode = new BarcodeEncoder();
            Bitmap bitmap = barcode.createBitmap(bit);
            return bitmap;
//            imgqr.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                ShowMsg.showResult(code, makeErrorMessage(status), getContext());

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", getContext());
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", getContext());
                }
            });
        }

        finalizeObject();
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private void dispPrinterWarnings(PrinterStatusInfo status) {
        //EditText edtWarnings = (EditText)findViewById(R.id.edtWarnings);
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }
        //MensajesDialogAlert.mensajeInformacion(getContext(),"Informacion de impresion",warningsMsg);
        Toast.makeText(getContext(), warningsMsg, Toast.LENGTH_LONG).show();
        System.out.println("mensaje warning: " + warningsMsg);
        //edtWarnings.setText(warningsMsg);
    }

    private void updateButtonState(boolean state) {
        Button btnReceipt = (Button) v.findViewById(R.id.btnFacturarPedido);
//        Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);
        btnReceipt.setEnabled(state);
//        btnCoupon.setEnabled(state);
    }

    public interface pedidoxclienteListener {
        void refresh();

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e("TAG", "onLocationChanged");
        this.location = location;

        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Log.e("TAG", latLng.latitude + "-" + latLng.longitude);
    }


    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("TAG", "onConnected");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(9000);
        mLocationRequest.setFastestInterval(9000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        try {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        getActivity().unregisterReceiver(gpsLocationReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(gpsLocationReceiver);
    }


    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect(i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    //Ejecutar en la interfaz de usuario
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };
    public BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Si la acción es la ubicación
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Compruebe si el GPS está encendido o apagado
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //Si el GPS está apagado, muestre el diálogo de ubicación
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    Log.e("About GPS", "GPS is Disabled in your device");
                    latLng = null;
                    showSettingDialog();
//                    getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Registrar el receptor de difusión para comprobar el estado del GPS.

                    // dismiss();
                }

            }
        }
    };
    //////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


}


