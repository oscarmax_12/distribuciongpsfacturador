package com.legado.distribuciongps;

import android.app.Application;

import com.legado.distribuciongps.db.ClienteDistrib;
import com.legado.distribuciongps.db.DaoMaster;
import com.legado.distribuciongps.db.DaoSession;

import org.greenrobot.greendao.database.Database;

/**
 * Created by __Adrian__ on 15/9/2017.
 */

public class AppController  extends Application {

    public static final boolean ENCRYPTED = true;
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,"clientes-db"); //The users-db here is the name of our database.
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        daoSession.deleteAll(ClienteDistrib.class);
        ///// Using the below lines of code we can toggle ENCRYPTED to true or false in other to use either an encrypted database or not.
//      DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ? "users-db-encrypted" : "users-db");
//      Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
//      daoSession = new DaoMaster(db).newSession();
    }
    public DaoSession getDaoSession() {
        return daoSession;
    }

}