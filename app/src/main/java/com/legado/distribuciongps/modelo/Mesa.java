package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 08/06/2018.
 */

public class Mesa {

    private String codMesa;
    private String descMesa;

    public Mesa() {
    }

    public Mesa(String codMesa, String descMesa) {
        this.codMesa = codMesa;
        this.descMesa = descMesa;
    }

    public String getCodMesa() {
        return codMesa;
    }

    public void setCodMesa(String codMesa) {
        this.codMesa = codMesa;
    }

    public String getDescMesa() {
        return descMesa;
    }

    public void setDescMesa(String descMesa) {
        this.descMesa = descMesa;
    }

}
