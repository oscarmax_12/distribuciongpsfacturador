package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 21/06/2018.
 */

public class DetaDocumento {

    private String codigo;
    private String descripcion;
    private Integer cantidad;
    private Double preUni;
    private Double dscto;
    private Double subtotal;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPreUni() {
        return preUni;
    }

    public void setPreUni(Double preUni) {
        this.preUni = preUni;
    }

    public Double getDscto() {
        return dscto;
    }

    public void setDscto(Double dscto) {
        this.dscto = dscto;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

}
