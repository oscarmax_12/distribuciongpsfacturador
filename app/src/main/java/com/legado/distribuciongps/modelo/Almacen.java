package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 08/06/2018.
 */

public class Almacen {

    private String codAlmacen;
    private String descAlmacen;

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getDescAlmacen() {
        return descAlmacen;
    }

    public void setDescAlmacen(String descAlmacen) {
        this.descAlmacen = descAlmacen;
    }

}