package com.legado.distribuciongps.modelo;

/**
 * Created by __Adrian__ on 7/9/2017.
 */
public class Placa {

    private Empresa empresa;
    private Sede sede;
    private String numPlaca;

    public Placa() {

    }

    public Placa(String numPlaca) {
        this.numPlaca = numPlaca;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public String getNumPlaca() {
        return numPlaca;
    }

    public void setNumPlaca(String numPlaca) {
        this.numPlaca = numPlaca;
    }
}
