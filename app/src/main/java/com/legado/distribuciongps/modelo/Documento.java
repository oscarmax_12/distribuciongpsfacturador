package com.legado.distribuciongps.modelo;

import java.util.List;

/**
 * Created by ERICK on 21/06/2018.
 */

public class Documento {

    private String docCliente;
    private String tipoDoc;
    private String serie;
    private String preimpreso;
    private String fecha;
    private String fechaEmision;
    private String ruta;
    private String placa;
    private String dirDespacho;
    private String anulados;
    private List<DetaDocumento> detalles;

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public List<DetaDocumento> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetaDocumento> detalles) {
        this.detalles = detalles;
    }

    public String getDirDespacho() {
        return dirDespacho;
    }

    public void setDirDespacho(String dirDespacho) {
        this.dirDespacho = dirDespacho;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDocCliente() {
        return docCliente;
    }

    public void setDocCliente(String docCliente) {
        this.docCliente = docCliente;
    }

    public String getAnulados() {
        return anulados;
    }

    public void setAnulados(String anulados) {
        this.anulados = anulados;
    }
}
