package com.legado.distribuciongps.modelo;

import java.math.BigDecimal;

/**
 * Created by ERICK on 07/06/2018.
 */

public class Articulo{

    private String codigo;
    private String descripcion;
    private Integer cantidad;
    private Integer cantidadNueva;
    private String undMed;
    private BigDecimal precioBase;
    private BigDecimal precioFinal;
    private Double porcDesc;
    private Double montoIgv;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getUndMed() {
        return undMed;
    }

    public void setUndMed(String undMed) {
        this.undMed = undMed;
    }

    public BigDecimal getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(BigDecimal precioBase) {
        this.precioBase = precioBase;
    }

    public BigDecimal getPrecioFinal() {
        return precioFinal;
    }

    public void setPrecioFinal(BigDecimal precioFinal) {
        this.precioFinal = precioFinal;
    }

    public Integer getCantidadNueva() {
        return cantidadNueva;
    }

    public void setCantidadNueva(Integer cantidadNueva) {
        this.cantidadNueva = cantidadNueva;
    }

    public Double getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(Double porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Double getMontoIgv() {
        return montoIgv;
    }

    public void setMontoIgv(Double montoIgv) {
        this.montoIgv = montoIgv;
    }
}
