package com.legado.distribuciongps.modelo;

/**
 * Created by __Adrian__ on 12/9/2017.
 */

public class ClienteDistribModel {
    private String codCliente;
    private String nroPacking;
    private String descCliente;
    private String codLocal;
    private String descDireccion;
    private String codRuta;
    private String codEmpresa;
    private String codAlmacen;
    private String coordenadas;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNroPacking() {
        return nroPacking;
    }

    public void setNroPacking(String nroPacking) {
        this.nroPacking = nroPacking;
    }

    public String getCodLocal() {
        return codLocal;
    }

    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    public String getDescDireccion() {
        return descDireccion;
    }

    public void setDescDireccion(String descDireccion) {
        this.descDireccion = descDireccion;
    }

    public String getCodRuta() {
        return codRuta;
    }

    public void setCodRuta(String codRuta) {
        this.codRuta = codRuta;
    }

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public ClienteDistribModel() {
    }

    public ClienteDistribModel(String descCliente, String codCliente) {
        this.descCliente = descCliente;
        this.codCliente = codCliente;
    }


    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getDescCliente() {
        return descCliente;
    }

    public void setDescCliente(String descCliente) {
        this.descCliente = descCliente;
    }
}
