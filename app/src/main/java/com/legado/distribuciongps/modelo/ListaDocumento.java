package com.legado.distribuciongps.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ERICK on 19/08/2019.
 */

public class ListaDocumento implements Serializable {

    private List<DocReparto> lista=new ArrayList<DocReparto>();

    public List<DocReparto> getLista() {
        return lista;
    }

    public void setLista(List<DocReparto> lista) {
        this.lista = lista;
    }

}