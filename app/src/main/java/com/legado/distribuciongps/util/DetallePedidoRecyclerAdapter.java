package com.legado.distribuciongps.util;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Articulo;
import com.legado.distribuciongps.modelo.DetaDocumento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ERICK on 13/06/2018.
 */

public class DetallePedidoRecyclerAdapter extends RecyclerView.Adapter<DetallePedidoRecyclerAdapter.PedidoRecyclerViewHolder>  {

    private List<Articulo> detallePedidos =new ArrayList<>();
    private List<DetaDocumento> detalleDocumentos = new ArrayList<>();
    private FragmentActivity activity;
    private Context context;

    public DetallePedidoRecyclerAdapter(List<DetaDocumento> detalleDocumentos, List<Articulo> detallePedidos, Context context, FragmentActivity activity) {
        this.detallePedidos = detallePedidos;
        this.detalleDocumentos = detalleDocumentos;
        this.context=context;
        this.activity=activity;
    }

    @Override
    public PedidoRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pedidoconfirmar, parent, false);
       /// return new PedidoRecyclerViewHolder(v, this);
        PedidoRecyclerViewHolder holder = new PedidoRecyclerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PedidoRecyclerViewHolder holder,final int position) {
        if(this.detalleDocumentos != null){
          /*  holder.lyPreUni.setVisibility(View.VISIBLE);
            holder.lyDescuento.setVisibility(View.VISIBLE);
            holder.lySubTotal.setVisibility(View.VISIBLE);
            holder.lytxtPreUni.setVisibility(View.VISIBLE);
            holder.lytxtDescuento.setVisibility(View.VISIBLE);
            holder.lytxtSubTotal.setVisibility(View.VISIBLE);*/
            ////holder.codArticulo.setBackgroundColor(Color.BLUE);

            holder.codArticulo.setText(detalleDocumentos.get(position).getCodigo());
            holder.descArticulo.setText(detalleDocumentos.get(position).getDescripcion());
            holder.cantidad.setText(detalleDocumentos.get(position).getCantidad().toString());
            holder.txtDescuento.setText(Metodos.RetornarStringConDosDecimales( detalleDocumentos.get(position).getDscto()*100 ));
            holder.txtPreUni.setText(Metodos.RetornarStringConTresDecimales( (detalleDocumentos.get(position).getPreUni() <= 0.001 ? 0.001 : detalleDocumentos.get(position).getPreUni())  ));
            holder.txtSubTotal.setText(Metodos.RetornarStringConDosDecimales(detalleDocumentos.get(position).getSubtotal()));
        } else {
            holder.codArticulo.setText(detallePedidos.get(position).getCodigo());
            holder.descArticulo.setText(detallePedidos.get(position).getDescripcion());
            holder.cantidad.setText(detallePedidos.get(position).getCantidad().toString());

            holder.txtDescuento.setText(Metodos.RetornarStringConDosDecimales( detallePedidos.get(position).getPorcDesc()*100 ));
            holder.txtPreUni.setText(Metodos.RetornarStringConTresDecimales( (detallePedidos.get(position).getPrecioBase().doubleValue() <= 0.001 ? 0.001 : detallePedidos.get(position).getPrecioBase().doubleValue())  ));
            holder.txtSubTotal.setText(Metodos.RetornarStringConDosDecimales(detallePedidos.get(position).getPrecioFinal().doubleValue()));

           /* holder.lyPreUni.setVisibility(View.GONE);
            holder.lyDescuento.setVisibility(View.GONE);
            holder.lySubTotal.setVisibility(View.GONE);
            holder.lytxtPreUni.setVisibility(View.GONE);
            holder.lytxtDescuento.setVisibility(View.GONE);
            holder.lytxtSubTotal.setVisibility(View.GONE);*/
        }
    }

    @Override
    public int getItemCount() {
        if(this.detalleDocumentos != null){
            return detalleDocumentos.size();
        }else{
            return detallePedidos.size();
        }
    }

    public static class PedidoRecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView codArticulo;
        public TextView descArticulo;
        public TextView cantidad;
        public LinearLayout lyPreUni;
        public LinearLayout lyDescuento;
        public LinearLayout lySubTotal;
        public LinearLayout lytxtPreUni;
        public LinearLayout lytxtDescuento;
        public LinearLayout lytxtSubTotal;
        public TextView txtPreUni;
        public TextView txtDescuento;
        public TextView txtSubTotal;
        public LinearLayout itemLinear;

        public PedidoRecyclerViewHolder(View itemView) {
            super(itemView);
            codArticulo = (TextView) itemView.findViewById(R.id.codArticulo);
            descArticulo = (TextView) itemView.findViewById(R.id.descArticulo);
            cantidad = (TextView) itemView.findViewById(R.id.txtCantidad);
            lyPreUni = (LinearLayout) itemView.findViewById(R.id.lyPreUni);
            lyDescuento = (LinearLayout) itemView.findViewById(R.id.lyDescuento);
            lySubTotal = (LinearLayout) itemView.findViewById(R.id.lySubTotal);
            lytxtPreUni = (LinearLayout) itemView.findViewById(R.id.lytxtPreUni);
            lytxtDescuento = (LinearLayout) itemView.findViewById(R.id.lytxtDescuento);
            lytxtSubTotal = (LinearLayout) itemView.findViewById(R.id.lytxtSubTotal);
            txtPreUni = (TextView) itemView.findViewById(R.id.txtPreUni);
            txtDescuento = (TextView) itemView.findViewById(R.id.txtDescuento);
            txtSubTotal = (TextView) itemView.findViewById(R.id.txtSubTotal);

        }
    }

}
