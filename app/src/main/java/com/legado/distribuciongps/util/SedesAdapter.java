package com.legado.distribuciongps.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Asignacion;

import java.util.List;

/**
 * Created by ERICK on 27/06/2018.
 */

public class SedesAdapter extends ArrayAdapter<Asignacion> {

    private static final String TAG = "SedesAdapter";
    private List<Asignacion> asignaciones;
    private Activity activity;

    public SedesAdapter(Activity a, int textViewResourceId, List<Asignacion> asignaciones) {
        super(a, textViewResourceId, asignaciones);
        this.asignaciones = asignaciones;
        activity = a;
    }


    public static class ViewHolderSede {
        public TextView descSede;
    }

    ViewHolderSede holder;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_empresasede, null);
            holder = new ViewHolderSede();
            holder.descSede = (TextView) v.findViewById(R.id.descEmpresa);
            v.setTag(holder);
        } else holder=(ViewHolderSede) v.getTag();
        final Asignacion asignacion = asignaciones.get(position);
        if (asignacion != null) {
            holder.descSede.setText(asignacion.getSede().getDescSede());
        }
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return  getView(position, convertView, parent);
    }

}