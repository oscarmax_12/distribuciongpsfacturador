package com.legado.distribuciongps.util;

/**
 * Created by ERICK on 07/06/2018.
 */

public class Model {

    private int number;
    private String fruit;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getFruit() {
        return fruit;
    }

    public void setFruit(String fruit) {
        this.fruit = fruit;
    }

}
