package com.legado.distribuciongps.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.legado.distribuciongps.modelo.Asignacion;
import com.legado.distribuciongps.modelo.ListaDocumento;
import com.legado.distribuciongps.modelo.Usuario;

import java.util.Arrays;
import java.util.List;

public class SessionUsuario {

    SharedPreferences preferences; //Lista de Preferencias
    SharedPreferences.Editor editor; // Editor de Preferencias
    Context _context; // Contexto
    int PRIVATE_MODE = 0; // Modo de Preferencias -> es el modo seguro solo la aplicacion accede a este archivo xml
    public static final String PREFER_NAME = "session";// Nombre de archivo de preferencias compartidas
    public static final String IS_USER_LOGIN = "isUserLoggedIn"; // Todas las claves de preferencias compartidas
    public static final String KEY_NAME = "nombre"; // Nombre de usuario (hacer pública la variable para acceder desde fuera)
    public static final String KEY_EMAIL = "email"; // Dirección de correo electrónico (hacer pública la variable para acceder desde fuera)
    Gson gson = new Gson();

    public SessionUsuario(Context context) {
        this._context = context;
        preferences = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void IniciarSession(Boolean seinicio, Usuario usuario, String codAplicacion, String codSede, String codDep) {
        editor.putBoolean("login", seinicio);
        editor.putString("username", usuario != null ? usuario.getNombre() : "");
        editor.putString("nick", usuario != null ? usuario.getNick() : "");
        editor.putString("codsede", codSede);
        editor.putString("coddep", codDep);
        Gson gson = new Gson();
        if (usuario != null) {
            String asignacionJson = gson.toJson(usuario.getAsignaciones());
            editor.putString("asignaciones", asignacionJson);
        }
        editor.putString("url", "http://190.223.55.172:8080/serviciodistribucionv3/distribucion/");
        if (seinicio) {
            editor.putString("codigo", codAplicacion);
        }
        editor.commit();
    }

    public void guardarPaqueteCliente(PaqueteClientes paquete) {
        Gson gson = new Gson();
        String paqueteJson = gson.toJson(paquete);
        editor.putString("paquetecliente", paqueteJson);
        editor.commit();
    }

    public PaqueteClientes getPaqueteCliente() {
        String json = preferences.getString("paquetecliente", "");
        PaqueteClientes obj = gson.fromJson(json, PaqueteClientes.class);
        return obj;
    }

    public List<Asignacion> getAsignaciones() {
        String json = preferences.getString("asignaciones", "");
        Asignacion[] obj = gson.fromJson(json, Asignacion[].class);
        return Arrays.asList(obj);
    }

    public Boolean getSession() {
        return preferences.getBoolean("login", false);
    }

    public String getUsuarioXray() {
        return preferences.getString("username", "");
    }

    public String getNickUsuario() {
        return preferences.getString("nick", "");
    }

    public String getCodDep() {
        return preferences.getString("coddep", "");
    }

    public String getCodSede() {
        return preferences.getString("codsede", "");
    }

    public void CargarCodigo(String codAplicacion) {
        editor.putString("codigo", codAplicacion);
        editor.commit();
    }

    public void guardaImpresora(String srtImprsMAC) {
        editor.putString("impresora", srtImprsMAC);
        editor.commit();
    }

    public String getImpresora() {
        String codigo = preferences.getString("impresora", "");
        return codigo;
    }
    public String getCodigoAplicacion() {
        String codigo = preferences.getString("codigo", "");
        return codigo;
    }

    public void CargarCodigoUrl(String url) {
        editor.putString("url", url);
        editor.commit();
    }

    public String getCodigoUrl() {
        String url = preferences.getString("url", "");
        return url;
    }

    public void GuardarListaDocumentos(ListaDocumento util) {
        Gson gson = new Gson();
        String paqueteJson = gson.toJson(util);
        editor.putString("listado", paqueteJson);
        editor.commit();
    }

    public ListaDocumento getListaDocumentos() {
        String json = preferences.getString("listado", "");
        ListaDocumento obj = gson.fromJson(json, ListaDocumento.class);
        return obj;
    }

}