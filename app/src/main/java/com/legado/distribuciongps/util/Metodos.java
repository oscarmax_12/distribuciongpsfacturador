package com.legado.distribuciongps.util;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.legado.distribuciongps.BuildConfig;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;
import com.legado.distribuciongps.modelo.Version;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.regex.Pattern;

/**
 * Created by ERICK on 05/06/2018.
 */

public class Metodos {
 public static String IS_VALIDATE_VERSION;
    private static SessionUsuario sessionUsuario;
    public static void validarSesion(Activity act ) {
        sessionUsuario = new SessionUsuario(act.getBaseContext());
        Version objVersion = new Version();
        objVersion.setDescripcion("DISTRIBUCION");
        objVersion.setVersion_app(BuildConfig.VERSION_NAME);
        objVersion.setCodSede(sessionUsuario.getCodSede());
        String imei=obtenerIMEI(act);
        objVersion.setImei(imei);
        objVersion.setPlaca(sessionUsuario.getCodigoAplicacion());
        Gson gson = new Gson();

        String logRepartoJson = gson.toJson(objVersion);
        Log.e("validarVersion", logRepartoJson);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getVersionService().ValidarVersion(objVersion);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                if (response.body().getEstado() >= 1) {
                    Metodos.IS_VALIDATE_VERSION="true";

                } else {

                    Metodos.IS_VALIDATE_VERSION="false";
                    //   progressDialog.dismiss();

                }
            }
            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Metodos.IS_VALIDATE_VERSION=t.getMessage();
                //
            }
        });
    }
    private static String obtenerIMEI(Activity act) {
        final TelephonyManager telephonyManager = (TelephonyManager) act.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
            if (ContextCompat.checkSelfPermission(act,Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(act, Manifest.permission.READ_PHONE_STATE)) {

                    ActivityCompat.requestPermissions(act, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);

                } else {

                    ActivityCompat.requestPermissions(act, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);
                }
            }
            return telephonyManager.getImei();
        } else {
            return telephonyManager.getDeviceId();
        }

    }
    public static String parsingEmptyUppercase(String cadena) throws Exception{
        try {
            if (cadena==null) {
                return null;
            }
            return cadena.trim().equals("")?null:cadena.trim().toUpperCase();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String formatearDecimales(String numeroString){
        DecimalFormat  formateador = new DecimalFormat("###,###.##");

        return  formateador.format(Double.parseDouble(numeroString));
    }

    public static String RetornarStringConDosDecimales(Double numero){
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');
        DecimalFormat formato = null;
        if(numero < 1 && numero >-1){
            formato = new DecimalFormat("0.00",simbolos);
        }else{
            formato = new DecimalFormat("###,###,###,###.00",simbolos);
        }
        return formato.format(numero);
    }

    public static String RetornarStringConTresDecimales(Double numero){
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');
        DecimalFormat formato = null;
        if(numero < 1 && numero >-1){
            formato = new DecimalFormat("0.000",simbolos);
        }else{
            formato = new DecimalFormat("###,###,###,###.000",simbolos);
        }
        return formato.format(numero);
    }

    public static boolean verificarNumero(String cadena) {//SOLO NUMEROS DE 9 DIGITOS COMO MAXIMO
        Boolean esnumero = true;
        try{
            Integer.parseInt(cadena);
        }catch(Exception e){
            esnumero = false;
        }
        return esnumero;
    }

    public static void showProgressDialogWithTitle(String substring,ProgressDialog progressDialog) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    // Method to hide/ dismiss Progress bar
    public static void hideProgressDialogWithTitle(ProgressDialog progressDialog) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

//====================
public static class Numero_a_Letra {

    private final String[] UNIDADES = {"", "Un ", "Dos ", "Tres ", "Cuatro ", "Cinco ", "Seis ", "Siete ", "Ocho ", "Nueve "};
    private final String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
            "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
            "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
    private final String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
            "setecientos ", "ochocientos ", "novecientos "};

    public Numero_a_Letra() {
    }

    public String Convertir(String numero, boolean mayusculas) {
        String literal = "";
        String parte_decimal;
        //si el numero utiliza (.) en lugar de (,) -> se reemplaza
        numero = numero.replace(".", ",");
        //si el numero no tiene parte decimal, se le agrega ,00
        if(numero.indexOf(",")==-1){
            numero = numero + ",00";
        }
        //se valida formato de entrada -> 0,00 y 999 999 999,00
        if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {
            //se divide el numero 0000000,00 -> entero y decimal
            String Num[] = numero.split(",");
            //de da formato al numero decimal
            parte_decimal = Num[1] + "/100 Nuevos Soles.";
            //se convierte el numero a literal
            if (Integer.parseInt(Num[0]) == 0) {//si el valor es cero
                literal = "cero ";
            } else if (Integer.parseInt(Num[0]) > 999999) {//si es millon
                literal = getMillones(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 999) {//si es miles
                literal = getMiles(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 99) {//si es centena
                literal = getCentenas(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 9) {//si es decena
                literal = getDecenas(Num[0]);
            } else {//sino unidades -> 9
                literal = getUnidades(Num[0]);
            }
            //devuelve el resultado en mayusculas o minusculas
            if (mayusculas) {
                return (literal + parte_decimal).toUpperCase();
            } else {
                return (literal + parte_decimal);
            }
        } else {//error, no se puede convertir
            return literal = null;
        }
    }

    /* funciones para convertir los numeros a literales */

    private String getUnidades(String numero) {// 1 - 9
        //si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
        String num = numero.substring(numero.length() - 1);
        return UNIDADES[Integer.parseInt(num)];
    }

    private String getDecenas(String num) {// 99
        int n = Integer.parseInt(num);
        if (n < 10) {//para casos como -> 01 - 09
            return getUnidades(num);
        } else if (n > 19) {//para 20...99
            String u = getUnidades(num);
            if (u.equals("")) { //para 20,30,40,50,60,70,80,90
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
            } else {
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
            }
        } else {//numeros entre 11 y 19
            return DECENAS[n - 10];
        }
    }

    private String getCentenas(String num) {// 999 o 099
        if( Integer.parseInt(num)>99 ){//es centena
            if (Integer.parseInt(num) == 100) {//caso especial
                return " cien ";
            } else {
                return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getDecenas(num.substring(1));
            }
        }else{//por Ej. 099
            //se quita el 0 antes de convertir a decenas
            return getDecenas(Integer.parseInt(num)+"");
        }
    }

    private String getMiles(String numero) {// 999 999
        //obtiene las centenas
        String c = numero.substring(numero.length() - 3);
        //obtiene los miles
        String m = numero.substring(0, numero.length() - 3);
        String n="";
        //se comprueba que miles tenga valor entero
        if (Integer.parseInt(m) > 0) {
            n = getCentenas(m);
            return n + "mil " + getCentenas(c);
        } else {
            return "" + getCentenas(c);
        }

    }

    private String getMillones(String numero) { //000 000 000
        //se obtiene los miles
        String miles = numero.substring(numero.length() - 6);
        //se obtiene los millones
        String millon = numero.substring(0, numero.length() - 6);
        String n = "";
        if(millon.length()>1){
            n = getCentenas(millon) + "millones ";
        }else{
            n = getUnidades(millon) + "millon ";
        }
        return n + getMiles(miles);
    }
}

}
