package com.legado.distribuciongps.activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;


import com.legado.distribuciongps.R;
import com.legado.distribuciongps.util.SessionUsuario;

import static com.legado.distribuciongps.util.LogUtil.makeLogTag;


public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = makeLogTag(BaseActivity.class);

    protected static final int NAV_DRAWER_ITEM_INVALID = -1;
    protected static final int SPINNER_ITEM_INVALID = -1;
    private DrawerLayout drawerLayout;
    private Toolbar actionBarToolbar;
    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;
    TextView txtPlaca;
    Menu menu;
    SessionUsuario sessionUsuario;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        sessionUsuario = new SessionUsuario(this);
        setupNavDrawer();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Downloading file...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);


    }

    /**
     * Sets up the navigation drawer.
     */
    private void setupNavDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout == null) {
            // current activity does not have a drawer.
            return;
        }
        // drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupCardSelectListener();
            setSelectedItem(navigationView);
        }

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.getMenu().getItem(0).setChecked(true);
//
//        if (navigationView != null) {
//            setupDrawerSelectListener(navigationView);
//            setSelectedItem(navigationView);
//        }
//
//
//        View header=navigationView.getHeaderView(0);
        sessionUsuario = new SessionUsuario(this);
       txtPlaca = (TextView)navigationView.findViewById(R.id.txtPlacaLogin);
        txtPlaca.setText(sessionUsuario.getCodigoAplicacion());
//      menu=navigationView.getMenu();

    }


    private void setupCardSelectListener() {
        final CardView menu0 = (CardView) findViewById(R.id.menu0);
        final CardView menu1 = (CardView) findViewById(R.id.menu1);
        final CardView menu2 = (CardView) findViewById(R.id.menu2);
        final CardView menu3 = (CardView) findViewById(R.id.menu3);
//        final CardView menu4= (CardView) findViewById(R.id.menu4);
        final CardView menu5 = (CardView) findViewById(R.id.menu5);
        final CardView menu6 = (CardView) findViewById(R.id.menu6);
        final CardView menu7 = (CardView) findViewById(R.id.menu7);
        final Button btnCerrarSesion = (Button) findViewById(R.id.btnCerrarSesion);
        final Button btnBuscarImpre = (Button) findViewById(R.id.btnBuscarImpresora);


        menu0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu0.getId());
            }
        });
        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu1.getId());
            }
        });
        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu2.getId());
            }
        });
        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu3.getId());
            }
        });
//        menu4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onNavigationItemClicked(menu4.getId());
//            }
//        });

        menu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu5.getId());
            }
        });
        menu6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu6.getId());
            }
        });
        menu7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(menu7.getId());
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(btnCerrarSesion.getId());
            }
        });
        btnBuscarImpre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemClicked(btnBuscarImpre.getId());
            }
        });


    }

    protected void openDrawer() {
        if (drawerLayout == null)
            return;
        ObjectAnimator.ofFloat(actionBarToolbar, View.ROTATION.getName(), 360).start();
        drawerLayout.openDrawer(GravityCompat.START);
    }

    protected void closeDrawer() {
        if (drawerLayout == null)
            return;
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    /**
     * Updated the checked item in the navigation drawer
     *
     * @param navigationView the navigation view
     */
    private void setSelectedItem(NavigationView navigationView) {
        // Which navigation item should be selected?
        int selectedItem = getSelfNavDrawerItem(); // subclass has to override this method
        navigationView.setCheckedItem(selectedItem);
    }

    private void setSelectedSpinner(Spinner spinner) {
        // Which navigation item should be selected?
        int selectedItem = getSpinnerPosition(); // subclass has to override this method
        spinner.setSelection(selectedItem);
    }


    /**
     * Creates the item click listener.
     *
     * @param navigationView the navigation view
     */
    private void setupDrawerSelectListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        drawerLayout.closeDrawers();
                        onNavigationItemClicked(menuItem.getItemId());
                        return true;
                    }
                });
    }


    protected void setupSpinnerSelectListener(Spinner spinner) {

    }

    /**
     * Handles the navigation item click.
     *
     * @param itemId the clicked item
     */
    private void onNavigationItemClicked(final int itemId) {
        if (itemId == getSelfNavDrawerItem()) {
            // Already selected
            closeDrawer();
            return;
        }

        goToNavDrawerItem(itemId);
    }

    /**
     * Handles the navigation item click and starts the corresponding activity.
     *
     * @param item the selected navigation item
     */
    protected void goToNavDrawerItem(int item) {

    }

    /**
     * Provides the action bar instance.
     *
     * @return the action bar.
     */
    protected ActionBar getActionBarToolbar() {
        if (actionBarToolbar == null) {
            actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (actionBarToolbar != null) {
                setSupportActionBar(actionBarToolbar);
            }
        }
        return getSupportActionBar();
    }


    protected int getSelfNavDrawerItem() {
        return NAV_DRAWER_ITEM_INVALID;
    }

//    protected void openDrawer() {
//        if (drawerLayout == null)
//            return;
//
//        drawerLayout.openDrawer(GravityCompat.START);
//    }
//
//    protected void closeDrawer() {
//        if (drawerLayout == null)
//            return;
//
//        drawerLayout.closeDrawer(GravityCompat.START);
//    }

    public abstract boolean providesActivityToolbar();

    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected Menu getMenu() {
        return menu;
    }

    protected int getSpinnerPosition() {
        return SPINNER_ITEM_INVALID;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

}