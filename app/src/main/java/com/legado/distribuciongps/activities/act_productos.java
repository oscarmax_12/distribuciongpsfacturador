package com.legado.distribuciongps.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.listado_productos;
import com.legado.distribuciongps.fragments.pedidoFacturarFragment;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;

public class act_productos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_porductos);



Metodos.validarSesion(this);
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(this, "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            this.finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(this, "ERROR!", "Error");
            this.finish();
        }
        Bundle b = getIntent().getExtras();
        String nroPedido = "",montoTotal,flag10="",codEmpresaTemp="",tipoDoc="",seriePre="",codLoc=""; // or other values
        if(b != null){
            nroPedido = b.getString("nroPedido");
            montoTotal = b.getString("montoTotal");
            flag10 = b.getString("flag10");
            codEmpresaTemp = b.getString("codEmpresaTemp");
            tipoDoc = b.getString("tipoDoc");
            seriePre = b.getString("seriePre");
            codLoc = b.getString("codLoc");
            //Gson gson2 = new Gson();
            String objPedido = b.getString("objPedido");
            //Pedido objPedido = gson2.fromJson(json, Pedido.class);


            //PASO DE PARAMETROS AL FRAGMENT
            pedidoFacturarFragment inicioFragment = new pedidoFacturarFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle data = new Bundle();
            data.putString("nroPedido", nroPedido);
            data.putString("montoTotal", montoTotal);
            data.putString("flag10",flag10);
            data.putString("codEmpresaTemp",codEmpresaTemp);
            data.putString("tipoDoc", tipoDoc);
            data.putString("seriePre", seriePre);
            data.putString("codLoc", codLoc);
            data.putString("codCliente",b.getString("codCliente"));
            data.putString("objPedido",objPedido);
//            inicioFragment.setInitialSavedState(appState.getMyFragmentState());
//            getSupportFragmentManager().saveFragmentInstanceState(inicioFragment);
            inicioFragment.setArguments(data);
            fragmentTransaction.replace(R.id.contenedorProductos, inicioFragment);
            fragmentTransaction.commit();

        }else{
            Toast.makeText(this,"Error bundle",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }
}
