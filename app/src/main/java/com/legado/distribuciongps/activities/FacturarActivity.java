package com.legado.distribuciongps.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.BeansPedido;
import com.legado.distribuciongps.modelo.DetaDocumento;
import com.legado.distribuciongps.modelo.Documento;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.util.DetallePedidoRecyclerAdapter;
import com.legado.distribuciongps.util.DeviceList;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.PrinterCommands;
import com.legado.distribuciongps.util.SimpleDividerItemDecorator;
import com.legado.distribuciongps.util.UtilsPrint;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ERICK on 09/06/2018.
 */

public class FacturarActivity extends AppCompatActivity {

    @BindView(R.id.codEmpresa)
    TextView codEmpresa;
    @BindView(R.id.codSede)
    TextView codSede;
    @BindView(R.id.codMesa)
    TextView codMesa;
    @BindView(R.id.codAlmacen)
    TextView codAlmacen;
    @BindView(R.id.codVendedor)
    TextView codVendedor;
    @BindView(R.id.descCliente)
    TextView descCliente;
    @BindView(R.id.descCondicion)
    TextView descCondicion;
    @BindView(R.id.nroPedido)
    TextView nroPedido;
    @BindView(R.id.listaArticulos)
    RecyclerView listaArticulos;
    RecyclerView.LayoutManager lManagerPedido;
    DetallePedidoRecyclerAdapter detalleRecyclerAdapter;
    @BindView(R.id.txtCantidad)
    TextView txtCantidad;
    @BindView(R.id.txtMonto)
    TextView txtMonto;
    @BindView(R.id.txtCerrar)
    Button txtCerrar;
    @BindView(R.id.btnGenerar)
    Button btnGenerar;
    @BindView(R.id.btnValidar)
    Button btnValidar;
    private static BluetoothSocket btsocket;
    private static OutputStream outputStream;
    private Pedido ped;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facturar_pedido);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this, R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        Gson gson = new Gson();
        String json = getIntent().getStringExtra("pedido");
        ped = gson.fromJson(json, Pedido.class);

        listaArticulos.setVisibility(View.VISIBLE);
        listaArticulos.setHasFixedSize(true);
        lManagerPedido = new LinearLayoutManager(getApplicationContext());
        listaArticulos.setLayoutManager(lManagerPedido);
        detalleRecyclerAdapter = new DetallePedidoRecyclerAdapter(null, ped.getDetalles(), getApplicationContext(), this);
        listaArticulos.setAdapter(detalleRecyclerAdapter);
        listaArticulos.addItemDecoration(new SimpleDividerItemDecorator(this));

        nroPedido.setText(ped.getNumPedido());
        codEmpresa.setText(ped.getEmpresa().getDescEmpresa());
        codSede.setText(ped.getSede().getCodSede() != null ? ped.getSede().getDescSede() : "");
        codMesa.setText(ped.getMesa().getCodMesa() != null ? ped.getMesa().getDescMesa() + (ped.getCanal().getCodCanal() != null ? " - " + ped.getCanal().getDescCanal() : "") : "");
        codAlmacen.setText(ped.getAlmacen().getCodAlmacen() + " - " + ped.getAlmacen().getDescAlmacen());
        codVendedor.setText(ped.getVendedor().getCodVendedor());
        descCliente.setText(ped.getCliente().getNomCliente());
        descCondicion.setText(ped.getCodCondicion());
        txtCantidad.setText(String.valueOf(ped.getDetalles().size()));
        txtMonto.setText(ped.getMontoTotal().toString());
        txtCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (ped.getEstado().equals("G")) {
            btnGenerar.setText("Imprimir");
        } else {
            btnGenerar.setText("Generar");
        }
        AsignarFuncionBoton();
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                Call<JsonRespuesta<BeansPedido>> call = ApiRetrofit.getInstance().getPedidoService().ValidarPedido(ped);
                call.enqueue(new Callback<JsonRespuesta<BeansPedido>>() {
                    @Override
                    public void onResponse(Call<JsonRespuesta<BeansPedido>> call, Response<JsonRespuesta<BeansPedido>> response) {
                        JsonRespuesta rptaClientes = response.body();
                        Integer estado = rptaClientes.getEstado();
                        if (estado == 1) {
                            List<BeansPedido> data = rptaClientes.getData();
                            if (!data.isEmpty()) {
                                String desc = "Articulos faltantes: " + "\n";
                                for (int i = 0; i < data.size(); i++) {
                                    desc += data.get(i).getCod_item() + " - " + data.get(i).getDesc_item() + "\n";
                                }
                                Toast.makeText(getApplicationContext(), desc, Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Pedido correcto.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo 2", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonRespuesta<BeansPedido>> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo 1", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void AsignarFuncionBoton() {
        btnGenerar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ped.getEstado().equals("G")) {
                    printBill();
                } else {
                    progressDialog.show();
                    Call<JsonRespuesta<Documento>> call = ApiRetrofit.getInstance().getPedidoService().RefacturarPedido(ped);
                    call.enqueue(new Callback<JsonRespuesta<Documento>>() {
                        @Override
                        public void onResponse(Call<JsonRespuesta<Documento>> call, retrofit2.Response<JsonRespuesta<Documento>> response) {
                            JsonRespuesta rptaClientes = response.body();
                            Integer estado = rptaClientes.getEstado();

                            if (estado == 1) {
                                Documento docu = (Documento) rptaClientes.getItem();
                                ped.setFecha(docu.getFecha() != null ? docu.getFecha() : "");
                                ped.setPlaca(docu.getPlaca() != null ? docu.getPlaca() : "");
                                ped.setSeriePre(docu.getSerie());
                                ped.getCliente().setDirDespacho(docu.getDirDespacho());
                                ped.setRuta(docu.getRuta());
                                ped.getCliente().setRucCliente(docu.getDocCliente());
                                ped.setAnulados(docu.getAnulados());

                                Toast.makeText(getApplicationContext(), "Pedido Facturado correctamente.", Toast.LENGTH_LONG).show();
                                btnGenerar.setText("Imprimir");
                                ped.setEstado("G");
                                AsignarFuncionBoton();
                                progressDialog.dismiss();
                            } else if (estado == 3 || estado == 4) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), rptaClientes.getMensaje(), Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo 2", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonRespuesta<Documento>> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void LlenarLista(List<DetaDocumento> detalles) {
        detalleRecyclerAdapter = new DetallePedidoRecyclerAdapter(detalles, null, getApplicationContext(), this);
        listaArticulos.setAdapter(detalleRecyclerAdapter);
        listaArticulos.addItemDecoration(new SimpleDividerItemDecorator(this));
    }

    protected void printBill() {
        if (btsocket == null) {
            Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        } else {
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputStream = opstream;
            try {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                outputStream = btsocket.getOutputStream();
                byte[] printformat = new byte[]{0x1B, 0x21, 0x03};
                outputStream.write(printformat);

                printCustom(ped.getEmpresa().getDescEmpresa(), 2, 1);
                printCustom("RUC: " + ped.getEmpresa().getRuc(), 1, 1);
                printCustom(ped.getEmpresa().getDireccion() != null ? ped.getEmpresa().getDireccion() : "", 0, 1);
                printCustom("-------------------------", 0, 1);
                printCustom((ped.getTipDoc().equals("01") ? "Factura" : "Boleta") + " Electrónica", 0, 1);
                printCustom((ped.getTipDoc().equals("01") ? "F" : "B") + ped.getSeriePre(), 0, 1);
                //printPhoto(R.drawable.ic_icon_pos);
                printCustom("-------------------------", 0, 1);
                printCustom((ped.getTipDoc().equals("01") ? "RUC: " : "DNI:") + (ped.getCliente().getRucCliente() != null ? ped.getCliente().getRucCliente() : ""), 0, 0);
                printCustom("NOM./R.S.: " + ped.getCliente().getNomCliente(), 0, 0);
                printCustom("DIRECCIÓN: " + (ped.getCliente().getDirDespacho() != null ? ped.getCliente().getDirDespacho() : ""), 0, 0);
                printCustom("FEC. EMI.: " + (ped.getFecha() != null ? ped.getFecha() : ""), 0, 0);
                printCustom("VENDEDOR: " + (ped.getVendedor() != null ? (ped.getVendedor().getCodVendedor() != null ? ped.getVendedor().getCodVendedor() : "") : ""), 0, 0);
                printCustom("CONDICION: " + (ped.getCodCondicion() != null ? ped.getCodCondicion() : ""), 0, 0);
                printCustom("PLACA: " + (ped.getPlaca() != null ? ped.getPlaca() : ""), 0, 0);
                printCustom("RUTA: " + (ped.getRuta() != null ? ped.getRuta() : ""), 0, 0);
                printCustom("-------------------------", 0, 1);
                printCustom("Nro PEDIDO: " + ped.getNumPedido(), 0, 1);
                printNewLine();
                Double montoIgv = 0.00;
                for (int c = 0; c < ped.getDetalles().size(); c++) {
                    montoIgv += ped.getDetalles().get(c).getMontoIgv();
                    String desc = ped.getDetalles().get(c).getDescripcion().length() > 25 ? ped.getDetalles().get(c).getDescripcion().substring(0, 25) : ped.getDetalles().get(c).getDescripcion();
                    printCustom(ped.getDetalles().get(c).getCodigo() + " " + desc, 0, 0);
                    printCustom(RetornarMonto(10, ped.getDetalles().get(c).getCantidad().toString() + " " + ped.getDetalles().get(c).getUndMed()) + RetornarMonto(11, Metodos.RetornarStringConDosDecimales(ped.getDetalles().get(c).getPrecioBase().doubleValue())) + RetornarMonto(11, Metodos.RetornarStringConDosDecimales(ped.getDetalles().get(c).getPrecioFinal().doubleValue())), 0, 0);
                    printNewLine();//32 espacios
                }
                printCustom(RetornarMonto(20, "OP. GRAVADA: ") + RetornarMonto(12, Metodos.RetornarStringConDosDecimales(ped.getMontoTotal().doubleValue() - montoIgv)), 0, 0);
                // printCustom(RetornarMonto(20,"IGV-18%: ") + RetornarMonto(12,Metodos.RetornarStringConDosDecimales(ped.getMontoTotal().doubleValue() - (ped.getMontoTotal().doubleValue()/1.18) ) ), 0, 0);
                printCustom(RetornarMonto(20, "IGV-18%: ") + RetornarMonto(12, Metodos.RetornarStringConDosDecimales(montoIgv)), 0, 0);

                printCustom(RetornarMonto(20, "IMPORTE TOTAL S/.: ") + RetornarMonto(12, ped.getMontoTotal().toString()), 0, 0);
                printNewLine();
                printCustom("Doc. Anulados: " + (ped.getAnulados() != null ? ped.getAnulados() : ""), 0, 0);
                printCustom("Gracias por su compra", 0, 1);
                printCustom("Consulte su documento en", 0, 1);
                printCustom("www.gps-inka.com", 0, 1);
                printNewLine();
                printNewLine();
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String RetornarMonto(Integer cantidad, String monto) {
        Integer tamanio = cantidad - monto.length();
        for (int i = 0; i < tamanio; i++) {
            monto = " " + monto;
        }
        return monto;
    }

    private void printCustom(String msg, int size, int align) {
        //byte[] cc = {27, 33, 0 }; // manipulate your font size in the second parameter
        byte[] cc = new byte[]{0x1B, 0x14, 0x00};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes("Cp850"), 0, msg.getBytes("Cp850").length);
            outputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print text
    private void printText(String msg) {
        try {
            outputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print byte[]
    private void printText(byte[] msg) {
        try {
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print photo
    public void printPhoto(int img) {
        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), img);
            if (bmp != null) {
                byte[] command = UtilsPrint.decodeBitmap(bmp);
                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime[] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }

    private String leftRightAlign(String str1, String str2) {
        String ans = str1 + str2;
        if (ans.length() < 31) {
            int n = (31 - str1.length() + str2.length());
            ans = str1 + new String(new char[n]).replace("\0", " ") + str2;
        }
        return ans;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket = DeviceList.getSocket();
            if (btsocket != null) {
                Toast.makeText(getApplicationContext(), "Conexión Establecida.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}

