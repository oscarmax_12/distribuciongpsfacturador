package com.legado.distribuciongps.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.InicioFragment;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;

public class InicioActivity extends BaseActivity implements InicioFragment.InicioFragmentListeners {

    public static SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        //setupToolbar();
        CardView menu2 = (CardView) findViewById(R.id.menu0);
        menu2.setCardBackgroundColor(Color.parseColor("#FF9800"));
        InicioFragment fragment = (InicioFragment) getSupportFragmentManager().findFragmentById(R.id.content_inicio_info);
        if (fragment == null) {

            fragment = new InicioFragment(this);// InicioFragment.newInstance( this);
            InicioFragment.newInstance(this);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_inicio_info, fragment).addToBackStack(null)
                    .commit();

        }
    }


    public void stopService() {
        Intent serviceIntent = new Intent(this, VerificacionMontoService.class);
        serviceIntent.setAction("stop");
        stopService(serviceIntent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("destroACt", "asdasd");
        Intent servicioFore = new Intent(this, VerificacionMontoService.class);
        servicioFore.setAction("startFore");
        startForegroundService(servicioFore);
    }

    @Override
    public boolean providesActivityToolbar() {
        return true;
    }

    private void setupToolbar() {
        final ActionBar ab = getActionBarToolbar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setTitle(Html.fromHtml("<font color='#FFFFFF'>"
                + "DATOS VEHICULO"
                + "</font>"));
        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawer();
                return true;
            case R.id.logout:
                logout();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void goToNavDrawerItem(int item) {
        switch (item) {
            case R.id.menu0:
                Intent intent0 = new Intent(this, InicioActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.menu1:
                Intent pantalla = new Intent(this, toFrameMapa.class);
                Bundle b = new Bundle();
                b.putString("placa", sessionUsuario.getCodigoAplicacion());
                b.putString("op", "lista");
                pantalla.putExtras(b);
                startActivity(pantalla);

                break;
            case R.id.menu2:
                Intent intent2 = new Intent(this, MisEntregasActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.menu3:
                Intent intent3 = new Intent(this, TabsReboteActivity.class);
                startActivity(intent3);
                finish();
                break;
//            case R.id.menu4:
//                Intent intent4 = new Intent(this, ReboteConfirmacionActivity.class);
//                startActivity(intent4);
//                finish();
//                break;

            case R.id.menu5:
                Intent intent5 = new Intent(this, toFrameMapa.class);
                Bundle b1 = new Bundle();
                b1.putString("op", "facturar");
                b1.putString("placa", "nada");
                intent5.putExtras(b1);
                startActivity(intent5);

                break;
            case R.id.menu6:
                Intent intent6 = new Intent(this, mapgeofence.class);
                startActivity(intent6);

                break;
            case R.id.menu7:
                Intent intent7 = new Intent(this, PorLiquidarActivity.class);
                Bundle b2 = new Bundle();
                b2.putString("placa", sessionUsuario.getCodigoAplicacion());
                intent7.putExtras(b2);
                startActivity(intent7);

                break;
            case R.id.btnCerrarSesion:
                logout();
                stopService();
                break;
            case R.id.btnBuscarImpresora:
                Intent intent = new Intent(this, DiscoveryActivity.class);
               this.startActivityForResult(intent, 0);
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == -1) {
            String target = data.getStringExtra(getString(R.string.title_target));
            sessionUsuario.guardaImpresora(target);
            Toast.makeText(this,"IMPRESORA ENCONTRADA "+target,Toast.LENGTH_LONG);
//            SharedPreferences.Editor editor = this.sharedPref.edit();
//            editor.putString("impresora",target);
//            editor.commit();
//            EditText mEdtTarget = (EditText) findViewById(R.id.edtTarget);
//            mEdtTarget.setText(target);
//            if (target != null) {
//
//            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    private void logout() {
        sessionUsuario.IniciarSession(false, null, "", "","");//desconectarme
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }


    @Override
    public void showdrawer() {
        openDrawer();
    }
}
