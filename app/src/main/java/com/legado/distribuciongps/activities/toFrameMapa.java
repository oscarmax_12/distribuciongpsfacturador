package com.legado.distribuciongps.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.legado.distribuciongps.BuildConfig;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.fragments.ClientesBuscar;
import com.legado.distribuciongps.fragments.Listado_packing;
import com.legado.distribuciongps.fragments.RutasFrag;
import com.legado.distribuciongps.fragments.pedidoXrefacturar_frag;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Version;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class toFrameMapa extends BaseActivity implements View.OnClickListener {
    FloatingActionButton fab, fabRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_frame_mapa);
        fab = (FloatingActionButton) findViewById(R.id.fabClientesSinCoordenadas);
//        fabRefresh = (FloatingActionButton) findViewById(R.id.fabRefreshMap);


        Listado_packing listado = new Listado_packing();
//        Bundle data = new Bundle();
//        data.putString("placa", sessionUsuario.getCodigoAplicacion());
//        listado.setArguments(data);
//        FragmentTransaction frag = getSupportFragmentManager().beginTransaction();
//        frag.replace(R.id.contenedor, listado);
//        frag.commit();
        fab.setOnClickListener(this);
//        fabRefresh.setOnClickListener(this);
        Metodos.validarSesion(this);
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(this, "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            this.finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(this, "ERROR!", "Error");
            this.finish();
        }
        Bundle b = getIntent().getExtras();
        String placa = "", op = ""; // or other values
        if (b != null) {

            placa = b.getString("placa");
            op = b.getString("op");
            if (op.equals("facturar") && placa.equals("nada")) {
                pedidoXrefacturar_frag fragment = new pedidoXrefacturar_frag();
                FragmentTransaction fgtr = getSupportFragmentManager().beginTransaction();
                Bundle data2 = new Bundle();
                data2.putString("placa", String.valueOf(placa));
                fragment.setArguments(data2);
                fgtr.replace(R.id.contenedor, fragment);
                fgtr.commit();
            } else {

                //PASO DE PARAMETROS AL FRAGMENT
                Listado_packing inicioFragment = new Listado_packing();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Bundle data = new Bundle();
                data.putString("placa", String.valueOf(placa));

                inicioFragment.setArguments(data);
                fragmentTransaction.replace(R.id.contenedor, inicioFragment);
                fragmentTransaction.commit();
            }
        } else {
            Toast.makeText(this, "Error bundle", Toast.LENGTH_LONG).show();
        }


    }


    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();

        } else {
            getFragmentManager().popBackStack();
        }

    }


    @Override
    public boolean providesActivityToolbar() {
        return true;
    }


    @Override
    public void onClick(View v) {
        int op=v.getId();
        switch (op){
            case R.id.fabClientesSinCoordenadas:
                ClientesBuscar inicioFragment = new ClientesBuscar();
                Bundle data = new Bundle();
                data.putString("empresa", Listado_packing.empresa);
                data.putString("almacenes", Listado_packing.almacenes);
                data.putString("asignaciones", Listado_packing.packings);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().addToBackStack("atras");
                fragmentTransaction.replace(R.id.contenedor, inicioFragment);
                SharedPreferences.Editor editor = RutasFrag.sharedPref.edit();
                editor.putString("depaso","si");
                editor.commit();
                fragmentTransaction.commit();
                break;
//            case R.id.fabRefreshMap:
//                RutasFrag inicioFragment2 = new RutasFrag();
//                SharedPreferences.Editor editor2 = RutasFrag.sharedPref.edit();
//                editor2.putString("depaso","no");
//                editor2.commit();
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
////               inicioFragment2.setInitialSavedState(inicioFragment2.);
//
//                ft.detach(inicioFragment2);
//                ft.attach(inicioFragment2);
//                getSupportFragmentManager().saveFragmentInstanceState(inicioFragment2);
//                ft.commit();

//                inicioFragment2.getFragmentManager().beginTransaction().detach(frag).commit();
//                break;
        }
//        if (v.getId() == R.id.fabClientesSinCoordenadas) {
//
//
//
//        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onDestroy() {

        FragmentManager fragmentManager = getSupportFragmentManager(); //Desde Activity
//        Fragment fragmento = fragmentManager.findFragmentById(R.id.txtPedidoXrefacturar);
//        Log.e("frag actual",fragmento.getTag());
        Log.e("frag actual",String.valueOf(fragmentManager.getFragments()));
        Log.e("frag actual",String.valueOf(fragmentManager.getBackStackEntryCount()));
        super.onDestroy();
        Log.e("validateSP",String.valueOf(RutasFrag.sharedPref));
        if (RutasFrag.sharedPref!=null){
            SharedPreferences.Editor editor = RutasFrag.sharedPref.edit();
            editor.remove("empresa");
            editor.remove("almacenes");
            editor.remove("asignaciones");
            editor.putString("recarga","no");
            editor.putString("depaso","no");
            editor.commit();
        }


    }



    public void showFloatingActionButton() {
        fab.show();
//        fabRefresh.show();
    }

    ;

    public void hideFloatingActionButton() {
        fab.hide();
//        fabRefresh.hide();
    }

    ;


}