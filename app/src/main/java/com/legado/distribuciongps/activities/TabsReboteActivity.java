package com.legado.distribuciongps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.InventarioReboteFragment;
import com.legado.distribuciongps.fragments.MisRebotesFragment;
import com.legado.distribuciongps.fragments.RebotesConfirmarFragment;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;

import java.util.ArrayList;
import java.util.List;

public class TabsReboteActivity extends BaseActivity {

    private ViewPager view_pager;
    private TabLayout tab_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_rebote);
        initToolbar();
        CardView menu3 = (CardView) findViewById(R.id.menu3);
        menu3.setCardBackgroundColor(Color.parseColor("#FF9800"));
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.amber900), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Gallery");
        getSupportActionBar().setTitle(
                Html.fromHtml("<font color='#FFFFFF'>"
                        + "MIS REBOTES"
                        + "</font>"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Tools.setSystemBarColor(this, R.color.grey_5);
//        Tools.setSystemBarLight(this);
    }

    private void initComponent() {
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(view_pager);

        tab_layout = (TabLayout) findViewById(R.id.tab_layout);
        tab_layout.setupWithViewPager(view_pager);
    }

    public void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RebotesConfirmarFragment.newInstance(adapter), "MIS REBOTES");
        adapter.addFragment(InventarioReboteFragment.newInstance(), "INVENTARIO");

//        adapter.addFragment(FragmentTabsGallery.newInstance(), "RECENT");
        viewPager.setAdapter(adapter);
    }


    @Override
    protected void goToNavDrawerItem(int item) {
        switch (item) {
            case R.id.menu0:
                Intent intent0 = new Intent(this, InicioActivity.class);
                startActivity(intent0);
                finish();
                break;

            case R.id.menu1:
                Intent pantalla = new Intent(this, toFrameMapa.class);
                Bundle b = new Bundle();
                b.putString("placa", sessionUsuario.getCodigoAplicacion());
                b.putString("op", "facturar");
                pantalla.putExtras(b);
                startActivity(pantalla);

                break;
            case R.id.menu2:
                Intent intent2 = new Intent(this, MisEntregasActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.menu3:
//                Intent intent3 = new Intent(this, TabsReboteActivity.class);
//                startActivity(intent3);
//                finish();
                break;
//            case R.id.menu4:
//                Intent intent4 = new Intent(this, ReboteConfirmacionActivity.class);
//                startActivity(intent4);
//                finish();
//                break;
            case R.id.menu5:
                Intent intent5 = new Intent(this, toFrameMapa.class);
                Bundle b1 = new Bundle();
                b1.putString("op", "facturar");
                b1.putString("placa", "nada");
                intent5.putExtras(b1);
                startActivity(intent5);

                break;
            case R.id.menu6:
                Intent intent6 = new Intent(this, mapgeofence.class);
                startActivity(intent6);

                break;
            case R.id.menu7:
                Intent intent7 = new Intent(this, PorLiquidarActivity.class);
                startActivity(intent7);

                break;
            case R.id.btnCerrarSesion:
                stopService();
                logout();
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawer();
                return true;
            case R.id.verPlacas:
//                VerPlacas();
                return true;
            case R.id.verUrl:
//                VerUrls();
                return true;
            case R.id.logout:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        sessionUsuario.IniciarSession(false, null, "", "","");//desconectarme
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    @Override
    public boolean providesActivityToolbar() {
        return false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, VerificacionMontoService.class);
        serviceIntent.setAction("stop");
        stopService(serviceIntent);
    }
}
