package com.legado.distribuciongps.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Articulo;
import com.legado.distribuciongps.modelo.Coordenadas;
import com.legado.distribuciongps.modelo.Motivo;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.util.CustomAdapter;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.getCoordenadasActual;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ERICK on 07/06/2018.
 */
public class PedidoXFacturarActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    public static List<Articulo> detalles;
    public static Pedido pedido;
    private static CustomAdapter customAdapter;
    @BindView(R.id.nroPedido)
    TextView nroPedido;
    @BindView(R.id.codEmpresa)
    TextView codEmpresa;
    @BindView(R.id.codSede)
    TextView codSede;
    @BindView(R.id.codMesa)
    TextView codMesa;
    @BindView(R.id.codAlmacen)
    TextView codAlmacen;
    @BindView(R.id.codVendedor)
    TextView codVendedor;
    @BindView(R.id.descCliente)
    TextView descCliente;
    @BindView(R.id.descCondicion)
    TextView descCondicion;
    @BindView(R.id.btnRegistrar)
    Button btnRegistrar;
    ProgressDialog progressDialog;
    SessionUsuario sessionUsuario;
    String seriePre;
    String tipoDoc;
    getCoordenadasActual objCoor;
    static Activity actToCerrar;
    boolean flagRegister=true;

    public static void actPedido(Activity act) {
        actToCerrar = act;
    }

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        ButterKnife.bind(this);
        sessionUsuario = new SessionUsuario(this);
        progressDialog = new ProgressDialog(this, R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        customAdapter = new CustomAdapter(this, getApplicationContext());
        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        Gson gson = new Gson();
        String json = getIntent().getStringExtra("pedido");
        Pedido ped = gson.fromJson(json, Pedido.class);

        seriePre = getIntent().getStringExtra("seriePre");
        tipoDoc = getIntent().getStringExtra("tipoDoc");
        objCoor = new getCoordenadasActual(this);

        detalles = ped.getDetalles();
        pedido = ped;
        recyclerView.setItemViewCacheSize(detalles.size());
        nroPedido.setText("Ped: " + pedido.getNumPedidoRef());
        codEmpresa.setText(pedido.getEmpresa().getDescEmpresa());
        codSede.setText(pedido.getSede().getCodSede() != null ? pedido.getSede().getDescSede() : "");
        codMesa.setText(pedido.getMesa().getCodMesa() != null ? pedido.getMesa().getDescMesa() + (pedido.getCanal().getCodCanal() != null ? " - " + pedido.getCanal().getDescCanal() : "") : "");
        codAlmacen.setText(pedido.getAlmacen().getCodAlmacen());
        codAlmacen.setVisibility(View.GONE);
        codVendedor.setText("Vend: " + pedido.getVendedor().getCodVendedor());
        descCliente.setText("Cli: " + pedido.getCliente().getNomCliente());
        descCondicion.setText("Cond: " + pedido.getCodCondicion());

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pedido.setDetalles(detalles);
//                String val=MensajesDialogAlert.mensajeEditText(PedidoActivity.this,"CONFIRME","INGRESE EL MOTIVO DE REBOTE PARCIAL");
//                if (val==null||val.equalsIgnoreCase("")){
//                    MensajesDialogAlert.mensajeError(PedidoActivity.this,"ERROR","Debe ingresar un motivo");
//                }else{
//                    Log.e("si tiene var",val);
//                }
                cargarMotivosParciales(pedido);
//                RegistrarPedidoNuevo(pedido);
            }
        });

    }

    public void cargarMotivosParciales(Pedido ped) {
        progressDialog.show();
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("op", "2");
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getMotivoService().listarMotivos(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();


                    int estado = response.body().getEstado();
                    ArrayList<Motivo> objListaMotivo = new ArrayList<>();
                    ArrayList<Object> obj = new ArrayList<>();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Motivo objMotivo = new Motivo();
                                objMotivo.setDescripcion(objItem.getString("descripcion"));
                                objMotivo.setLlave(objItem.getString("llave"));

                                objListaMotivo.add(objMotivo);
                                obj.add(objItem.getString("llave") + ", " + objItem.getString("descripcion"));
                            }

//                            adapterMotivo = new AdaptadorMotivo(getContext());
//                            adapterMotivo.addList(objListaMotivo);
//                            recyclerPackings.setAdapter(adapterPackings);
                            Object[] mStringArray = obj.toArray();
                            String[] objStrings = new String[obj.size()];
                            for (int i = 0; i < obj.size(); i++) {
                                objStrings[i] = String.valueOf(obj.get(i));
                            }
//                            String[] str = mStringArray.keySet().toArray(new String[mStringArray.length]);
                            for (int i = 0; i < mStringArray.length; i++) {
                                Log.e("string is", (String) mStringArray[i]);
//                                Log.e("string is",str[i]);
                            }
                            progressDialog.dismiss();
                            withItems(objStrings, ped);

                        }
                    } else {
                        progressDialog.dismiss();
                        MensajesDialogAlert.mensajeError(PedidoXFacturarActivity.this, "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    MensajesDialogAlert.mensajeError(PedidoXFacturarActivity.this, "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(PedidoXFacturarActivity.this, "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }

    public void withItems(String[] motivos, Pedido ped) {

        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoXFacturarActivity.this);
        String title = "ELEGIR MOTIVO:";
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(titleM)

                .setItems(motivos, (dialog, which) -> {
                    Toast.makeText(PedidoXFacturarActivity.this, motivos[which] + " is clicked", Toast.LENGTH_SHORT).show();
                    String[] parts = motivos[which].split(",");
                    Log.e("cliente", parts[0]);
                    Log.e("codcliente", parts[1]);
                    boolean op = MensajesDialogAlert.mensajeConfirmacion(PedidoXFacturarActivity.this, "Confirmar", "Rebote parcial con motivo " + parts[1] + " ?");
                    if (op) {
//                        Coordenadas LL;
//                        Metodos.showProgressDialogWithTitle("Procesando Rebote Total", progressDialog2);
//                        LL = checkMyPermissionLocation();
//                        if (LL == null) {
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                            MensajesDialogAlert.mensajeError(PedidoActivity.this, "ERROR", "Debe tener el GPS prendido");
//                        } else {
//                            String user = sessionUsuario.getNickUsuario();
//                            String[] serie = seriePre.split("-");
                        ped.setMotivo(parts[0]);

                        RegistrarPedidoNuevo(ped);
//                        }


                    }
                });

//        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", null);
//        builder.setNeutralButton("NEUTRAL", null);
//        builder.setPositiveButtonIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
        builder.setIcon(getResources().getDrawable(R.drawable.ic_24px));

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }

    public static void Actualizar(int posicion) {
        customAdapter.refreshBlockOverlay(posicion);
    }

    private void RegistrarPedidoNuevo(Pedido pedidoNuevo) {

        Coordenadas coor = objCoor.checkMyPermissionLocation();
        if (coor == null) {
            MensajesDialogAlert.mensajeError(PedidoXFacturarActivity.this, "Error", "Debe tener prendido el GPS");
        } else {
            pedidoNuevo.setPlaca(sessionUsuario.getCodigoAplicacion());
            pedidoNuevo.setSeriePre(seriePre);
            pedidoNuevo.setTipDoc(tipoDoc);
            pedidoNuevo.setCoordX(coor.getLatitud());
            pedidoNuevo.setCoordY(coor.getLongitud());
            Gson gson = new Gson();
            String pedidoJson = gson.toJson(pedidoNuevo);
            Log.d("assas", pedidoJson);

            try {
                progressDialog.show();
                Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPedidoService().RegistrarPedido(pedidoNuevo);
                call.enqueue(new Callback<JsonRespuesta>() {
                    @Override
                    public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {
                        JsonRespuesta rptaClientes = response.body();
                        Integer estado = rptaClientes.getEstado();
                        if (estado == 1) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Pedido registrado correctamente.", Toast.LENGTH_LONG).show();
                            finish();
                            //objCoor.stopBroadcast();
                            flagRegister=false;
                            actToCerrar.finish();
                        } else if (estado == 2) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), rptaClientes.getMensaje(), Toast.LENGTH_LONG).show();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo", Toast.LENGTH_SHORT).show();
                        }
                        objCoor.stopBroadcast();
                    }

                    @Override
                    public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                        //progress.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo", Toast.LENGTH_SHORT).show();
                        objCoor.stopBroadcast();
                    }
                });
            } catch (Exception e) {
                MensajesDialogAlert.mensajeError(PedidoXFacturarActivity.this, "ERROR", e.getMessage());
                e.printStackTrace();
                objCoor.stopBroadcast();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(flagRegister){
            objCoor.stopBroadcast();
        }

    }

//    public void pedido() {
//
//        progressDialog.show();
//        Map<String, String> dataClientes = new HashMap<>();
//        dataClientes.put("codemp", codEmpresaTemp);
//        dataClientes.put("codlocal", codLocallidad);
//        dataClientes.put("codtipdoc", tipoDoc);
//        dataClientes.put("numpedido", codPedido);
//        dataClientes.put("numplaca", sessionUsuario.getCodigoAplicacion());
//        Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getPedidoService().ObtenerPedido(dataClientes);
//        call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
//            @Override
//            public void onResponse(Call<JsonRespuesta<Pedido>> call, retrofit2.Response<JsonRespuesta<Pedido>> response) {
//                JsonRespuesta<Pedido> respuesta = response.body();
//                Pedido pedido = respuesta.getItem();
//                Integer estado = respuesta.getEstado();
//                if (estado == 1) {
////                    Intent intent = new Intent(getActivity(), PedidoActivity.class);
////                    PedidoActivity.actPedido(getActivity());
//                    Gson gson = new Gson();
//                    pedido.getVendedor().setUsuario(sessionUsuario.getNickUsuario());
//                    pedido.setNumPedidoRef(codPedido);
//                    String pedidoJson = gson.toJson(pedido);
//                    Log.e("PedidoJson", pedidoJson);
//                    intent.putExtra("pedido", pedidoJson);
//                    intent.putExtra("seriePre", seriePre);
//                    intent.putExtra("tipoDoc", tipoDoc);
////                    startActivity(intent);
//                    progressDialog.dismiss();
//                } else {
//                    Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
//                    progressDialog.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
//                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
//                progressDialog.dismiss();
//            }
//        });
//
//
//    }

    interface ItemReboteConfirmarListener {

        void onItemClick(View view, int position);
    }
}