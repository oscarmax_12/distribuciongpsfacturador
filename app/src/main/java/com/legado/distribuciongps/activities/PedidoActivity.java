package com.legado.distribuciongps.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Articulo;
import com.legado.distribuciongps.modelo.Coordenadas;
import com.legado.distribuciongps.modelo.Motivo;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.util.CustomAdapter;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.getCoordenadasActual;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ERICK on 07/06/2018.
 */
public class PedidoActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    public static List<Articulo> detalles;
    public static Pedido pedido;
    private static CustomAdapter customAdapter;
    @BindView(R.id.nroPedido)
    TextView nroPedido;
    @BindView(R.id.codEmpresa)
    TextView codEmpresa;
    @BindView(R.id.codSede)
    TextView codSede;
    @BindView(R.id.codMesa)
    TextView codMesa;
    @BindView(R.id.codAlmacen)
    TextView codAlmacen;
    @BindView(R.id.codVendedor)
    TextView codVendedor;
    @BindView(R.id.descCliente)
    TextView descCliente;
    @BindView(R.id.descCondicion)
    TextView descCondicion;
    @BindView(R.id.btnRegistrar)
    Button btnRegistrar;
    ProgressDialog progressDialog;
    SessionUsuario sessionUsuario;
    String seriePre;
    String tipoDoc;
    getCoordenadasActual objCoor;
    static Activity actToCerrar;
    boolean flagRegister=true;

    public static void actPedido(Activity act) {
        actToCerrar = act;
    }

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        ButterKnife.bind(this);
        sessionUsuario = new SessionUsuario(this);
        progressDialog = new ProgressDialog(this, R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        customAdapter = new CustomAdapter(this, getApplicationContext());
        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        Gson gson = new Gson();
        String json = getIntent().getStringExtra("pedido");
        Pedido ped = gson.fromJson(json, Pedido.class);
        Metodos.validarSesion(this);
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(this, "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            this.finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(this, "ERROR!", "Error");
            this.finish();
        }

        seriePre = getIntent().getStringExtra("seriePre");
        tipoDoc = getIntent().getStringExtra("tipoDoc");
        objCoor = new getCoordenadasActual(this);

        detalles = ped.getDetalles();
        pedido = ped;
        recyclerView.setItemViewCacheSize(detalles.size());
        nroPedido.setText("Ped: " + pedido.getNumPedidoRef());
        codEmpresa.setText(pedido.getEmpresa().getDescEmpresa());
        codSede.setText(pedido.getSede().getCodSede() != null ? pedido.getSede().getDescSede() : "");
        codMesa.setText(pedido.getMesa().getCodMesa() != null ? pedido.getMesa().getDescMesa() + (pedido.getCanal().getCodCanal() != null ? " - " + pedido.getCanal().getDescCanal() : "") : "");
        codAlmacen.setText(pedido.getAlmacen().getCodAlmacen());
        codAlmacen.setVisibility(View.GONE);
        codVendedor.setText("Vend: " + pedido.getVendedor().getCodVendedor());
        descCliente.setText("Cli: " + pedido.getCliente().getNomCliente());
        descCondicion.setText("Cond: " + pedido.getCodCondicion());

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pedido.setDetalles(detalles);
//                String val=MensajesDialogAlert.mensajeEditText(PedidoActivity.this,"CONFIRME","INGRESE EL MOTIVO DE REBOTE PARCIAL");
//                if (val==null||val.equalsIgnoreCase("")){
//                    MensajesDialogAlert.mensajeError(PedidoActivity.this,"ERROR","Debe ingresar un motivo");
//                }else{
//                    Log.e("si tiene var",val);
//                }
                cargarMotivosParciales(pedido);
//                RegistrarPedidoNuevo(pedido);
            }
        });

    }

    public void cargarMotivosParciales(Pedido ped) {
        progressDialog.show();
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("op", "2");
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getMotivoService().listarMotivos(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();


                    int estado = response.body().getEstado();
                    ArrayList<Motivo> objListaMotivo = new ArrayList<>();
                    ArrayList<Object> obj = new ArrayList<>();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Motivo objMotivo = new Motivo();
                                objMotivo.setDescripcion(objItem.getString("descripcion"));
                                objMotivo.setLlave(objItem.getString("llave"));

                                objListaMotivo.add(objMotivo);
                                obj.add(objItem.getString("llave") + ", " + objItem.getString("descripcion"));
                            }

//                            adapterMotivo = new AdaptadorMotivo(getContext());
//                            adapterMotivo.addList(objListaMotivo);
//                            recyclerPackings.setAdapter(adapterPackings);
                            Object[] mStringArray = obj.toArray();
                            String[] objStrings = new String[obj.size()];
                            for (int i = 0; i < obj.size(); i++) {
                                objStrings[i] = String.valueOf(obj.get(i));
                            }
//                            String[] str = mStringArray.keySet().toArray(new String[mStringArray.length]);
                            for (int i = 0; i < mStringArray.length; i++) {
                                Log.e("string is", (String) mStringArray[i]);
//                                Log.e("string is",str[i]);
                            }
                            progressDialog.dismiss();
                            withItems(objStrings, ped);

                        }
                    } else {
                        progressDialog.dismiss();
                        MensajesDialogAlert.mensajeError(PedidoActivity.this, "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    MensajesDialogAlert.mensajeError(PedidoActivity.this, "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(PedidoActivity.this, "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }

    public void withItems(String[] motivos, Pedido ped) {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PedidoActivity.this);
        String title = "ELEGIR MOTIVO:";
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        titleM.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(titleM)

                .setItems(motivos, (dialog, which) -> {
                    Toast.makeText(PedidoActivity.this, motivos[which] + " is clicked", Toast.LENGTH_SHORT).show();
                    String[] parts = motivos[which].split(",");
                    Log.e("cliente", parts[0]);
                    Log.e("codcliente", parts[1]);
                    boolean op = MensajesDialogAlert.mensajeConfirmacion(PedidoActivity.this, "Confirmar", "Rebote parcial con motivo " + parts[1] + " ?");
                    if (op) {
//                        Coordenadas LL;
//                        Metodos.showProgressDialogWithTitle("Procesando Rebote Total", progressDialog2);
//                        LL = checkMyPermissionLocation();
//                        if (LL == null) {
//                            Metodos.hideProgressDialogWithTitle(progressDialog2);
//                            MensajesDialogAlert.mensajeError(PedidoActivity.this, "ERROR", "Debe tener el GPS prendido");
//                        } else {
//                            String user = sessionUsuario.getNickUsuario();
//                            String[] serie = seriePre.split("-");
                        ped.setMotivo(parts[0]);

                        RegistrarPedidoNuevo(ped);
//                        }


                    }
                });

//        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", null);
//        builder.setNeutralButton("NEUTRAL", null);
//        builder.setPositiveButtonIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
        builder.setIcon(getResources().getDrawable(R.drawable.ic_24px));

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }

    public static void Actualizar(int posicion) {
        customAdapter.refreshBlockOverlay(posicion);
    }

    private void RegistrarPedidoNuevo(Pedido pedidoNuevo) {

        Coordenadas coor = objCoor.checkMyPermissionLocation();
        if (coor == null) {
            MensajesDialogAlert.mensajeError(PedidoActivity.this, "Error", "Debe tener prendido el GPS");
        } else {
            pedidoNuevo.setPlaca(sessionUsuario.getCodigoAplicacion());
            pedidoNuevo.setSeriePre(seriePre);
            pedidoNuevo.setTipDoc(tipoDoc);
            pedidoNuevo.setCoordX(coor.getLatitud());
            pedidoNuevo.setCoordY(coor.getLongitud());
            Gson gson = new Gson();
            String pedidoJson = gson.toJson(pedidoNuevo);
            Log.d("assas", pedidoJson);

            try {
                progressDialog.show();
                Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPedidoService().RegistrarPedido(pedidoNuevo);
                call.enqueue(new Callback<JsonRespuesta>() {
                    @Override
                    public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {
                        JsonRespuesta rptaClientes = response.body();
                        Integer estado = rptaClientes.getEstado();
                        switch (response.body().getEstado()){
                            case 1:
                                Metodos.hideProgressDialogWithTitle(progressDialog);
                                MensajesDialogAlert.mensajeExito(PedidoActivity.this, "EXITO", response.body().getMensaje());
                                finish();
                                //objCoor.stopBroadcast();
                                flagRegister=false;
                                actToCerrar.finish();
                                break;
                            case -1:
                                Metodos.hideProgressDialogWithTitle(progressDialog);
                                MensajesDialogAlert.mensajeError(PedidoActivity.this, "Ingreso no valido", response.body().getMensaje());
                                break;
                            default:
                                Metodos.hideProgressDialogWithTitle(progressDialog);
                                MensajesDialogAlert.mensajeError(PedidoActivity.this, "Comunicarse con Soporte", response.body().getMensaje());
                                break;

                        }
//                        if (estado == 1) {
//                            progressDialog.dismiss();
//                            Toast.makeText(getApplicationContext(), "Pedido registrado correctamente.", Toast.LENGTH_LONG).show();
//                            finish();
//                            //objCoor.stopBroadcast();
//                            flagRegister=false;
//                            actToCerrar.finish();
//                        } else if (estado == 2) {
//                            progressDialog.dismiss();
//                            Toast.makeText(getApplicationContext(), rptaClientes.getMensaje(), Toast.LENGTH_LONG).show();
//                        } else {
//                            progressDialog.dismiss();
//                            Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo", Toast.LENGTH_SHORT).show();
//                        }
                        objCoor.stopBroadcast();
                    }

                    @Override
                    public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                        //progress.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Problemas de conexión , por favor intentelo de nuevo", Toast.LENGTH_SHORT).show();
                        objCoor.stopBroadcast();
                    }
                });
            } catch (Exception e) {
                MensajesDialogAlert.mensajeError(PedidoActivity.this, "ERROR", e.getMessage());
                e.printStackTrace();
                objCoor.stopBroadcast();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(flagRegister){
            objCoor.stopBroadcast();
        }

    }
}