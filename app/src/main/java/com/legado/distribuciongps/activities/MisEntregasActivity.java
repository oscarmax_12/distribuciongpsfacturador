package com.legado.distribuciongps.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.MisEntregasFragment;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;
import com.legado.distribuciongps.util.SessionUsuario;

public class MisEntregasActivity extends BaseActivity {
    SessionUsuario sessionUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_entregas);
        sessionUsuario = new SessionUsuario(this);
        setupToolbar();
        CardView menu2 = (CardView) findViewById(R.id.menu2);
        menu2.setCardBackgroundColor(Color.parseColor("#FF9800"));
        MisEntregasFragment fragment = (MisEntregasFragment) getSupportFragmentManager().findFragmentById(R.id.content_mis_entregas);
        if (fragment == null) {
            fragment = MisEntregasFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_mis_entregas, fragment).addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    @Override
    public boolean providesActivityToolbar() {
        return true;
    }

    private void setupToolbar() {
        final ActionBar ab = getActionBarToolbar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);

        ab.setTitle(
                Html.fromHtml("<font color='#FFFFFF'>"
                        + "MIS LIQUIDACIONES"
                        + "</font>"));
        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void goToNavDrawerItem(int item) {
        switch (item) {
            case R.id.menu0:
                Intent intent0 = new Intent(this, InicioActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.menu1:
                Intent pantalla = new Intent(this, toFrameMapa.class);
                Bundle b = new Bundle();
                b.putString("placa", sessionUsuario.getCodigoAplicacion());
                b.putString("op", "facturar");
                pantalla.putExtras(b);
                startActivity(pantalla);

                break;
            case R.id.menu2:
//                Intent intent2 = new Intent(this, MisEntregasActivity.class);
//                startActivity(intent2);
//                finish();
                break;
            case R.id.menu3:
                Intent intent3 = new Intent(this, TabsReboteActivity.class);
                startActivity(intent3);
                finish();
                break;
//            case R.id.menu4:
//                Intent intent4 = new Intent(this, ReboteConfirmacionActivity.class);
//                startActivity(intent4);
//                finish();
//                break;
            case R.id.menu5:
                Intent intent5 = new Intent(this, toFrameMapa.class);
                Bundle b1 = new Bundle();
                b1.putString("op", "facturar");
                b1.putString("placa", "nada");
                intent5.putExtras(b1);
                startActivity(intent5);

                break;
            case R.id.menu6:
                Intent intent6 = new Intent(this, mapgeofence.class);
                startActivity(intent6);

                break;
            case R.id.menu7:
                Intent intent7 = new Intent(this, PorLiquidarActivity.class);
                Bundle b2 = new Bundle();
                b2.putString("placa", sessionUsuario.getCodigoAplicacion());
                intent7.putExtras(b2);
                startActivity(intent7);

                break;
            case R.id.btnCerrarSesion:
                stopService();
                logout();
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawer();
                return true;
            case R.id.logout:
                logout();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, VerificacionMontoService.class);
        serviceIntent.setAction("stop");
        stopService(serviceIntent);
    }

    private void logout() {
        sessionUsuario.IniciarSession(false, null, "", "","");//desconectarme
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
