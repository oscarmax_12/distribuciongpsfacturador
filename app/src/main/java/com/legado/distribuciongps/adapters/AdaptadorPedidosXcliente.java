package com.legado.distribuciongps.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.act_productos;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Producto;

import java.util.ArrayList;

public class AdaptadorPedidosXcliente extends RecyclerView.Adapter<AdaptadorPedidosXcliente.ViewHolder> {
    private Context context;
    private Activity act;
    private ArrayList<Pedido> listaPedidoAux;
    private ArrayList<Producto> listaProductoAux;
    private int posicionItemSeleccionado;


    public AdaptadorPedidosXcliente(Activity act, Context context) {
        this.context = context;
        this.act = act;
    }

    public int obtenerPosicionItemSeleccionado() {
        return posicionItemSeleccionado;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pedidoxcliente, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //Mostrar los datos de las rutinas dentro de cada control que contiene el cardView

        //1. Crear un objeto rutina para poder extraer los datos
        Pedido pedido = listaPedidoAux.get(position);


        //2. Mostrar los datos de la packing en los controles del cardView
        holder.txtNroPedido.setText("Pedido: " + pedido.getNumPedido());
        holder.txtNroSeriePreImpresa.setText("Serie Pre.: " + pedido.getSeriePre());
        holder.txtMontoTotalPedido.setText("Monto Total: " + String.valueOf(pedido.getMontoTotal()));
        holder.txtDescripContraCre.setText(pedido.getTipoPago());

    }

    @Override
    public int getItemCount() {
        return listaPedidoAux.size();
    }

    public void addList(ArrayList<Pedido> arrayList) {
        //Permite recivir la lista de biens que esta dentro de la clase bien
        listaPedidoAux = arrayList;

        //Permite notificar al Adaptador que cambios en la base de datos
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnLongClickListener, View.OnCreateContextMenuListener {
        //Permite declarar y relacionar los controles que estan contenidos dentro del CardView
        TextView txtNroPedido, txtNroSeriePreImpresa, txtMontoTotalPedido, txtDescripContraCre;
        RecyclerView rvListaPorductos;

        public ViewHolder(View itemView) {
            super(itemView);

            //Relacionar (enlazar) los controles declarados en java con los controles que estan en el CardView
            txtNroPedido = (TextView) itemView.findViewById(R.id.txtNroPedidoitem);
            txtNroSeriePreImpresa = (TextView) itemView.findViewById(R.id.txtNumSeriePreItem);
            txtMontoTotalPedido = (TextView) itemView.findViewById(R.id.txtTotalPedidoItem);
            txtDescripContraCre = (TextView) itemView.findViewById(R.id.txtContadoCredito);



            itemView.setOnClickListener(this::onClick);
//            if (listaPedidoAux.size() <= 1) {
//                itemView.callOnClick();
//            }
            itemView.setOnLongClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
////            int position = getAdapterPosition();
////            String identificador = listaPackingAux.get(position).getIdentificador_s();
////            String marca = listaPackingAux.get(position).getMarca();
////           Log.e("dio clic",marca);
////
////            Intent obj=new Intent(context,DetalleBienAct.class);
////            obj.putExtra("cod",identificador);
////            context.startActivity(obj);
//            v.
//        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            String nombre = listaPackingAux.get(this.getAdapterPosition()).getNro_packing();
//
//            menu.setHeaderTitle(nombre);
//
//            menu.add(0, 1, 0, "Editar");
//            menu.add(0, 2, 0, "Cancelar");
//            menu.add(0, 3, 0, "Completado");
        }

        @Override
        public boolean onLongClick(View v) {
            posicionItemSeleccionado = this.getAdapterPosition();
            return false;
        }

        private void onClick(View v) {


            int pos = getAdapterPosition();

            String codPedido, montoTotal, codEmpresaTemp, tipoDoc, seriePre,codLocal,vendedorNomb,clienteNomb,direcDespacho,ruta;
            Intent pantalla;
            Bundle b;
            if (pos <= -1) {
                codPedido = listaPedidoAux.get(0).getNumPedido();
                montoTotal = String.valueOf(listaPedidoAux.get(0).getMontoTotal());
                codEmpresaTemp = String.valueOf(listaPedidoAux.get(0).getCodEmpresaTemp());
                tipoDoc = listaPedidoAux.get(0).getTipDoc();
                seriePre = listaPedidoAux.get(0).getSeriePre();
                codLocal = listaPedidoAux.get(0).getCodLocalidad();
                pantalla = new Intent(act, act_productos.class);
                b = new Bundle();
                b.putString("flag10", listaPedidoAux.get(0).getFlag10());
                b.putString("nroPedido", codPedido);
                b.putString("montoTotal", montoTotal);
                b.putString("codEmpresaTemp", codEmpresaTemp);
                b.putString("tipoDoc", tipoDoc);
                b.putString("seriePre", seriePre);
                b.putString("codLoc", codLocal);
                b.putString("codCliente", listaPedidoAux.get(0).getCliente().getCodCliente());


                pantalla.putExtras(b);
                act.startActivity(pantalla);
            } else {
                codPedido = listaPedidoAux.get(pos).getNumPedido();
                codEmpresaTemp = listaPedidoAux.get(pos).getCodEmpresaTemp();
                montoTotal = String.valueOf(listaPedidoAux.get(pos).getMontoTotal());
                tipoDoc = listaPedidoAux.get(pos).getTipDoc();
                seriePre = listaPedidoAux.get(pos).getSeriePre();
                codLocal = listaPedidoAux.get(pos).getCodLocalidad();
                vendedorNomb = listaPedidoAux.get(pos).getVendedorNomb();
                direcDespacho = listaPedidoAux.get(pos).getDireccDespacho();
                clienteNomb = listaPedidoAux.get(pos).getCliente().getNomCliente();
                ruta = listaPedidoAux.get(pos).getRuta();
                pantalla = new Intent(act, act_productos.class);
                b = new Bundle();
                b.putString("flag10", listaPedidoAux.get(pos).getFlag10());
                b.putString("codEmpresaTemp", codEmpresaTemp);
                b.putString("nroPedido", codPedido);
                b.putString("montoTotal", montoTotal);
                b.putString("tipoDoc", tipoDoc);
                b.putString("seriePre", seriePre);
                b.putString("codLoc", codLocal);
                b.putString("direcc_despacho", direcDespacho);
                b.putString("vendedorNomb", vendedorNomb);
                b.putString("nombCliente", clienteNomb);
                b.putString("ruta", ruta);
                Gson objPedido=new Gson();
                String pedidoJson = objPedido.toJson(listaPedidoAux.get(pos));
                Log.d("ObjAdapterPed", pedidoJson);
                b.putString("objPedido",pedidoJson);
                b.putString("codCliente", listaPedidoAux.get(pos).getCliente().getCodCliente());

                pantalla.putExtras(b);
                act.startActivity(pantalla);
            }
        }
    }

}
