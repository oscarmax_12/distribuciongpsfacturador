package com.legado.distribuciongps.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Producto;

import java.util.ArrayList;

public class AdaptadorProductosXPedido extends RecyclerView.Adapter<AdaptadorProductosXPedido.ViewHolder>{
    private Context context;
    private ArrayList<Producto> listaProductoAux;
    private int posicionItemSeleccionado;


    public AdaptadorProductosXPedido(Context context){
        this.context = context;
        this.listaProductoAux = new ArrayList<>();
    }

    public int obtenerPosicionItemSeleccionado(){
        return posicionItemSeleccionado;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_productodetallexpedido,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //Mostrar los datos de las rutinas dentro de cada control que contiene el cardView

        //1. Crear un objeto rutina para poder extraer los datos
        Producto producto = listaProductoAux.get(position);



        //2. Mostrar los datos de la packing en los controles del cardView
        holder.txtProductoName.setText(producto.getNombreProducto());
        holder.txtMontoProducto.setText("Monto : "+producto.getMontoTotalxProduc());
        holder.txtCantidad.setText("Cant: "+producto.getCantProd());
        holder.chkLlevar.setChecked(producto.isLlevar());
        holder.txtUnidadMedida.setText("UM.: "+producto.getUniMedida());
        holder.chkLlevar.setOnClickListener(v -> {
            if (holder.chkLlevar.isChecked()){
                listaProductoAux.get(position).setLlevar(true);
            }else{
                listaProductoAux.get(position).setLlevar(false);
            }
        });
    }

    public void refresh(){
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return listaProductoAux.size();
    }

    public void addList(ArrayList<Producto> arrayList){
        //Permite recivir la lista de biens que esta dentro de la clase bien
        listaProductoAux = arrayList;

        //Permite notificar al Adaptador que cambios en la base de datos
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements  View.OnLongClickListener, View.OnCreateContextMenuListener{
        //Permite declarar y relacionar los controles que estan contenidos dentro del CardView
        TextView txtProductoName, txtUnidadMedida,txtMontoProducto,txtCantidad;
        EditText txtCantEditar;
        CheckBox chkLlevar;

        public ViewHolder(View itemView) {
            super(itemView);

            //Relacionar (enlazar) los controles declarados en java con los controles que estan en el CardView
            txtProductoName = (TextView) itemView.findViewById(R.id.txtNombreProducto_item);
            txtMontoProducto = (TextView) itemView.findViewById(R.id.txtMontoProducto_item);
            txtCantidad=(TextView)itemView.findViewById(R.id.txtCantProducto_item);
            chkLlevar=(CheckBox)itemView.findViewById(R.id.chkLlevarProducto_item);
            txtUnidadMedida=(TextView)itemView.findViewById(R.id.txtUnidadMedida_item);
            chkLlevar.setOnClickListener(v -> {

                if (chkLlevar.isChecked()){
                    Log.e("CHECK","TRUE");
//                    chkLlevar.setChecked(false);
                    listaProductoAux.get(obtenerPosicionItemSeleccionado()).setLlevar(true);
                }else{
                    Log.e("CHECK","FALSE");

//                    chkLlevar.setChecked(true);

                    listaProductoAux.get(obtenerPosicionItemSeleccionado()).setLlevar(false);
                }
            });

            itemView.setOnLongClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
////            int position = getAdapterPosition();
////            String identificador = listaPackingAux.get(position).getIdentificador_s();
////            String marca = listaPackingAux.get(position).getMarca();
////           Log.e("dio clic",marca);
////
////            Intent obj=new Intent(context,DetalleBienAct.class);
////            obj.putExtra("cod",identificador);
////            context.startActivity(obj);
//            v.
//        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            String nombre = listaPackingAux.get(this.getAdapterPosition()).getNro_packing();
//
//            menu.setHeaderTitle(nombre);
//
//            menu.add(0, 1, 0, "Editar");
//            menu.add(0, 2, 0, "Cancelar");
//            menu.add(0, 3, 0, "Completado");
        }

        @Override
        public boolean onLongClick(View v) {
            posicionItemSeleccionado = this.getAdapterPosition();
            return false;
        }
    }

}
