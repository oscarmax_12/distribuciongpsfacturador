package com.legado.distribuciongps.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.PedidoActivity;
import com.legado.distribuciongps.fragments.MyDialogEdit;
import com.legado.distribuciongps.fragments.pedidoFacturarFragment;
import com.legado.distribuciongps.util.Metodos;

/**
 * Created by ERICK on 07/06/2018.
 */
public class AdaptadorXFacturar extends RecyclerView.Adapter<AdaptadorXFacturar.MyViewHolder>  {

    private LayoutInflater inflater;
    private Context ctx;
    private FragmentActivity activity;
    private cambioBotonGuardar listener;



    public AdaptadorXFacturar(FragmentActivity activity, Context ctx) {
        inflater = LayoutInflater.from(ctx);
        this.ctx = ctx;
        this.activity = activity;
    }

    @Override
    public AdaptadorXFacturar.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_pedido, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }
   public interface cambioBotonGuardar {

        void cambiobtnFactGuardar();
    }
    public void refreshBlockOverlay(int position) {
        notifyItemChanged(position);
    }

    @Override
    public void onBindViewHolder(final AdaptadorXFacturar.MyViewHolder holder, final int position) {
//        holder.txtCodigo.setText(PedidoActivity.detalles.get(position).getCodigo());
        holder.txtDescripcion.setText(PedidoActivity.detalles.get(position).getCodigo()+" - "+PedidoActivity.detalles.get(position).getDescripcion());
        holder.txtCantidad.setText(String.valueOf(PedidoActivity.detalles.get(position).getCantidad()));
        holder.txtCantidadNueva.setText(String.valueOf(PedidoActivity.detalles.get(position).getCantidadNueva()));
        holder.txtPreUni.setText(Metodos.RetornarStringConTresDecimales(PedidoActivity.detalles.get(position).getPrecioFinal().doubleValue()));
        holder.txtTotal.setText(Metodos.RetornarStringConTresDecimales(PedidoActivity.detalles.get(position).getCantidad()*PedidoActivity.detalles.get(position).getPrecioFinal().doubleValue() ));
        holder.btnEditar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                MyDialogEdit newFragment = MyDialogEdit.newInstance(PedidoActivity.detalles.get(position).getCantidadNueva().toString(),PedidoActivity.detalles.get(position).getCantidad().toString(), PedidoActivity.detalles.get(position).getCodigo(),position);//carritoFragment
                newFragment.show(ft, "edit");
                listener.cambiobtnFactGuardar();
            }
        });

        holder.btnQuitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.cambiobtnFactGuardar();
                new AlertDialog.Builder(activity)
                .setTitle("CONFIRMACION")
                .setMessage("Desea eliminar el articulo de la lista ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        notifyItemRemoved(position);

                        PedidoActivity.detalles.remove(position);
                        notifyItemRangeChanged(position, PedidoActivity.detalles.size());
                    }})
                .setNegativeButton(android.R.string.no, null).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return PedidoActivity.detalles.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {
        protected Button btnQuitar,btnEditar;
        public TextView txtCodigo, txtDescripcion, txtCantidad, txtPreUni, txtTotal, txtCantidadNueva;

        public MyViewHolder(View itemView) {
            super(itemView);
//            txtCodigo = (TextView) itemView.findViewById(R.id.txtCodigo);
            txtDescripcion = (TextView) itemView.findViewById(R.id.txtDescripcion);
            txtCantidad = (TextView) itemView.findViewById(R.id.txtCantidad);
            txtCantidadNueva = (TextView) itemView.findViewById(R.id.txtCantidadNueva);
            txtPreUni = (TextView) itemView.findViewById(R.id.txtPreUni);
            txtTotal = (TextView) itemView.findViewById(R.id.txtTotal);
            btnQuitar = (Button) itemView.findViewById(R.id.btnQuitar);
            btnEditar= (Button) itemView.findViewById(R.id.btnEditar);
        }
    }

}