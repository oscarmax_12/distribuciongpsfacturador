package com.legado.distribuciongps.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Placa;
import java.util.List;


/**
 * Created by __Adrian__ on 25/8/2017.
 */

public class PlacaAdapter extends RecyclerView.Adapter<PlacaAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Placa item);
    }

    private final List<Placa> items;
    private final OnItemClickListener listener;
    private Activity activity;

    public PlacaAdapter(List<Placa> items, OnItemClickListener listener,Activity activity) {
        this.items = items;
        this.listener = listener;
        this.activity=activity;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.placa_item, parent, false);
        return new ViewHolder(v,activity);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bind(items.get(position), listener);

    }

    @Override public int getItemCount() {

        return items.size();

    }
    static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtPlaca;
        public ListView listPacking ;
        public Activity activity;

        public ViewHolder(View itemView,Activity activity) {
            super(itemView);
            txtPlaca = (TextView) itemView.findViewById(R.id.txtPlaca);
            this.activity=activity;
        }

        public void bind(final Placa item, final OnItemClickListener listener) {
            txtPlaca.setText(item.getNumPlaca());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }

    }

}