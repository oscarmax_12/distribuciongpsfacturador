package com.legado.distribuciongps.adapters;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.AdapterCommunication;
import com.legado.distribuciongps.util.SessionUsuario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by __Adrian__ on 19/4/2017.
 */

public class EntregasRecyclerAdapter extends RecyclerView.Adapter<EntregasRecyclerAdapter.EntregasRecyclerViewHolder> implements ItemEntregaListener {
    private static final String TAG = "EntregasRecyclerAdapter";

    private List<Rebote> documentoDeudas =new ArrayList<>();
    private FragmentActivity activity;
    private AdapterCommunication mListener;
    private Context context;
    private String codCliente;
    private SessionUsuario sessionUsuario;
    Fragment fragment;
    // MyDialogProgress dialogPago;

    Bundle args;
    public void setOnClickListener(AdapterCommunication listener){
        mListener = listener;
    }



    public EntregasRecyclerAdapter(List<Rebote> documentoDeudas, FragmentActivity activity, Fragment fragment) {
        this.activity=activity;
        this.documentoDeudas=documentoDeudas;
        this.fragment=fragment;
    }
    @Override
    public EntregasRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mis_entregas, parent, false);
        sessionUsuario=new SessionUsuario(v.getContext());
        return new EntregasRecyclerViewHolder(v, this);
    }
    @Override
    public void onBindViewHolder(final EntregasRecyclerViewHolder holder, final int position) {
        holder.numeroDoc.setText(documentoDeudas.get(position).getSerie()+"-"+documentoDeudas.get(position).getPreimpreso());
//      holder.tipoDoc.setText(documentoDeudas.get(position).getTipoDoc());
//   holder.codCliente.setText(documentoDeudas.get(position).getCodCliente());
        //holder.descCliente.setText(documentoDeudas.get(position).getTipoRebote());
        holder.monto.setText(documentoDeudas.get(position).getMonto().toString());
        holder.txtClienteDescrip.setText("Cli. "+documentoDeudas.get(position).getDescCliente()+" "+documentoDeudas.get(position).getCodCliente());
        holder.txtDireccion.setText(documentoDeudas.get(position).getDirecCliente());

        holder.itemView.setSelected(true);
//        holder.itemView.setBackgroundResource(R.drawable.selector_row_cobranza);


    }

    public BigDecimal montoTotalLiquidacion(){
        BigDecimal montoTotal=new BigDecimal(0);
        for (int i = 0; i <documentoDeudas.size() ; i++) {
            montoTotal = montoTotal.add(documentoDeudas.get(i).getMonto());
        }
        return montoTotal;
    }
    @Override
    public int getItemCount() {
        return documentoDeudas.size();
    }

    public List<Rebote> listaEntregas(){
        return documentoDeudas;
    }


    @Override
    public void onItemClick(View view, int position) {


    }


    public static class EntregasRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView numeroDoc;
        public TextView codCliente;
        public TextView descCliente;
        // public TextView tipoDoc;
        public TextView monto;
        public TextView saldo;
        public TextView txtEstado;
        public TextView txtClienteDescrip;
        public TextView txtDireccion;
        public ItemEntregaListener listener;

        public EntregasRecyclerViewHolder(View itemView, ItemEntregaListener listener) {
            super(itemView);
            numeroDoc = (TextView) itemView.findViewById(R.id.txtNumeroDoc);
            //  tipoDoc = (TextView) itemView.findViewById(R.id.txtTipoDoc);
            monto = (TextView) itemView.findViewById(R.id.txtMonto);
            txtClienteDescrip = (TextView) itemView.findViewById(R.id.txtDescClienteEntregas);
            txtDireccion = (TextView) itemView.findViewById(R.id.txtDirecionClienteEntregas);

            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(view, getAdapterPosition());
        }
    }
}

interface ItemEntregaListener{

    void onItemClick(View view, int position);
}

