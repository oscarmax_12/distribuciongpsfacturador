package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by OSCAR on 17/09/2019.
 */
public interface ClienteService {


    @POST("listarClientesXPackings")
    Call<JsonRespuesta> ListarClientesXPackings(@QueryMap Map<String, String> parametros);

}