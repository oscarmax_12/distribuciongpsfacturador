package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.DocReparto;
import com.legado.distribuciongps.modelo.Rebote;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by ERICK on 16/08/2019.
 */

public interface RepartoService {

    @POST("cargarDocReparto")
    Call<JsonRespuesta<DocReparto>> IniciarJornadaReparto(@QueryMap Map<String, String> parametros);
    @POST("registrarRebote")
    Call<JsonRespuesta> RegistrarRebote(@Body Rebote rebote);

    //Listado de Rutas by Oscar 9/9/2019
    @POST("listarRutasXPlaca")
    Call<JsonRespuesta> ListarRutasXPlaca(@QueryMap Map<String, String> parametros);
}
