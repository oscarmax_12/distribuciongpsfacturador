package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.BeansPedido;
import com.legado.distribuciongps.modelo.Documento;
import com.legado.distribuciongps.modelo.Pedido;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by OSCAR on 17/09/2019.
 */
public interface PackingService {


    @POST("listarPackingXPlaca")
    Call<JsonRespuesta> ListarPackingXPlaca(@QueryMap Map<String, String> parametros);

}