package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Placa;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by __Adrian__ on 10/4/2017.
 */
public interface PlacaService {

    @POST("listarPlacasxSede")
    Call<JsonRespuesta<Placa>> listarPlacasxSede(@QueryMap Map<String, String> parametros);

}