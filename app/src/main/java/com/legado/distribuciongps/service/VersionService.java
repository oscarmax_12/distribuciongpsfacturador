package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Entrega;
import com.legado.distribuciongps.modelo.Version;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ERICK on 16/08/2019.
 */

public interface VersionService {

    @POST("validarversion")
    Call<JsonRespuesta> ValidarVersion(@Body Version obj_version);

}
