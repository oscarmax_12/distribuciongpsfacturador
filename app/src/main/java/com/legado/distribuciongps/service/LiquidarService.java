package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by OSCAR on 17/09/2019.
 */
public interface LiquidarService {


    @POST("infoLiquidarXPlaca")
    Call<JsonRespuesta> infoLiquidarXPlaca(@QueryMap Map<String, String> parametros);

    @POST("liquidarCierre")
    Call<JsonRespuesta> liquidarCierre(@QueryMap Map<String, String> parametros);

}