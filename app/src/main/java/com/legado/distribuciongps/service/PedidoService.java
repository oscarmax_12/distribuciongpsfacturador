package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.BeansPedido;
import com.legado.distribuciongps.modelo.Documento;
import com.legado.distribuciongps.modelo.Pedido;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
/**
 * Created by ERICK on 07/06/2018.
 */
public interface PedidoService {

    @POST("obtenerPedido")
    Call<JsonRespuesta<Pedido>> ObtenerPedido(@QueryMap Map<String, String> parametros);

    @POST("obtenerNuevoPedido")
    Call<JsonRespuesta<Pedido>> ObtenerNuevoPedido(@QueryMap Map<String, String> parametros);

    @POST("registrarPedidoMovil")
    Call<JsonRespuesta> RegistrarPedido(@Body Pedido pedido);

    @POST("refacturarPedido")
    Call<JsonRespuesta<Documento>> RefacturarPedido(@Body Pedido pedido);

    @POST("facturar_pedidos")
    Call<JsonRespuesta> FacturarPedido(@QueryMap Map<String, String> parametros);

    @POST("validarPedido")
    Call<JsonRespuesta<BeansPedido>> ValidarPedido(@Body Pedido pedido);

    @POST("listarPedidosyProductosXCliente")
    Call<JsonRespuesta> listarPedidosyProductosXCliente(@QueryMap Map<String, String> parametros);

    @POST("updateLogRefacturacion")
    Call<JsonRespuesta> updateLogRefacturacion(@QueryMap Map<String, String> parametros);
}