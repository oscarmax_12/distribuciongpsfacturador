package com.legado.distribuciongps.api;

import com.legado.distribuciongps.service.BancoService;
import com.legado.distribuciongps.service.ClienteService;
import com.legado.distribuciongps.service.EntregaService;
import com.legado.distribuciongps.service.LiquidarService;
import com.legado.distribuciongps.service.LoginService;
import com.legado.distribuciongps.service.MetricasService;
import com.legado.distribuciongps.service.MotivoService;
import com.legado.distribuciongps.service.PackingService;
import com.legado.distribuciongps.service.PedidoService;
import com.legado.distribuciongps.service.PlacaService;
import com.legado.distribuciongps.service.RepartoService;
import com.legado.distribuciongps.service.VersionService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ERICK on 17/5/2017.
 */

public class ApiRetrofit {

    private static ApiRetrofit instance = null;
    private LoginService loginService;
    private PlacaService placaService;
    private PedidoService pedidoService;
    private RepartoService repartoService;
    private PackingService packingService;
    private ClienteService clienteService;
    private MotivoService motivoService;
    private MetricasService metricasService;
    private EntregaService entregaService;
    private VersionService versionService;
    private BancoService bancoService;
    private LiquidarService liquidarService;

    public LiquidarService getLiquidarService() {
        return liquidarService;
    }

    public BancoService getBancoService() {
        return bancoService;
    }

    public VersionService getVersionService() {
        return versionService;
    }

    public EntregaService getEntregaService() {
        return entregaService;
    }

    public MetricasService getMetricasService() {
        return metricasService;
    }

    public MotivoService getMotivoService() {
        return motivoService;
    }

    public ClienteService getClienteService() {
        return clienteService;
    }

    public PackingService getPackingService() { return this.packingService; }

    public RepartoService getRepartoService() {
        return repartoService;
    }

    public static ApiRetrofit getInstance(String baseUrl) {
        if (instance == null) {
            instance = new ApiRetrofit(baseUrl);
        }
        return instance;
    }
    public static ApiRetrofit getInstance() {
        if (instance == null) {
            //instance = new ApiRetrofit( "http://172.17.55.13:8084/distribucion/");
            //WS PRODUCCCION
//            instance = new ApiRetrofit( "http://190.223.55.172:8080/serviciodistribucion_test/distribucion/");
//            WS DE PRUEBAS
//            instance = new ApiRetrofit( "http://172.17.55.13:8085/distribucion/");
//            instance = new ApiRetrofit( "http://190.223.55.172:8080/serviciodistribucionv4/distribucion/");
//            instance = new ApiRetrofit( "http://190.223.55.172:8080/serviciodistribucionv5/distribucion/");
            instance = new ApiRetrofit( "http://172.16.0.19:8080/serviciodistribucionv5/distribucion/");
        }
        return instance;
    }

    public static void BorrarInstance(){
        instance = null;
    }

    private ApiRetrofit (String baseUrl) {
        buildRetrofit(baseUrl);
    }

    private okhttp3.OkHttpClient getRequestHeader() {
        return new okhttp3.OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder = originalRequest.newBuilder().header("Authorization",Credentials.basic("adrian", "123456"));
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }}).connectTimeout(3, TimeUnit.MINUTES).writeTimeout(3,TimeUnit.MINUTES).readTimeout(3,TimeUnit.MINUTES).build();
    }

    private void buildRetrofit(String url) {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
        Retrofit mRetrofit = new Retrofit.Builder().baseUrl(url).client(getRequestHeader()).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(rxAdapter).build();
        this.loginService = mRetrofit.create(LoginService.class);
        this.placaService = mRetrofit.create(PlacaService.class);
        this.pedidoService=mRetrofit.create(PedidoService.class);
        this.repartoService=mRetrofit.create(RepartoService.class);
        this.packingService=mRetrofit.create(PackingService.class);
        this.clienteService=mRetrofit.create(ClienteService.class);
        this.motivoService=mRetrofit.create(MotivoService.class);
        this.metricasService=mRetrofit.create(MetricasService.class);
        this.entregaService=mRetrofit.create(EntregaService.class);
        this.versionService=mRetrofit.create(VersionService.class);
        this.bancoService=mRetrofit.create(BancoService.class);
        this.liquidarService=mRetrofit.create(LiquidarService.class);

    }

    public LoginService getLoginService() {
        return this.loginService;
    }

    public PlacaService getPlacaService() {
        return placaService;
    }

    public PedidoService getPedidoService() {
        return pedidoService;
    }
}